% display the hyperspectral data from Foster et al. 
% http://personalpages.manchester.ac.uk/staff/david.foster/Hyperspectral_images_of_natural_scenes_02.html
% with the chosen illuminant spectra
% other scripts show the image under canonical light and estimated light by
% different illuminant estimation methods
% the data is rendered for CIE 1931 colour matching functions and sRGB
% representation 

% by Roshanak Zakizadeh June 2016

clear, clc,close all
scenenum = 1;
load(['../../DATA/spectral data/allscenes_31/scene' num2str(scenenum) '.mat']);
%%
% load the chosen spectra, if you are using a different spectra be careful
% to change the path to the relevant chosen light 
load illum_4000.mat;
illum = illum_4000(2:end-1,:);

radiances = zeros(size(reflectances)); % initialize array
nbands = size(reflectances,3);
for i = 1:nbands,
  radiances(:,:,i) = reflectances(:,:,i)*illum(i);
end
im_radiance = radiances; %#ok<NASGU>
[r, c, w] = size(radiances);
radiances = reshape(radiances, r*c, w);

% Load the CIE 1931 colour matching functions xyzbar
load xyzbar.mat;
xyzbar = xyzbar(2:end-1,:);
XYZ = (xyzbar'*radiances')';
XYZ_light = (xyzbar'*illum)';
XYZ_light = XYZ_light./(XYZ_light(2));

XYZ = reshape(XYZ, r, c, 3);
XYZ = max(XYZ, 0);
XYZ = XYZ/max(XYZ(:));

% sRGB color image
RGB_casted = XYZ2sRGB_exgamma(XYZ);
RGB_casted = max(RGB_casted, 0);
RGB_casted = min(RGB_casted, 1);

RGB_light = XYZ2sRGB_exgamma(XYZ_light);
RGB_light_norm = RGB_light./norm(RGB_light);
figure,imshow(RGB_casted.^0.4, 'Border','tight');
disp(RGB_light_norm);
%% under canonical light
illum_canonical = ones(31,1);
radiances = zeros(size(reflectances)); % initialize array
nbands = size(reflectances,3);
for i = 1:nbands,
  radiances(:,:,i) = reflectances(:,:,i)*illum_canonical(i);
end
im_radiance = radiances;
[r, c, w] = size(radiances);
radiances = reshape(radiances, r*c, w);

% Load the CIE 1931 colour matching functions xyzbar
load xyzbar.mat;
xyzbar = xyzbar(2:end-1,:);
XYZ = (xyzbar'*radiances')';
XYZ_light = (xyzbar'*illum_canonical)';
XYZ_light = XYZ_light./(XYZ_light(2));

XYZ = reshape(XYZ, r, c, 3);
XYZ = max(XYZ, 0);
XYZ = XYZ/max(XYZ(:));

% RGB color image
RGB_canonical= XYZ2sRGB_exgamma(XYZ);
RGB_canonical = max(RGB_canonical, 0);
RGB_canonical = min(RGB_canonical, 1);

RGB_light = XYZ2sRGB_exgamma(XYZ_light);
RGB_light_norm = RGB_light./norm(RGB_light);
figure; imshow(RGB_canonical.^0.4, 'Border','tight'),title('under canonical light');
disp(RGB_light_norm);

%% reproduce the image using estimated illuminant by whitepatch
% load the path of the chosen light
load(['estimated illuminant for 4000k/scene' num2str(scenenum) '_WP.mat'])
radiances_reproduced = bsxfun(@rdivide,radiances,w');
load xyzbar.mat;
xyzbar = xyzbar(2:end-1,:);
XYZ = (xyzbar'*radiances_reproduced')';
XYZ_light = (xyzbar'*w)';
XYZ_light = XYZ_light./(XYZ_light(2));

XYZ = reshape(XYZ, r, c, 3);
XYZ = max(XYZ, 0);
XYZ = XYZ/max(XYZ(:));

% RGB color image
RGB = XYZ2sRGB_exgamma(XYZ);
RGB = max(RGB, 0);
RGB = min(RGB, 1);

RGB_light = XYZ2sRGB_exgamma(XYZ_light);
RGB_light_norm = RGB_light./norm(RGB_light);
figure,imshow(RGB.^0.4, 'Border','tight')
%figure; subplot(2,1,1),imshow(RGB.^0.4, 'Border','tight'), subplot(2,1,2),...
 %   imshow(RGB_canonical.^0.4, 'Border','tight');
disp(RGB_light_norm);
 
disp('recovery error')
disp(recov_error)

disp('reproduction error')
disp(repro_error)

%% reproduce the image using estimated illuminant by general grey world
load(['estimated illuminant for 4000k/scene' num2str(scenenum) '_GGW.mat'])
r1 = 2;
c1 = 1;
estim_illum = squeeze(w(:,r1,c1));
radiances_reproduced = bsxfun(@rdivide,radiances,estim_illum');
load xyzbar.mat;
xyzbar = xyzbar(2:end-1,:);
XYZ = (xyzbar'*radiances_reproduced')';
XYZ_light = (xyzbar'*estim_illum)';
XYZ_light = XYZ_light./(XYZ_light(2));

XYZ = reshape(XYZ, r, c, 3);
XYZ = max(XYZ, 0);
XYZ = XYZ/max(XYZ(:));

% RGB color image
RGB = XYZ2sRGB_exgamma(XYZ);
RGB = max(RGB, 0);
RGB = min(RGB, 1);

RGB_light = XYZ2sRGB_exgamma(XYZ_light);
RGB_light_norm = RGB_light./norm(RGB_light);
imshow(RGB.^0.4, 'Border','tight')
%figure; subplot(2,1,1),imshow(RGB.^0.4, 'Border','tight'), subplot(2,1,2),...
 %   imshow(RGB_canonical.^0.4, 'Border','tight');
disp(RGB_light_norm);

disp('recovery error')
disp(recov_error(r1,c1))

disp('reproduction error')
disp(repro_error(r1,c1))

%% reproduce the image using estimated illuminant by 1ge
load(['estimated illuminant for 4000k/scene' num2str(scenenum) '_1GE.mat'])
r1 = 4;
c1 = 1;
estim_illum = squeeze(w(:,r1,c1));
radiances_reproduced = bsxfun(@rdivide,radiances,estim_illum');
load xyzbar.mat;
xyzbar = xyzbar(2:end-1,:);
XYZ = (xyzbar'*radiances_reproduced')';
XYZ_light = (xyzbar'*estim_illum)';
XYZ_light = XYZ_light./(XYZ_light(2));

XYZ = reshape(XYZ, r, c, 3);
XYZ = max(XYZ, 0);
XYZ = XYZ/max(XYZ(:));

% RGB color image
RGB = XYZ2sRGB_exgamma(XYZ);
RGB = max(RGB, 0);
RGB = min(RGB, 1);

RGB_light = XYZ2sRGB_exgamma(XYZ_light);
RGB_light_norm = RGB_light./norm(RGB_light);
figure; subplot(2,1,1),imshow(RGB.^0.4, 'Border','tight'), subplot(2,1,2),...
    imshow(RGB_canonical.^0.4, 'Border','tight');
disp(RGB_light_norm);

disp('recovery error')
disp(recov_error(r1,c1))

disp('reproduction error')
disp(repro_error(r1,c1))

%% reproduce the image using estimated illuminant by sog
load(['estimated illuminant for 4000k/scene' num2str(scenenum) '_SOG.mat'])
r1 = 2;

estim_illum = squeeze(w(:,r1));
radiances_reproduced = bsxfun(@rdivide,radiances,estim_illum');
load xyzbar.mat;
xyzbar = xyzbar(2:end-1,:);
XYZ = (xyzbar'*radiances_reproduced')';
XYZ_light = (xyzbar'*estim_illum)';
XYZ_light = XYZ_light./(XYZ_light(2));

XYZ = reshape(XYZ, r, c, 3);
XYZ = max(XYZ, 0);
XYZ = XYZ/max(XYZ(:));

% RGB color image
RGB = XYZ2sRGB_exgamma(XYZ);
RGB = max(RGB, 0);
RGB = min(RGB, 1);

RGB_light = XYZ2sRGB_exgamma(XYZ_light);
RGB_light_norm = RGB_light./norm(RGB_light);
imshow(RGB.^0.4, 'Border','tight')
% figure; subplot(2,1,1),imshow(RGB.^0.4, 'Border','tight'), subplot(2,1,2),...
%     imshow(RGB_canonical.^0.4, 'Border','tight');
disp(RGB_light_norm);

disp('recovery error')
disp(recov_error(r1))

disp('reproduction error')
disp(repro_error(r1))