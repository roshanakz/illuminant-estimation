% this script converts hyperspectral data to CIE XYZ and sRGB representations
% http://personalpages.manchester.ac.uk/staff/d.h.foster/Tutorial_HSI2RGB/Tutorial_HSI2RGB.html
% AND estimate the illuminant of the images in hyperspectral space as well
% as calculating their recovery and reproduction angular errors in the same
% space

clear, clc,
scenenum = 1;
% load the data from your own path instead
load(['../../DATA/spectral data/allscenes_31/scene' num2str(scenenum) '.mat']);
load 'illum_25000.mat';
illum = illum_25000(2:end-1,:);
radiances = zeros(size(reflectances)); % initialize array
nbands = size(reflectances,3);
for i = 1:nbands,
  radiances(:,:,i) = reflectances(:,:,i)*illum(i);
end
im_radiance = radiances;
[r, c, w] = size(radiances);
radiances = reshape(radiances, r*c, w);


load xyzbar.mat;
xyzbar = xyzbar(2:end-1,:);
XYZ = (xyzbar'*radiances')';
XYZ_light = (xyzbar'*illum)';
XYZ_light = XYZ_light./(XYZ_light(2));

XYZ = reshape(XYZ, r, c, 3);
XYZ = max(XYZ, 0);
XYZ = XYZ/max(XYZ(:));

% RGB color image
RGB = XYZ2sRGB_exgamma(XYZ);
RGB = max(RGB, 0);
RGB = min(RGB, 1);

RGB_light = XYZ2sRGB_exgamma(XYZ_light);
RGB_light_norm = RGB_light./norm(RGB_light);
figure; imshow(RGB.^0.4, 'Border','tight');

disp(RGB_light_norm);

%%
% illuminant estimation
addpath '../simple statistics';
addpath '../simple statistics/Grey-edge';

%% white_patch
 
% radiance
w = simpleSattistics_spectral(im_radiance,0,-1,0);
illum_norm = illum./norm(illum);
esti = illum_norm./w;
esti = esti./sqrt(sum(esti.^2));
repro_error = acosd(sum(esti./sqrt(sum(ones(size(reflectances,3),1))))); %#ok<*NASGU>

est=w'./(sqrt(sum(w'.^2)')); 
recov_error = acosd((illum_norm'.*(est))*ones(size(reflectances,3),1));

save(['estimated illuminant for 25000k/scene' num2str(scenenum) '_WP.mat'], 'w','recov_error','repro_error') 

 %%  Grey-World sigma=0 p=1
 % general grey-world p and sigma
 
 repro_error = zeros(15,10);
 recov_error = zeros(15,10);
 w = zeros(size(reflectances,3),15,10);
 
 %radiance
 for mink_norm = 1:15
     for sigmas = 1:10
         w(:,mink_norm,sigmas) = simpleSattistics_spectral(im_radiance,0,mink_norm,sigmas);
         illum_norm = illum./norm(illum);
         esti = illum_norm./w(:,mink_norm,sigmas);
         esti = esti./sqrt(sum(esti.^2));
         repro_error(mink_norm,sigmas) = acosd(sum(esti./sqrt(sum(ones(size(reflectances,3),1)))));

         est=w(:,mink_norm,sigmas)'./(sqrt(sum(w(:,mink_norm,sigmas)'.^2)')); 
         recov_error(mink_norm,sigmas)=acosd((illum_norm'.*(est))*ones(size(reflectances,3),1));

     end
 end
 
 % % 3 band
%  for p = 1:15
%      for sigmas = 1:10
%          w(:,p,sigmas) = simpleSattistics(RGB,0,p,sigmas);
%          esti = RGB_light_norm./w(:,p,sigmas)';
%          esti = esti./sqrt(sum(esti.^2));
%          ang_gw(p,sigmas) = acosd(sum(esti./sqrt(3)));
%          est=w(:,p,sigmas)'./(sqrt(sum(w(:,p,sigmas)'.^2)'));
%          err_gw(p,sigmas)=acosd((RGB_light_norm.*(est))*[1;1;1]);
%      end
%  end

disp('min reproduction error')
minrepro = min(repro_error(:));
[r1,c1] =  find(repro_error==min(repro_error(:))); %#ok<*ASGLU>

disp('min recovery error')
minrecov = min(recov_error(:));
[r2,c2] =  find(recov_error==min(recov_error(:)));

save(['estimated illuminant for 4000k/scene' num2str(scenenum) '_GGW.mat'], 'w','recov_error','repro_error') 

 %%  Shades of Grey
 repro_error = zeros(15,1);
 recov_error = zeros(15,1);
 w = zeros(size(reflectances,3),15);

 %radiance
 for mink_norm = 2
     
         w(:,mink_norm) = simpleSattistics_spectral(im_radiance,0,mink_norm,0);
         illum_norm = illum./norm(illum);
         esti = illum_norm./w(:,mink_norm);
         esti = esti./sqrt(sum(esti.^2));
         repro_error(mink_norm) = acosd(sum(esti./sqrt(sum(ones(size(reflectances,3),1)))));

         est=w(:,mink_norm)'./(sqrt(sum(w(:,mink_norm)'.^2)')); 
         recov_error(mink_norm)=acosd((illum_norm'.*(est))*ones(size(reflectances,3),1));

     
 end
 
 
%  repro_wp = zeros(15,1);
%  recov_wp = zeros(15,1);
%  w = zeros(3,15);
%  
%  for mink_norm = 1:15
%          w(:,mink_norm) = simpleSattistics(RGB,0,mink_norm,0);
%          esti = RGB_light_norm./w(:,mink_norm)';
%          esti = esti./sqrt(sum(esti.^2));
%          repro_wp(mink_norm) = acosd(sum(esti./sqrt(3)));
%          est=w(:,mink_norm)'./(sqrt(sum(w(:,mink_norm)'.^2)'));
%          recov_wp(mink_norm)=acosd((RGB_light_norm.*(est))*[1;1;1]);
%      
%  end
 
disp('min reproduction error')
minrepro = min(repro_error(2:end,:));
r1 =  find(repro_error==min(repro_error(2:end,:)));

disp('min recovery error')
minrecov = min(recov_error(2:end,:));
r2 =  find(recov_error==min(recov_error(2:end,:)));

save(['estimated illuminant for 25000k/scene' num2str(scenenum) '_SOG.mat'], 'w','recov_error','repro_error') 
 
  %% Grey-Edge
% mink_norm=19;    % any number between 1 and infinity
% sigmas=8;        % sigma 
diff_order= 2;   % differentiation order (1 or 2)
 
repro_error = zeros(15,10);
 recov_error = zeros(15,10);
 w = zeros(size(reflectances,3),15,10);
 
 %radiance
 for mink_norm = 1:15
     for sigmas = 1:10
         w(:,mink_norm,sigmas) = simpleSattistics_spectral(im_radiance,diff_order,mink_norm,sigmas);
         illum_norm = illum./norm(illum);
         esti = illum_norm./w(:,mink_norm,sigmas);
         esti = esti./sqrt(sum(esti.^2));
         repro_error(mink_norm,sigmas) = acosd(sum(esti./sqrt(sum(ones(size(reflectances,3),1)))));

         est=w(:,mink_norm,sigmas)'./(sqrt(sum(w(:,mink_norm,sigmas)'.^2)')); 
         recov_error(mink_norm,sigmas)=acosd((illum_norm'.*(est))*ones(size(reflectances,3),1));

     end
 end
 
%  repro_wp = zeros(15,10);
%  recov_wp = zeros(15,10);
%  w = zeros(3,15,10);
% 
% for mink_norm = 1:15
%      for sigmas = 1:10
%          w(:,mink_norm,sigmas) = simpleSattistics(RGB,diff_order,mink_norm,sigmas);
%          esti = RGB_light_norm./w(:,mink_norm,sigmas)';
%          esti = esti./sqrt(sum(esti.^2));
%          repro_wp(mink_norm,sigmas) = acosd(sum(esti./sqrt(3)));
%          est=w(:,mink_norm,sigmas)'./(sqrt(sum(w(:,mink_norm,sigmas)'.^2)'));
%          recov_wp(mink_norm,sigmas)=acosd((RGB_light_norm.*(est))*[1;1;1]);
%      end
%  end


 disp('min reproduction error')
minrepro = min(repro_error(:));
[r1,c1] =  find(repro_error==min(repro_error(:)));

disp('min recovery error')
minrecov = min(recov_error(:));
[r2,c2] =  find(recov_error==min(recov_error(:)));

save(['estimated illuminant for 4000k/scene' num2str(scenenum) '_2GE.mat'], 'w','recov_error','repro_error') 
