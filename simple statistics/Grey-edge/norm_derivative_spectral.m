function [Derw]=norm_derivative_spectral(in, sigma, order)

if(nargin<3) order=1; end

% R=in(:,:,1);
% G=in(:,:,2);
% B=in(:,:,3);
Derx = zeros(size(in));
Dery = zeros(size(in));
Derw = zeros(size(in));
Derxx = zeros(size(in));
Deryy = zeros(size(in));
Derxy = zeros(size(in));

if(order==1)
    
    for bandnum = 1:size(in,3)        
        Derx(:,:,bandnum)=gDer(squeeze(in(:,:,bandnum)),sigma,1,0);
        Dery(:,:,bandnum)=gDer(squeeze(in(:,:,bandnum)),sigma,0,1);
        Derw(:,:,bandnum)=sqrt(Derx(:,:,bandnum).^2+Dery(:,:,bandnum).^2);    
    end
end

if(order==2)        %computes frobius norm
    
    for bandnum = 1:size(in,3)
        Derxx(:,:,bandnum)=gDer(squeeze(in(:,:,bandnum)),sigma,2,0);
        Deryy(:,:,bandnum)=gDer(squeeze(in(:,:,bandnum)),sigma,0,2);
        Derxy(:,:,bandnum)=gDer(squeeze(in(:,:,bandnum)),sigma,1,1);
        Derw(:,:,bandnum)=sqrt(Derxx(:,:,bandnum).^2+4*Derxy(:,:,bandnum).^2+Deryy(:,:,bandnum).^2);
    end
end