function w = simpleSattistics_spectral(input_data,njet,mink_norm,sgm,mask_im)

if(nargin<2), njet=0; end
if(nargin<3), mink_norm=1; end
if(nargin<4), sigma=1; end
if(nargin<5), mask_im=zeros(size(input_data,1),size(input_data,2)); end

% remove all saturated points
% saturation_threshold = 255;
% mask_im2 = mask_im + (dilation33(double(max(input_data,[],3)>=saturation_threshold)));   
% mask_im2=double(mask_im2==0);
% mask_im2=set_border(mask_im2,sigma+1,0);
% the mask_im2 contains pixels higher saturation_threshold and which are
% not included in mask_im.

% output_data=input_data;


if(sgm~=0)
    mask_im = gDer(double(mask_im),sgm,0,0);
end
mask_im2 = set_border(1-double(mask_im),sgm+1,0)==1;
%mask_im2 = ~mask_im2;

if(njet==0)
   if(sgm~=0)
     for ii=1:size(input_data,3)
        input_data(:,:,ii)=gDer(input_data(:,:,ii),sgm,0,0);
     end
   end
end

if(njet>0)
    
    [Derx]=norm_derivative_spectral(input_data, sgm, njet);
    
    input_data=Derx;
    %input_data(:,:,2)=Gx;
    %input_data(:,:,3)=Bx;    
end

input_data=abs(input_data);
white_band = zeros(size(input_data,3),1);
if(mink_norm~=-1)          % minkowski norm = (1,infinity >
    kleur=power(input_data,mink_norm);
 
    for bandnum = 1 : size(input_data,3)
        white_band(bandnum) = power(sum(sum(kleur(:,:,bandnum).*mask_im2)),1/mink_norm);
        %white_G = power(sum(sum(kleur(:,:,2).*mask_im2)),1/mink_norm);
        %white_B = power(sum(sum(kleur(:,:,3).*mask_im2)),1/mink_norm);

        %white_G=white_G/som;
        %white_B=white_B/som;
    end
        som=sqrt(sum(white_band.^2));

        white_band=white_band./som;
else                    %minkowski-norm is infinit: Max-algorithm   
    
    
%     R=input_data(:,:,1);
%     G=input_data(:,:,2);
%     B=input_data(:,:,3);
    for bandnum = 1 : size(input_data,3)
        
        temp = input_data(:,:,bandnum);
        white_band(bandnum) = max(temp(:).*mask_im2(:));
    %white_G=max(G(:).*mask_im2(:));
    %white_B=max(B(:).*mask_im2(:));
         
    %white_G=white_G/som;
    %white_B=white_B/som;
    end
    som=sqrt(sum(white_band.^2));
    white_band=white_band./som;

    
end
%w = [white_band;white_G;white_B;];
w = white_band;