function [img_out, mask] = imgShiftMask(img_raw, blk, sat, coordinates) %, p)

img_out = double(img_raw) - blk;
img_out(img_out<0)=0;

%mask = max(img_out,[],3) >= (sat-blk)*0.98;
mask = max(img_out,[],3) >= (sat)*0.98;
% mask = max(img_out,[],3) >= sat-blk;

if exist('coordinates', 'var')
    coordinates = min(coordinates,[Inf size(mask,1) Inf size(mask,2)]);
    coordinates = max(coordinates,[1 1 1 1]);
    mask(coordinates(1):coordinates(2),coordinates(3):coordinates(4)) = true;
end