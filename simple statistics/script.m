clear,clc,
camera = 'Canon600D';
load(['C:\ROSHANAK\DATA\NUS processed\' camera '\' camera '_gt_firstgreyedge.mat']);
%% white patch
N = length(all_image_names);
recoveryError = zeros([N 1]);
reproductionError = zeros([N 1]);
estimateIlluminant = zeros([N 3]);

[recoveryError, reproductionError, estimateIlluminant] = testSimpleStatistics(camera, 0, -1, 0);    

    
    
save(['C:\ROSHANAK\DATA\NUS processed\' camera '\' camera '_gt_whitepatch_notsubsampled'],'recoveryError', 'reproductionError','all_image_names',...
   'saturation_level','darkness_level','estimateIlluminant','groundtruth_illuminants');
%% first greyedge
N = length(all_image_names);
recoveryError = zeros([N 9 10]);
reproductionError = zeros([N 9 10]);
estimateIlluminant = zeros([N 3 9 10]);

for mink_norm = 1:9
    parfor sgm = 1:10
        [recoveryError(:,mink_norm,sgm), reproductionError(:,mink_norm,sgm), estimateIlluminant(:,:,mink_norm,sgm)] = testSimpleStatistics(camera, 1, mink_norm, sgm);    
    end
end
    
    
save(['C:\ROSHANAK\DATA\NUS processed\' camera '\' camera '_gt_firstgreyedge_all3'],'recoveryError', 'reproductionError','all_image_names',...
   'saturation_level','darkness_level','estimateIlluminant','groundtruth_illuminants');

%% second grey edge
N = length(all_image_names);
recoveryError = zeros([N 9 10]);
reproductionError = zeros([N 9 10]);
estimateIlluminant = zeros([N 3 9 10]);

for mink_norm = 1:9
    parfor sgm = 1:10
        [recoveryError(:,mink_norm,sgm), reproductionError(:,mink_norm,sgm), estimateIlluminant(:,:,mink_norm,sgm)] = testSimpleStatistics(camera,2, mink_norm, sgm);    
    end
end
    
    
save(['C:\ROSHANAK\DATA\NUS processed\' camera '\' camera '_gt_secondgreyedge_all3'],'recoveryError', 'reproductionError','all_image_names',...
   'saturation_level','darkness_level','estimateIlluminant','groundtruth_illuminants');

%% shades of gray
N = length(all_image_names);
recoveryError = zeros([N 9]);
reproductionError = zeros([N 9]);
estimateIlluminant = zeros([N 3 9]);

parfor mink_norm = 1:9
    
       [recoveryError(:,mink_norm), reproductionError(:,mink_norm), estimateIlluminant(:,:,mink_norm)] = testSimpleStatistics(camera,0, mink_norm,0);    
   
end
    
    
save(['C:\ROSHANAK\DATA\NUS processed\' camera '\' camera '_gt_shadesofgray_all3'],'recoveryError', 'reproductionError','all_image_names',...
   'saturation_level','darkness_level','estimateIlluminant','groundtruth_illuminants');

%% gray world
N = length(all_image_names);
recoveryError = zeros([N 9 10]);
reproductionError = zeros([N 9 10]);
estimateIlluminant = zeros([N 3 9 10]);

for mink_norm = 1:9
    parfor sgm = 1:10
         [recoveryError(:,mink_norm,sgm), reproductionError(:,mink_norm,sgm), estimateIlluminant(:,:,mink_norm,sgm)] = testSimpleStatistics(camera,0, mink_norm, sgm-1);    
    end
end
    
    
    
save(['C:\ROSHANAK\DATA\NUS processed\' camera '\' camera '_gt_generalgrayworld_all3'],'recoveryError', 'reproductionError','all_image_names',...
   'saturation_level','darkness_level','estimateIlluminant','groundtruth_illuminants');

%% gamut pixel

N = length(all_image_names);
reproductionError = zeros([N 1]);
truel= groundtruth_illuminants;

estimateIlluminant = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\Results_Excel\Excelsheets\' camera '.xlsx'],'0','B3:D202');
for i =1:N
        reproductionError(i) = acosd(dot([1;1;1]/sqrt(3),(truel(i,:)./squeeze(estimateIlluminant(i,:)))./norm(truel(i,:)./squeeze(estimateIlluminant(i,:)))));
   
end

recoveryError = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\Results_Excel\Excelsheets\' camera '.xlsx'],'0','E3:E202');
save(['C:\ROSHANAK\DATA\NUS processed\' camera '\' camera '_Gamut_Pixel'],'recoveryError', 'reproductionError','all_image_names',...
   'saturation_level','darkness_level','estimateIlluminant','groundtruth_illuminants');
%% gamut 1st edge

N = length(all_image_names);
reproductionError = zeros([N 1]);
truel= groundtruth_illuminants;

estimateIlluminant = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\Results_Excel\Excelsheets\' camera '.xlsx'],'1grad','J3:L202');
for i =1:N
        reproductionError(i) = acosd(dot([1;1;1]/sqrt(3),(truel(i,:)./squeeze(estimateIlluminant(i,:)))./norm(truel(i,:)./squeeze(estimateIlluminant(i,:)))));
   
end

recoveryError = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\Results_Excel\Excelsheets\' camera '.xlsx'],'1grad','M3:M202');
save(['C:\ROSHANAK\DATA\NUS processed\' camera '\' camera '_Gamut_1stEdge'],'recoveryError', 'reproductionError','all_image_names',...
   'saturation_level','darkness_level','estimateIlluminant','groundtruth_illuminants');


