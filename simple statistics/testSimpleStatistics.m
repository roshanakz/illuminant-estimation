function [ae, reproe, ei] = testSimpleStatistics(camera,njet,mink_norm,sgm)


addpath('Grey-edge');
DB = load(['C:\ROSHANAK\DATA\original dataset\singapore\original singapore\' camera '\' camera '_gt.mat']);
N = length(DB.all_image_names);
truel = DB.groundtruth_illuminants;

ei = zeros([N 3]);
ae = zeros([N 1]);
reproe = zeros([N 1]);

in_dir  = ['C:\ROSHANAK\DATA\original dataset\singapore\original singapore\' camera '\PNG\']; 

for i =1:N
    img = double(imread([in_dir DB.all_image_names{i}, '.png']));
    % The Rehabilitation of MaxRGB CIC10 by Funt&Shi
    %if mink_norm == -1
    %    img = imresize(img,[64 64],'cubic');
    %end
    [img, mask] = imgShiftMask(img, DB.darkness_level, DB.saturation_level, DB.CC_coords(i,:));
    w = simpleSattistics(img,njet,mink_norm,sgm,mask);    
    ei(i,:) = w';
    ae(i) = acosd(truel(i,:)*w);
    reproe(i) = acosd(dot([1;1;1]/sqrt(3),(truel(i,:)'./w)./norm(truel(i,:)'./w)));
end

rmpath('Grey-edge');