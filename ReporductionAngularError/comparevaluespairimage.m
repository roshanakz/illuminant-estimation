clear,clc
ge1 = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\SFU\SFUlab-1stGreyEdge-reproduction');
ge2 = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\SFU\SFUlab-2ndGrayEdge-reproduction');
 ge1_recov = squeeze(ge1.angular_errors_recovery(:,14,4));
ge1_repro = squeeze(ge1.angular_errors_reproduction(:,14,4));
ge2_recov = squeeze(ge2.angular_errors_recovery(:,15,10));
ge2_repro = squeeze(ge2.angular_errors_reproduction(:,15,10));

data = [ge1_recov ge2_recov];
hbar = bar(data);
%set(hbar(1),'facecolor',[1 0 0]);
%set(hbar(2),'facecolor',[0 1 0]);
%set(gca,'xticklabel',{'jan','feb','mar','apr','may'})
width = 0.5;
bar3([ge1_recov(1:10:end,:) ge2_recov(1:10:end,:) ge1_repro(1:10:end,:) ge2_repro(1:10:end,:)],width);
 nnz(find(ge1_recov>ge2_recov))
nnz(find(ge1_repro>ge2_repro))
%%
clear,clc
ge1 = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\SFU\SFUlab-GrayWorld-reproduction');
ge2 = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\SFU\SFUlab-MaxRGB-reproduction');
 ge1_recov = squeeze(ge1.angular_errors_recovery);
ge1_repro = squeeze(ge1.angular_errors_reproduction);
ge2_recov = squeeze(ge2.angular_errors_recovery);
ge2_repro = squeeze(ge2.angular_errors_reproduction);

data = [ge1_recov ge2_recov];
hbar = bar(data);
%set(hbar(1),'facecolor',[1 0 0]);
%set(hbar(2),'facecolor',[0 1 0]);
%set(gca,'xticklabel',{'jan','feb','mar','apr','may'})
width = 0.5;
bar3([ge1_recov(1:10:end,:) ge2_recov(1:10:end,:) ge1_repro(1:10:end,:) ge2_repro(1:10:end,:)],width)
 nnz(find(ge1_recov>ge2_recov))
nnz(find(ge1_repro>ge2_repro))
%%
clear,clc
ge1 = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\SFU\SFUlab-GrayWorld-reproduction');
ge2 = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\SFU\SFUlab-ShadesofGray-reproduction');
 ge1_recov = squeeze(ge1.angular_errors_recovery);
ge1_repro = squeeze(ge1.angular_errors_reproduction);
ge2_recov = squeeze(ge2.angular_errors_recovery(7,:));
ge2_repro = squeeze(ge2.angular_errors_reproduction(7,:));

data = [ge1_recov ge2_recov];
hbar = bar(data);
%set(hbar(1),'facecolor',[1 0 0]);
%set(hbar(2),'facecolor',[0 1 0]);
%set(gca,'xticklabel',{'jan','feb','mar','apr','may'})
width = 0.5;
bar3([ge1_recov(1:10:end,:) ge2_recov(1:10:end,:) ge1_repro(1:10:end,:) ge2_repro(1:10:end,:)],width)
 nnz(find(ge1_recov>ge2_recov))
nnz(find(ge1_repro>ge2_repro))
%%
clear,clc
ge1 = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\SFU\SFUlab-1stGreyEdge-reproduction');
ge2 = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\SFU\SFUlab-spatio-reproduction');
 ge1_recov = squeeze(ge1.angular_errors_recovery(:,14,4));
ge1_repro = squeeze(ge1.angular_errors_reproduction(:,14,4));
ge2_recov = squeeze(ge2.angular_errors_recovery(:,2,8));
ge2_repro = squeeze(ge2.angular_errors_reproduction(:,2,8));

data = [ge1_recov ge2_recov];
hbar = bar(data);
%set(hbar(1),'facecolor',[1 0 0]);
%set(hbar(2),'facecolor',[0 1 0]);
%set(gca,'xticklabel',{'jan','feb','mar','apr','may'})
width = 0.5;
bar3([ge1_recov(1:10:end,:) ge2_recov(1:10:end,:) ge1_repro(1:10:end,:) ge2_repro(1:10:end,:)],width)
 nnz(find(ge1_recov>ge2_recov))
nnz(find(ge1_repro>ge2_repro))
%%
clear,clc
ge1 = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\SFU\SFUlab-GamutPixelbased-reproduction');
ge2 = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\SFU\SFUlab-GamutEdgebased-reproduction');
 ge1_recov = squeeze(ge1.angular_errors_recovery(:,4));
ge1_repro = squeeze(ge1.angular_errors_reproduction(:,2));
ge2_recov = squeeze(ge2.angular_errors_recovery(:,4));
ge2_repro = squeeze(ge2.angular_errors_reproduction(:,2));

data = [ge1_recov ge2_recov];
hbar = bar(data);
%set(hbar(1),'facecolor',[1 0 0]);
%set(hbar(2),'facecolor',[0 1 0]);
%set(gca,'xticklabel',{'jan','feb','mar','apr','may'})
width = 0.5;
bar3([ge1_recov(1:10:end,:) ge2_recov(1:10:end,:) ge1_repro(1:10:end,:) ge2_repro(1:10:end,:)],width)
 nnz(find(ge1_recov>ge2_recov))
nnz(find(ge1_repro>ge2_repro))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% the main code using Wilcoxon test
% SFU dataset
clear,clc
ge1 = load('../../../publications/2015/PAMI_reproduction angular error/results/SFU/SFUlab-1stGreyEdge-reproduction.mat');
%ge2 = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\SFU\SFUlab-2ndGreyEdge-reproduction');
wge = load('../../../publications/2015/PAMI_reproduction angular error/results/SFU/SFUlab-spatio-reproduction.mat');
%sog = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\SFU\SFUlab-shadesofGray-reproduction.mat');
pg = load('../../../publications/2015/PAMI_reproduction angular error/results/SFU/SFUlab-gamutPixelbased-reproduction.mat');
eg = load('../../../publications/2015/PAMI_reproduction angular error/results/SFU/SFUlab-gamutEdgebased-reproduction.mat');
sog = load('../../../publications/2015/PAMI_reproduction angular error/results/SFU/SFUlab-ShadesofGray-reproduction.mat');
ht = load('../../../publications/2015/PAMI_reproduction angular error/results/SFU/SFUlab-illum-reproduction.mat');



ge1_recov = squeeze(ge1.angular_errors_recovery(:,7,4));
ge1_repro = squeeze(ge1.angular_errors_reproduction(:,14,4));
wge_recov = squeeze(wge.angular_errors_recovery(:,2,1));
wge_repro = squeeze(wge.angular_errors_reproduction(:,2,1));
pg_recov = squeeze(pg.angular_errors_recovery(:,4));
pg_repro = squeeze(pg.angular_errors_reproduction(:,4));
eg_recov = squeeze(eg.angular_errors_recovery(:,2,2));
eg_repro = squeeze(eg.angular_errors_reproduction(:,2,2));
sog_recov = squeeze(sog.angular_errors_recovery(:,5));
sog_repro = squeeze(sog.angular_errors_reproduction(:,5));

thresh = sum([29>ge1_recov>0.5 29>wge_recov>0.5  29>pg_recov>0.5 29>eg_recov>0 29>sog_recov>0.5 29>ht.angular_errors_recovery_ML>0.5],2);
%thresh = sum([3.7>ge1_recov>0.5 3.7>wge_recov>0.5  3.7>pg_recov>0.5 3.7>eg_recov>0.5 3.7>sog_recov>0.5 3.7>ht.angular_errors_recovery_ML>0.5],2);
%thresh = sum([4>ge1_recov>0 4>wge_recov>0  4>pg_recov>0 4>eg_recov>0 4>sog_recov>0 4>ht.angular_errors_recovery_ML>0],2);
%thresh = sum([ge1_repro>0.0 wge_repro>0.0 pg_repro>0.0 eg_repro>0.0 sog_repro>0.0 ht.angular_errors_reproduction_ML>0.00],2);

thresh_ind = find(thresh==6);

ge1_recov_thresh = ge1_recov(thresh_ind);
ge1_repro_thresh = ge1_repro(thresh_ind);
wge_recov_thresh = wge_recov(thresh_ind);
wge_repro_thresh = wge_repro(thresh_ind);
pg_recov_thresh = pg_recov(thresh_ind);
pg_repro_thresh = pg_repro(thresh_ind);
eg_recov_thresh = eg_recov(thresh_ind);
eg_repro_thresh = eg_repro(thresh_ind);
sog_recov_thresh = sog_recov(thresh_ind);
sog_repro_thresh = sog_repro(thresh_ind);
HT_recov_thresh = ht.angular_errors_recovery_ML(thresh_ind);
HT_repro_thresh = ht.angular_errors_reproduction_ML(thresh_ind);

disp('WGE SOG recovery'), disp(signrank(wge_recov_thresh,sog_recov_thresh))
disp('WGE SOG reproduction'), disp(signrank(wge_repro_thresh,sog_repro_thresh))

disp('GE1 HT recovery'), disp(signrank(ge1_recov_thresh,HT_recov_thresh))
disp('GE1 HT reproduction'), disp(signrank(ge1_repro_thresh,HT_repro_thresh))

disp('GE1 SOG recovery'), disp(signrank(ge1_recov_thresh,sog_recov_thresh))
disp('GE1 SOG reproduction'), disp(signrank(ge1_repro_thresh,sog_repro_thresh))

disp('GE1 WGE recovery'), disp(signrank(ge1_recov_thresh,wge_recov_thresh))
disp('GE1 WGE reproduction'), disp(signrank(ge1_repro_thresh,wge_repro_thresh))

disp('PG HT recovery'), disp(signrank(pg_recov_thresh,HT_recov_thresh))
disp('PG HT reprodcution'), disp(signrank(pg_repro_thresh,HT_repro_thresh))

disp('PG SOG recovery'), disp(signrank(pg_recov_thresh,sog_recov_thresh))
disp('PG SOG reproduction'), disp(signrank(pg_repro_thresh,sog_repro_thresh))

disp('PG WGE recovery'), disp(signrank(pg_recov_thresh,wge_recov_thresh))
disp('PG WGE reproduction'), disp(signrank(pg_repro_thresh,wge_repro_thresh))

disp('PG GE1 recovery'), disp(signrank(pg_recov_thresh,ge1_recov_thresh))
disp('PG GE1 reproduction'), disp(signrank(pg_repro_thresh, ge1_repro_thresh))

disp('EG HT recovery'), disp(signrank(eg_recov_thresh,HT_recov_thresh))
disp('EG HT reproduction'), disp(signrank(eg_repro_thresh, HT_repro_thresh))

disp('EG SOG recovery'), disp(signrank(eg_recov_thresh,sog_recov_thresh))
disp('EG SOG reproduction'), disp(signrank(eg_repro_thresh,sog_repro_thresh))

disp('EG WGE recovery'), disp(signrank(eg_recov_thresh,wge_recov_thresh))
disp('EG WGE reproduction'), disp(signrank(eg_repro_thresh,wge_repro_thresh))

disp('EG GE1 recovery'), disp(signrank(eg_recov_thresh,ge1_recov_thresh))
disp('EG GE1 reproduction'), disp(signrank(eg_repro_thresh,ge1_repro_thresh))

disp('EG PG recovery'), disp(signrank(eg_recov_thresh,pg_recov_thresh))
disp('EG PG reproduction'), disp(signrank(eg_repro_thresh,pg_repro_thresh))

disp('WGE HT recovery'), disp(signrank(wge_recov_thresh,HT_recov_thresh))
disp('WGE HT reproduction'), disp(signrank(wge_repro_thresh,HT_repro_thresh))

disp('SOG HT recovery'), disp(signrank(sog_recov_thresh,HT_recov_thresh))
disp('SOG HT reproduction'), disp(signrank(sog_repro_thresh,HT_repro_thresh))

%signrank(wge_repro_thresh,ge1_repro_thresh)
%signrank(wge_recov_thresh,ge1_recov_thresh)
%signrank(HT_repro_thresh,sog_repro_thresh)
%signrank(HT_recov_thresh,sog_recov_thresh)

%[p,h,stats] =ranksum(ge1_repro_thresh,wge_repro_thresh,'alpha',0.01,'tail','right')
%[p,h,stats] =ranksum(ge1_recov_thresh,wge_recov_thresh,'alpha',0.01,'tail','right')
%subplot(1,2,1),histfit(ge1_repro-wge_repro),ylim([0 110]),xlabel('Reproduction errors difference'),...
%    subplot(1,2,2),histfit(ge1_recov-wge_recov),ylim([0 110]),xlabel('Recovery errors difference'),title('1st Order Greyedge, Weighted greyedge')
%figure,subplot(1,2,1),histfit(pg_repro-eg_repro),ylim([0 140]),xlabel('Reproduction errors difference'),...
%    subplot(1,2,2),histfit(pg_recov-eg_recov),ylim([0 140]),xlabel('Recovery errors difference'),title('Pixel-based gamut, edge-based gamut')

%p = kruskalwallis([ge1_recov wge_recov pg_recov eg_recov])
%p = kruskalwallis([ge1_repro wge_repro pg_repro eg_repro])
%bar(ge1_recov-wge_recov,'facecolor',[1 0 0]), hold on, bar(ge1_repro-wge_repro,'facecolor',[0 0 1]),legend(num2str(nnz(find(ge1_recov>wge_recov))),num2str(nnz(find(ge1_repro>wge_repro))))
%figure,bar(eg_recov-pg_recov,'facecolor',[1 0 0]), hold on, bar(eg_repro-pg_repro,'facecolor',[0 0 1]),legend(num2str(nnz(find(eg_recov>pg_recov))),num2str(nnz(find(eg_repro>pg_repro))))
%% Wilcoxon on greyball
clear,clc
wp = load('../../../publications/2015/PAMI_reproduction angular error/results/greyball/SFUGrayball-MaxRGB-reproduction.mat');
gi = load('../../../publications/2015/PAMI_reproduction angular error/results/greyball/SFUGrayball-GamutIntersection-reproduction.mat');
pg = load('../../../publications/2015/PAMI_reproduction angular error/results/greyball/SFUGrayball-GamutPixelbased-reproduction.mat');
eg = load('../../../publications/2015/PAMI_reproduction angular error/results/greyball/SFUGrayball-GamutEdgebased-reproduction.mat');
sog = load('../../../publications/2015/PAMI_reproduction angular error/results/greyball/SFUGrayball-ShadesofGray-reproduction.mat');
iics = load('../../../publications/2015/PAMI_reproduction angular error/results/greyball/SFUGrayball-iic-reproduction.mat');



wp_recov = squeeze(wp.angular_errors_recovery);
wp_repro = squeeze(wp.angular_errors_reproduction);
gi_recov = squeeze(gi.angular_errors_recovery(:,6,1));
gi_repro = squeeze(gi.angular_errors_reproduction(:,2,1));
pg_recov = squeeze(pg.angular_errors_recovery(:,2));
pg_repro = squeeze(pg.angular_errors_reproduction(:,2));
eg_recov = squeeze(eg.angular_errors_recovery(:,1,2));
eg_repro = squeeze(eg.angular_errors_reproduction(:,1,2));
sog_recov = squeeze(sog.angular_errors_recovery(:,8));
sog_repro = squeeze(sog.angular_errors_reproduction(:,14));

thresh = sum([29>wp_recov>0.5 29>gi_recov>0.5  29>pg_recov>0.5 29>eg_recov>0 29>sog_recov>0.5 29>iics.angular_errors_recovery>0.5],2);

thresh_ind = find(thresh==6);

wp_recov_thresh = wp_recov(thresh_ind);
wp_repro_thresh = wp_repro(thresh_ind);
gi_recov_thresh = gi_recov(thresh_ind);
gi_repro_thresh = gi_repro(thresh_ind);
pg_recov_thresh = pg_recov(thresh_ind);
pg_repro_thresh = pg_repro(thresh_ind);
eg_recov_thresh = eg_recov(thresh_ind);
eg_repro_thresh = eg_repro(thresh_ind);
sog_recov_thresh = sog_recov(thresh_ind);
sog_repro_thresh = sog_repro(thresh_ind);
iics_recov_thresh = iics.angular_errors_recovery(thresh_ind);
iics_repro_thresh = iics.angular_errors_reproduction(thresh_ind);

disp('gi SOG recovery'), disp(signrank(gi_recov_thresh,sog_recov_thresh))
disp('gi SOG reproduction'), disp(signrank(gi_repro_thresh,sog_repro_thresh))

disp('wp iics recovery'), disp(signrank(wp_recov_thresh,iics_recov_thresh))
disp('wp iics reproduction'), disp(signrank(wp_repro_thresh,iics_repro_thresh))

disp('wp SOG recovery'), disp(signrank(wp_recov_thresh,sog_recov_thresh))
disp('wp SOG reproduction'), disp(signrank(wp_repro_thresh,sog_repro_thresh))

disp('wp gi recovery'), disp(signrank(wp_recov_thresh,gi_recov_thresh))
disp('wp gi reproduction'), disp(signrank(wp_repro_thresh,gi_repro_thresh))

disp('PG iics recovery'), disp(signrank(pg_recov_thresh,iics_recov_thresh))
disp('PG iics reprodcution'), disp(signrank(pg_repro_thresh,iics_repro_thresh))

disp('PG SOG recovery'), disp(signrank(pg_recov_thresh,sog_recov_thresh))
disp('PG SOG reproduction'), disp(signrank(pg_repro_thresh,sog_repro_thresh))

disp('PG gi recovery'), disp(signrank(pg_recov_thresh,gi_recov_thresh))
disp('PG gi reproduction'), disp(signrank(pg_repro_thresh,gi_repro_thresh))

disp('PG wp recovery'), disp(signrank(pg_recov_thresh,wp_recov_thresh))
disp('PG wp reproduction'), disp(signrank(pg_repro_thresh, wp_repro_thresh))

disp('EG iics recovery'), disp(signrank(eg_recov_thresh,iics_recov_thresh))
disp('EG iics reproduction'), disp(signrank(eg_repro_thresh, iics_repro_thresh))

disp('EG SOG recovery'), disp(signrank(eg_recov_thresh,sog_recov_thresh))
disp('EG SOG reproduction'), disp(signrank(eg_repro_thresh,sog_repro_thresh))

disp('EG gi recovery'), disp(signrank(eg_recov_thresh,gi_recov_thresh))
disp('EG gi reproduction'), disp(signrank(eg_repro_thresh,gi_repro_thresh))

disp('EG wp recovery'), disp(signrank(eg_recov_thresh,wp_recov_thresh))
disp('EG wp reproduction'), disp(signrank(eg_repro_thresh,wp_repro_thresh))

disp('EG PG recovery'), disp(signrank(eg_recov_thresh,pg_recov_thresh))
disp('EG PG reproduction'), disp(signrank(eg_repro_thresh,pg_repro_thresh))

disp('gi iics recovery'), disp(signrank(gi_recov_thresh,iics_recov_thresh))
disp('gi iics reproduction'), disp(signrank(gi_repro_thresh,iics_repro_thresh))

disp('SOG iics recovery'), disp(signrank(sog_recov_thresh,iics_recov_thresh))
disp('SOG iics reproduction'), disp(signrank(sog_repro_thresh,iics_repro_thresh))

%%
% subplot(2,1,1)
% boxplot([ eg_recov(80:1:90,:) pg_recov(80:1:90,:) ge1_recov(80:1:90,:) wge_recov(80:1:90,:)])
% subplot(2,1,2)
% boxplot([ eg_repro(80:1:90,:) pg_repro(80:1:90,:) ge1_repro(80:1:90,:) wge_repro(80:1:90,:)])
subplot(2,1,1)
boxplot([ eg_recov pg_recov ge1_recov wge_recov],'Labels',{'gamutEdge','pixelGamut','1stGreyedge','weightedGreyedge'})
subplot(2,1,2)
boxplot([ eg_repro pg_repro ge1_repro wge_repro],'Labels',{'gamutEdge','pixelGamut','1stGreyedge','weightedGreyedge'})

median([ eg_recov pg_recov ge1_recov wge_recov],1)
median([ eg_repro pg_repro ge1_repro wge_repro],1)

%signrank(eg_recov(70:1:90,:),pg_recov(70:1:90,:))
%signrank(eg_repro(70:1:90,:),pg_repro(70:1:90,:))
 nnz(find(eg_recov>pg_recov))
nnz(find(eg_repro>pg_repro))

%signrank(ge1_recov(70:1:90,:),wge_recov(70:1:90,:))
%signrank(ge1_repro(70:1:90,:),wge_repro(70:1:90,:))
nnz(find(ge1_recov>wge_recov))
nnz(find(ge1_repro>wge_repro))
%%
subplot(2,1,1)
boxplot([ eg_recov(70:1:90,:) pg_recov(70:1:90,:) ge1_recov(70:1:90,:) wge_recov(70:1:90,:)],'Labels',{'gamutEdge','pixelGamut','1stGreyedge','weightedGreyedge'})
subplot(2,1,2)
boxplot([ eg_repro(70:1:90,:) pg_repro(70:1:90,:) ge1_repro(70:1:90,:) wge_repro(70:1:90,:)],'Labels',{'gamutEdge','pixelGamut','1stGreyedge','weightedGreyedge'})

median([ eg_recov(70:1:90,:) pg_recov(70:1:90,:) ge1_recov(70:1:90,:) wge_recov(70:1:90,:)],1)
median([ eg_repro(70:1:90,:) pg_repro(70:1:90,:) ge1_repro(70:1:90,:) wge_repro(70:1:90,:)],1)

%signrank(eg_recov(70:1:90,:),pg_recov(70:1:90,:))
%signrank(eg_repro(70:1:90,:),pg_repro(70:1:90,:))
 nnz(find(eg_recov(70:1:90,:)>pg_recov(70:1:90,:)))
nnz(find(eg_repro(70:1:90,:)>pg_repro(70:1:90,:)))

%signrank(ge1_recov(70:1:90,:),wge_recov(70:1:90,:))
%signrank(ge1_repro(70:1:90,:),wge_repro(70:1:90,:))
nnz(find(ge1_recov(70:1:90,:)>wge_recov(70:1:90,:)))
nnz(find(ge1_repro(70:1:90,:)>wge_repro(70:1:90,:)))

%%
clear,clc

eg = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Canon1DsMkIII\Canon1DsMkIII_gamutedge.mat');
ge2 = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Canon1DsMkIII\Canon1DsMkIII_gt_secondgreyedge.mat');
ge1 = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Canon1DsMkIII\Canon1DsMkIII_gt_firstgreyedge.mat');
sog = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Canon1DsMkIII\Canon1DsMkIII_gt_shadesofgrey.mat');
Maxrgb = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Canon1DsMkIII\Canon1DsMkIII_gt_whitepatch.mat');
pg = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Canon1DsMkIII\Canon1DsMkIII_gamutpixel.mat');
gw = load('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Canon1DsMkIII\Canon1DsMkIII_gt_greyworld.mat');



ge1_recov = squeeze(ge1.recovery_error);
ge1_repro = squeeze(ge1.reproduction_error);
Maxrgb_recov = squeeze(Maxrgb.recovery_error);
Maxrgb_repro = squeeze(Maxrgb.reproduction_error);
sog_recov = squeeze(sog.recovery_error(:,5));
sog_repro = squeeze(sog.reproduction_error(:,5));
pg_recov = squeeze(pg.gp.angular_errors_recovery(:,3));
pg_repro = squeeze(pg.gp.angular_errors_reproduction(:,3));


thresh = sum([ge1_recov>5  pg_recov>5 Maxrgb_recov>5 sog_recov>5],2);
%thresh = sum([ge1_repro>0.5 wge_repro>0.5 pg_repro>0.5 eg_repro>0.5 sog_repro>0.5 ht.angular_errors_reproduction_ML>0.5],2);

thresh_ind = find(thresh==4);

ge1_recov_thresh = ge1_recov(thresh_ind);
ge1_repro_thresh = ge1_repro(thresh_ind);
%wge_recov_thresh = wge_recov(thresh_ind);
%wge_repro_thresh = wge_repro(thresh_ind);
pg_recov_thresh = pg_recov(thresh_ind);
pg_repro_thresh = pg_repro(thresh_ind);
Maxrgb_recov_thresh = Maxrgb_recov(thresh_ind);
Maxrgb_repro_thresh = Maxrgb_repro(thresh_ind);
sog_recov_thresh = sog_recov(thresh_ind);
sog_repro_thresh = sog_repro(thresh_ind);
%HT_recov_thresh = ht.angular_errors_recovery_ML(thresh_ind);
%HT_repro_thresh = ht.angular_errors_reproduction_ML(thresh_ind);
%%

ge2_recov = squeeze(ge2.recovery_error);
ge2_repro = squeeze(ge2.reproduction_error);
gw_recov = squeeze(gw.recovery_error);
gw_repro = squeeze(gw.reproduction_error);
eg_recov = squeeze(eg.ge.angular_errors_recovery(:,2));
eg_repro = squeeze(eg.ge.angular_errors_reproduction(:,2));
sog_recov = squeeze(sog.recovery_error(:,5));
sog_repro = squeeze(sog.reproduction_error(:,5));

bar(ge2_recov-sog_recov,'facecolor',[1 0 0]), hold on, bar(ge2_repro-sog_repro,'facecolor',[0 0 1]),legend(num2str(nnz(find(ge2_recov>sog_recov))),num2str(nnz(find(ge2_repro>sog_repro))))
figure,bar(gw_recov-eg_recov,'facecolor',[1 0 0]), hold on, bar(gw_repro-eg_repro,'facecolor',[0 0 1]),legend(num2str(nnz(find(gw_recov>eg_recov))),num2str(nnz(find(gw_repro>eg_repro))))
%%

% subplot(2,1,1)
% boxplot([ eg_recov(80:1:90,:) pg_recov(80:1:90,:) ge1_recov(80:1:90,:) wge_recov(80:1:90,:)])
% subplot(2,1,2)
% boxplot([ eg_repro(80:1:90,:) pg_repro(80:1:90,:) ge1_repro(80:1:90,:) wge_repro(80:1:90,:)])

subplot(2,1,1)
boxplot([ge2_recov sog_recov gw_recov eg_recov])
subplot(2,1,2)
boxplot([ge2_repro sog_repro gw_repro eg_repro])


signrank(ge2_recov,sog_recov)
signrank(ge2_repro,sog_repro)
 nnz(find(ge2_recov>sog_recov))
nnz(find(ge2_repro>sog_repro))

signrank(gw_recov,eg_recov)
signrank(gw_repro,eg_repro)
 nnz(find(gw_recov>eg_recov))
nnz(find(gw_repro>eg_repro))
























