%%%%%%% the maximum recovery angle for random lights inside the gamut of all
%%%%%%%     ground truth illuminant of some dataset (e.g. SFU dataset)
%%%%%%%             written by: Roshanak Zakizadeh May 2014             

%clear, clc,
clc,clear light lightkeep lightkeepnorm lightrg lightinGamut D estimatedD estimErrorOld 
load 'C:\ROSHANAK\DATA\GreyWorld\sfulab_greyworld';
groundtruth_illuminants=((groundtruth_illuminants./255));
groundtruth_illuminants_r = groundtruth_illuminants(:,1)./sum(groundtruth_illuminants,2);
groundtruth_illuminants_g = groundtruth_illuminants(:,2)./sum(groundtruth_illuminants,2);
groundtruth_illuminants_b = groundtruth_illuminants(:,3)./sum(groundtruth_illuminants,2);
% K1 = convhull(groundtruth_illuminants_r,groundtruth_illuminants_g,groundtruth_illuminants_b);
% trisurf(K1,groundtruth_illuminants_r,groundtruth_illuminants_g,groundtruth_illuminants_b,'Facecolor',[0.6875 0.8750 0.8984]),xlabel('r')
% ylabel('g'),zlabel('b')

K = convhull(groundtruth_illuminants_r,groundtruth_illuminants_g);
%figure(1),plot(groundtruth_illuminants_r(K),groundtruth_illuminants_g(K),'k-',groundtruth_illuminants_r,groundtruth_illuminants_g,'bx','LineWidth',.3),xlabel('r'),ylabel('g'), hold on,
light = rand(1000000,3);
lightnorm = arrayfun(@(idx) norm(light(idx,:)), 1:size(light,1));
ind = find(lightnorm <= 1);
lightkeep = light(ind,:);
lightkeepnorm = arrayfun(@(idx) norm(lightkeep(idx,:)), 1:size(lightkeep,1));
lightrg(:,1)=lightkeep(:,1)./lightkeepnorm';
lightrg(:,2)=lightkeep(:,2)./lightkeepnorm';
IN = inpolygon(lightrg(:,1),lightrg(:,2),groundtruth_illuminants_r(K),groundtruth_illuminants_g(K));
lightinGamut = lightrg(IN,:);
% figure(1),plot(lightinGamut (:,1),lightinGamut (:,2),'y+')
%hold off
groundtruth_illuminants_rgb = [groundtruth_illuminants_r,groundtruth_illuminants_g,groundtruth_illuminants_b];
lightinGamut(:,3)= ones(size(lightinGamut,1),1)- sum(lightinGamut,2);

for kk = 1:size(groundtruth_illuminants,1)
D = lightinGamut./repmat(groundtruth_illuminants_rgb(kk,:),size(lightinGamut,1),1);
estimatedD = repmat(estimated_illuminants(kk,:),size(lightinGamut,1),1).*D;
[estimErrorOld, estimErrorNew] = error_measure( lightinGamut,estimatedD);
%[estimErrorOld2, estimErrorNew2] = error_measure( ,estimatedD);
[maxerrorGW(kk),ind(kk)] = max(estimErrorOld);
%maxerrorGWnew(kk) = max(estimErrorNew);
%figure(kk),
%plot(groundtruth_illuminants_r(K),groundtruth_illuminants_g(K),'k-',groundtruth_illuminants_r,groundtruth_illuminants_g,'bx','LineWidth',.3),xlabel('r'),ylabel('g'), hold on,

%figure(1),plot(groundtruth_illuminants_r(K),groundtruth_illuminants_g(K),'k-',lightinGamut(:,1),lightinGamut(:,2),'ro'),xlabel('r'),ylabel('g'), hold on,
%[maxA,ind] = max(estimErrorOld);
%figure(kk),
figure(1),plot(lightinGamut(ind(kk),1),lightinGamut(ind(kk),2),'rs','LineWidth',4), hold on,
end

%figure(1),plot(groundtruth_illuminants_r(K),groundtruth_illuminants_g(K),'k-',lightinGamut(:,1),lightinGamut(:,2),'ro'),xlabel('r'),ylabel('g'), hold on,
%[maxA,ind] = max(estimErrorOld);
%figure(1),plot(lightinGamut(ind(kk),1),lightinGamut(ind(kk),2),'bs','LineWidth',4);

gammach = abs(sqrt(estimated_illuminants(:,1)./estimated_illuminants(:,3)));
light1 = [ones(size(gammach)),zeros(size(gammach)),gammach];
betach = abs(sqrt(estimated_illuminants(:,3)./estimated_illuminants(:,2)));
light2 = [zeros(size(betach)),betach,ones(size(betach))];
alphach = abs(sqrt(estimated_illuminants(:,2)./estimated_illuminants(:,1)));
light3 = [alphach,ones(size(alphach)),zeros(size(alphach))];

estimlightgamma = [estimated_illuminants(:,1),zeros(size(betach)),estimated_illuminants(:,3).*gammach];
[maxoldgamma]=error_measure( estimlightgamma, light1);
estimlightbeta = [zeros(size(betach)),estimated_illuminants(:,2).*betach,estimated_illuminants(:,3)];
[maxoldbeta]=error_measure( estimlightbeta, light2);
estimlightalpha = [estimated_illuminants(:,1).*alphach,estimated_illuminants(:,2),zeros(size(betach))];
[maxoldalpha]=error_measure( estimlightalpha, light3);

[estimErrorOldGW, estimErrorNewGW] = error_measure( groundtruth_illuminants, estimated_illuminants);
xGW = sort(estimErrorOldGW,1);
nGW = length(estimErrorOldGW);
pGW = (((1:nGW)-0.5)' ./ nGW);

figure(2),plot(pGW,xGW,'r-','LineWidth',3),hold on,plot(pGW,sort(maxerrorGW',1),'b-','LineWidth',3),...
    hold on,plot(pGW,sort(maxoldgamma,1),'-','LineWidth',3,'color',[0.9,0.05,0.94])
% ,hold on,stairs(sort(maxoldbeta,1),p,'b-'),...
%     hold on,stairs(sort(maxoldalpha,1),p,'g-');
ylabel('Recovery angular errors');
xlabel('Cumulative probability distribution'),hold on

xGWnew = sort(estimErrorNewGW,1);
nGWnew = length(estimErrorNewGW);
pGWnew = (((1:nGWnew)-0.5)' ./ nGWnew);
figure(3),plot(pGWnew,xGWnew,'r-','LineWidth',3),hold on,
    ylabel('Reproduction angular errors'),xlabel('Cumulative probability distribution'),hold on
  
%% Pixel Gamut

%clear, clc,
clc,clear light lightkeep lightkeepnorm lightrg lightinGamut D estimatedD estimErrorOld 
load 'C:\ROSHANAK\DATA\Gamut mapping\sfulab_gamut\sfulab_gamut_pixelbased';
groundtruth_illuminants=((groundtruth_illuminants./255));
groundtruth_illuminants_r = groundtruth_illuminants(:,1)./sum(groundtruth_illuminants,2);
groundtruth_illuminants_g = groundtruth_illuminants(:,2)./sum(groundtruth_illuminants,2);
groundtruth_illuminants_b = groundtruth_illuminants(:,3)./sum(groundtruth_illuminants,2);
K = convhull(groundtruth_illuminants_r,groundtruth_illuminants_g);
%figure(1),plot(groundtruth_illuminants_r(K),groundtruth_illuminants_g(K),'r-',groundtruth_illuminants_r,groundtruth_illuminants_g,'b+'),xlabel('r'),ylabel('g'), hold on,
light = rand(1000000,3);
lightnorm = arrayfun(@(idx) norm(light(idx,:)), 1:size(light,1));
ind = find(lightnorm <= 1);
lightkeep = light(ind,:);
lightkeepnorm = arrayfun(@(idx) norm(lightkeep(idx,:)), 1:size(lightkeep,1));
lightrg(:,1)=lightkeep(:,1)./lightkeepnorm';
lightrg(:,2)=lightkeep(:,2)./lightkeepnorm';
IN = inpolygon(lightrg(:,1),lightrg(:,2),groundtruth_illuminants_r(K),groundtruth_illuminants_g(K));
lightinGamut = lightrg(IN,:);
%figure(1),plot(lightinGamut (:,1),lightinGamut (:,2),'g*')
%hold off
groundtruth_illuminants_rgb = [groundtruth_illuminants_r,groundtruth_illuminants_g,groundtruth_illuminants_b];
lightinGamut(:,3)= ones(size(lightinGamut,1),1)- sum(lightinGamut,2);


for kk = 1:size(groundtruth_illuminants,1)
D = lightinGamut./repmat(groundtruth_illuminants_rgb(kk,:),size(lightinGamut,1),1);
estimatedD = repmat(reshape(estimated_illuminants(kk,4,:),1,3),size(lightinGamut,1),1).*D;
[estimErrorOld, estimErrorNew] = error_measure( lightinGamut,estimatedD);
%[estimErrorOld2, estimErrorNew2] = error_measure( ,estimatedD);
maxerrorGP(kk) = max(estimErrorOld);
maxerrorGPnew(kk) = max(estimErrorNew);
end

gammach_GP = abs(sqrt(estimated_illuminants(:,4,1)./estimated_illuminants(:,4,3)));
light1_GP = [ones(size(gammach_GP)),zeros(size(gammach_GP)),gammach_GP];
betach_GP = abs(sqrt(estimated_illuminants(:,4,3)./estimated_illuminants(:,4,2)));
light2_GP = [zeros(size(betach_GP)),betach_GP,ones(size(betach_GP))];
alphach_GP = abs(sqrt(estimated_illuminants(:,4,2)./estimated_illuminants(:,4,1)));
light3_GP = [betach_GP,ones(size(betach_GP)),zeros(size(betach_GP))];

estimlightgamma_GP = [estimated_illuminants(:,4,1),zeros(size(betach_GP)),estimated_illuminants(:,4,3).*gammach_GP];
[maxoldgamma_GP]=error_measure( estimlightgamma_GP, light1_GP);
estimlightbeta_GP = [zeros(size(betach_GP)),estimated_illuminants(:,4,2).*betach_GP,estimated_illuminants(:,4,3)];
[maxoldbeta_GP]=error_measure( estimlightbeta_GP, light2_GP);
estimlightalpha_GP = [estimated_illuminants(:,4,1).*betach_GP,estimated_illuminants(:,4,2),zeros(size(betach_GP))];
[maxoldalpha_GP]=error_measure( estimlightalpha_GP, light3_GP);

[estimErrorOldGamutP, estimErrorNewGamutP] = error_measure( groundtruth_illuminants, estimated_illuminants(:,4,:));
xGP = sort(estimErrorOldGamutP,1);
nGP = length(estimErrorOldGamutP);
pGP = (((1:nGP)-0.5)' ./ nGP);

figure(2),plot(pGP,xGP,'--r','LineWidth',3),hold on,plot(pGP,sort(maxerrorGP',1),'--b','LineWidth',3),...
    hold on,plot(pGP,sort(maxoldgamma_GP,1),'--','LineWidth',3,'color',[0.9,0.05,0.94])
% ,hold on,stairs(sort(maxoldbeta,1),p,'b-'),...
%     hold on,stairs(sort(maxoldalpha,1),p,'g-');
ylabel('Recovery angular errors');
xlabel('Cumulative probability distribution'), hold on,
h=legend('actual error for SFU lights (Gray-world) ','maximum error for convex set lights (Gray-world)','maximum error for analytical lights (Gray-world)',...
    'actual error for SFU lights (Pixel-based gamut)','maximum error for convex set lights (Pixel-based gamut)','maximum error for analytical lights (Pixel-based gamut)'),
% h=legend('maximum error for analytical lights (Gray-world)','maximum error for analytical lights (Pixel-based gamut)','maximum error for convex set lights (Gray-world)',...
%     'maximum error for convex set lights (Pixel-based gamut)','actual error for SFU lights (Gray-world) ','actual error for SFU lights (Pixel-based gamut)'),
%reorderLegend([3,6,2,5,1,4]) 
reorderLegend([4,1,5,2,6,3]) 
figureHandle = gcf;
set(findall(figureHandle,'type','text'),'FontName','Times New Roman','fontSize',11)

xGPnew = sort(estimErrorNewGamutP,1);
nGPnew = length(estimErrorNewGamutP);
pGPnew = (((1:nGWnew)-0.5)' ./ nGWnew);
figure(3),plot(pGPnew,xGPnew,'--r','LineWidth',3)
ylabel('Reproduction angular errors');
xlabel('Cumulative probability distribution'), hold on,
h=legend('Reproduction error for Gray-world','Reproduction error for Pixel-based gamut'),
   figureHandle = gcf;
set(findall(figureHandle,'type','text'),'FontName','Times New Roman','fontSize',11)

%% MaxRGB

%clear, clc,
clc,clear light lightkeep lightkeepnorm lightrg lightinGamut D estimatedD estimErrorOld 
load 'U:\Roshanak\CODE\colour constancy\Data\White-Patch\sfulab_whitepatch';
groundtruth_illuminants=((groundtruth_illuminants./255));
groundtruth_illuminants_r = groundtruth_illuminants(:,1)./sum(groundtruth_illuminants,2);
groundtruth_illuminants_g = groundtruth_illuminants(:,2)./sum(groundtruth_illuminants,2);
groundtruth_illuminants_b = groundtruth_illuminants(:,3)./sum(groundtruth_illuminants,2);
K = convhull(groundtruth_illuminants_r,groundtruth_illuminants_g);
%figure(1),plot(groundtruth_illuminants_r(K),groundtruth_illuminants_g(K),'r-',groundtruth_illuminants_r,groundtruth_illuminants_g,'b+'),xlabel('r'),ylabel('g'), hold on,
light = rand(1000000,3);
lightnorm = arrayfun(@(idx) norm(light(idx,:)), 1:size(light,1));
ind = find(lightnorm <= 1);
lightkeep = light(ind,:);
lightkeepnorm = arrayfun(@(idx) norm(lightkeep(idx,:)), 1:size(lightkeep,1));
lightrg(:,1)=lightkeep(:,1)./lightkeepnorm';
lightrg(:,2)=lightkeep(:,2)./lightkeepnorm';
IN = inpolygon(lightrg(:,1),lightrg(:,2),groundtruth_illuminants_r(K),groundtruth_illuminants_g(K));
lightinGamut = lightrg(IN,:);
%figure(1),plot(lightinGamut (:,1),lightinGamut (:,2),'g*')
%hold off
groundtruth_illuminants_rgb = [groundtruth_illuminants_r,groundtruth_illuminants_g,groundtruth_illuminants_b];
lightinGamut(:,3)= ones(size(lightinGamut,1),1)- sum(lightinGamut,2);


for kk = 1:size(groundtruth_illuminants,1)
D = lightinGamut./repmat(groundtruth_illuminants_rgb(kk,:),size(lightinGamut,1),1);
estimatedD = repmat(estimated_illuminants(kk,:),size(lightinGamut,1),1).*D;
[estimErrorOld, estimErrorNew] = error_measure( lightinGamut,estimatedD);
%[estimErrorOld2, estimErrorNew2] = error_measure( ,estimatedD);
maxerrorrgb(kk) = max(estimErrorOld);
end

[estimErrorOldMRGB, estimErrorNewMRGB] = error_measure( groundtruth_illuminants, estimated_illuminants);
xrgb = sort(estimErrorOldMRGB,1);
nrgb = length(estimErrorOldMRGB);
prgb = (((1:nrgb)-0.5)' ./ nrgb);
figure(2),stairs(prgb,xrgb,'m-','LineWidth',10,'LineWidth',2),hold on,stairs(prgb,sort(maxerrorrgb',1),'c-','LineWidth',2)
% ,hold on,stairs(sort(maxoldbeta,1),p,'b-'),...
%     hold on,stairs(sort(maxoldalpha,1),p,'g-');
ylabel('Recovery angular errors'),
xlabel('Cumulative probability (p)'), 
%% GammutEdge

%clear, clc,
clc,clear light lightkeep lightkeepnorm lightrg lightinGamut D estimatedD estimErrorOld 
load 'U:\Roshanak\CODE\colour constancy\Data\Gamut mapping\sfulab_gamut\sfulab_gamut_edgebased';
groundtruth_illuminants=((groundtruth_illuminants./255));
groundtruth_illuminants_r = groundtruth_illuminants(:,1)./sum(groundtruth_illuminants,2);
groundtruth_illuminants_g = groundtruth_illuminants(:,2)./sum(groundtruth_illuminants,2);
groundtruth_illuminants_b = groundtruth_illuminants(:,3)./sum(groundtruth_illuminants,2);
K = convhull(groundtruth_illuminants_r,groundtruth_illuminants_g);
%figure(1),plot(groundtruth_illuminants_r(K),groundtruth_illuminants_g(K),'r-',groundtruth_illuminants_r,groundtruth_illuminants_g,'b+'),xlabel('r'),ylabel('g'), hold on,
light = rand(1000000,3);
lightnorm = arrayfun(@(idx) norm(light(idx,:)), 1:size(light,1));
ind = find(lightnorm <= 1);
lightkeep = light(ind,:);
lightkeepnorm = arrayfun(@(idx) norm(lightkeep(idx,:)), 1:size(lightkeep,1));
lightrg(:,1)=lightkeep(:,1)./lightkeepnorm';
lightrg(:,2)=lightkeep(:,2)./lightkeepnorm';
IN = inpolygon(lightrg(:,1),lightrg(:,2),groundtruth_illuminants_r(K),groundtruth_illuminants_g(K));
lightinGamut = lightrg(IN,:);
%figure(1),plot(lightinGamut (:,1),lightinGamut (:,2),'g*')
%hold off
groundtruth_illuminants_rgb = [groundtruth_illuminants_r,groundtruth_illuminants_g,groundtruth_illuminants_b];
lightinGamut(:,3)= ones(size(lightinGamut,1),1)- sum(lightinGamut,2);


for kk = 1:size(groundtruth_illuminants,1)
D = lightinGamut./repmat(groundtruth_illuminants_rgb(kk,:),size(lightinGamut,1),1);
estimatedD = repmat(reshape(estimated_illuminants(kk,2,2,:),1,3),size(lightinGamut,1),1).*D;
[estimErrorOld, estimErrorNew] = error_measure( lightinGamut,estimatedD);
%[estimErrorOld2, estimErrorNew2] = error_measure( ,estimatedD);
maxerrorGamE(kk) = max(estimErrorOld);
end

[estimErrorOldGamE, estimErrorNewGamE] = error_measure( groundtruth_illuminants, estimated_illuminants(:,2,2,:));
xGamE = sort(estimErrorOldGamE,1);
nGamE = length(estimErrorOldGamE);
pGamE = (((1:nGamE)-0.5)' ./ nGamE);
figure(2),stairs(pGamE,xGamE,'-','LineWidth',10,'LineWidth',2,'color',[0.9,0.5,0.05]),...
    hold on,stairs(pGamE,sort(maxerrorGamE',1),'-','LineWidth',2,'color',[0.6,0.4,1])
% ,hold on,stairs(sort(maxoldbeta,1),p,'b-'),...
%     hold on,stairs(sort(maxoldalpha,1),p,'g-');
ylabel('Recovery angular errors'),
xlabel('Cumulative probability (p)'), 

h=legend('actual angular errors GW ','maximum angular errors GW','actual angular errors PG','maximum angular errors PG',...
   'actual angular errors MaxRGB','maximum angular errors MaxRGB','actual angular errors GamutEdge','maximum angular errors GamutEdge'),grid on
   figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',10,'fontWeight','bold')

argb = trapz(prgb,sort(maxerrorrgb',1))-trapz(prgb,xrgb)
aGP = trapz(pGP,sort(maxerrorGP',1))-trapz(pGP,xGP)
aGW = trapz(pGW,sort(maxerrorGW',1))-trapz(pGW,xGW)
aGamE = trapz(pGamE,sort(maxerrorGamE',1))-trapz(pGamE,xGamE)

%%
clear, clc,
load 'U:\Roshanak\CODE\colour constancy\Data\GreyWorld\sfulab_greyworld';
groundtruth_illuminants=((groundtruth_illuminants./255));
groundtruth_illuminants_r = groundtruth_illuminants(:,1)./sum(groundtruth_illuminants,2);
groundtruth_illuminants_g = groundtruth_illuminants(:,2)./sum(groundtruth_illuminants,2);
groundtruth_illuminants_b = groundtruth_illuminants(:,3)./sum(groundtruth_illuminants,2);
K = convhull(groundtruth_illuminants_r,groundtruth_illuminants_g);
figure(1),plot(groundtruth_illuminants_r(K),groundtruth_illuminants_g(K),'k-',groundtruth_illuminants_r,groundtruth_illuminants_g,'ro'),xlabel('r'),ylabel('g'), hold on,
estimated_illuminants=estimated_illuminants./255;
estimated_illuminants_r = estimated_illuminants(:,1)./sum(estimated_illuminants,2);
estimated_illuminants_g = estimated_illuminants(:,2)./sum(estimated_illuminants,2);
estimated_illuminants_b = estimated_illuminants(:,3)./sum(estimated_illuminants,2);
figure(1),plot(estimated_illuminants_r,estimated_illuminants_g,'bo'),xlabel('r'),ylabel('g'),hold on,
[estimErrorOld, estimErrorNew] = error_measure( groundtruth_illuminants, estimated_illuminants);
[maxA,ind] = max(estimErrorOld);
figure(1),line([groundtruth_illuminants_r(ind) estimated_illuminants_r(ind)],[groundtruth_illuminants_g(ind) estimated_illuminants_g(ind)],'Color',[0.5 0.5 0], 'LineWidth',2)