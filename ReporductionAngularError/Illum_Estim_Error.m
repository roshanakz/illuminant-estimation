function [ EstimErrorOld, EstimErrorNew ] = Illum_Estim_Error( estimated_illuminants,groundtruth_illuminants )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
[nimages,r,c,channels]= size(estimated_illuminants);
EstimErrorOld =zeros(nimages,r,c);
EstimErrorOld = reshape(EstimErrorOld, nimages, r*c);

TrueIlluminant(:,:,1) = repmat(groundtruth_illuminants(:,1),[1 56]);
TrueIlluminant(:,:,2) = repmat(groundtruth_illuminants(:,2),[1 56]);
TrueIlluminant(:,:,3) = repmat(groundtruth_illuminants(:,3),[1 56]);

D65=[1;1;1];
EstimErrorNew = zeros(nimages,r,c);
EstimErrorNew = reshape(EstimErrorNew, nimages, r*c);

est1=estimated_illuminants;
est1=reshape(est1,nimages,r*c,3);

 for imageno = 1:nimages
     for samp = 1:(r*c)
             
             % old angular error
             normTrueIlluminant =TrueIlluminant(imageno,samp,:)/norm(reshape(TrueIlluminant(imageno,samp,:),1,3));
             normest1 =est1(imageno,samp,:)/norm(reshape(est1(imageno,samp,:),1,3));
             EstimErrorOld(imageno,samp) = acosd(dot(normTrueIlluminant,normest1));
             
             % new angular error
             D = diag(D65)/diag(groundtruth_illuminants(imageno,:));
             estTrans = D*(reshape(est1(imageno,samp,:),1,3))';
             EstimErrorNew(imageno,samp)= acosd(dot((D65/norm(D65)),(estTrans/norm(estTrans))));
     end
 end
 
 EstimErrorOld = reshape(EstimErrorOld,nimages,r,c);
 EstimErrorNew = reshape(EstimErrorNew,nimages,r,c);


