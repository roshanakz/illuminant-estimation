%RZ-- 31/03/2014 ---%

%% best algos according to new and old errors (mean, median, trimean) for shi
%%
%--------------------------------------------- whitepatch-----------------%
clear,clc,
% load 'MaxRGB-Shi_processed';
%load 'MaxRGB-SFU_processed';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-MaxRGB-reverse';
%load 'U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-MaxRGB-reverse';
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
%medianOld = nanmedian(angular_errors_recovery)
%estimOld95Q = quantile(angular_errors_recovery,0.95)
estimnew95Q = quantile(angular_errors_reproduction,0.95)
%maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
%meanold = mean(angular_errors_recovery)
meannew = mean(angular_errors_reproduction)
%%
%---------------------- Grey world --------------------------------------%
clear,clc,
%load 'Grey-World-Shi_processed';
%load 'Grey-World-SFU_processed';
%load 'U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GrayWorld-reverse';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-GrayWorld-reverse';
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
%medianOld = nanmedian(angular_errors_recovery)
%estimOld95Q = quantile(angular_errors_recovery,0.95)
estimnew95Q = quantile(angular_errors_reproduction,0.95)
%maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
%meanold = mean(angular_errors_recovery)
meannew = mean(angular_errors_reproduction)
%%
%------------------- SVR ------------------------------------------------%
clear,
%load 'SVR-Shi_processed';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-regression-reverse';
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
%medianOld = nanmedian(angular_errors_recovery)
%estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
%maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
%meanold = nanmean(angular_errors_recovery)
meannew = nanmean(angular_errors_reproduction)
%%
%------------------- iic ------------------------------------------------%
clear,clc,
 %load 'iic-SFU_processed';
%load 'iic-Shi_processed';
load 'U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-iic-reverse';
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
%medianOld = nanmedian(angular_errors_recovery)
%estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
%maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
%meanold = nanmean(angular_errors_recovery)
meannew = nanmean(angular_errors_reproduction)
%%
%------------------------- guassian -------------------------------------%
clear,
%load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-spatcorr_reg-reproduction';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-spatcorr_reg-reverse';
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
%medianOld = nanmedian(angular_errors_recovery)
%estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
%maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
%meanold = nanmean(angular_errors_recovery)
meannew = nanmean(angular_errors_reproduction)
%%
%--------------- Heavy tailed-based -------------------------------------%
clear,
%load 'illum-SFU_processed';
%load 'illum-Shi_processed';
%load 'U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-illum-reverse';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-illum-reverse';
%estimOldQ = quantile(angular_errors_recovery_ML,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction_ML,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction_ML)
%medianOld = nanmedian(angular_errors_recovery_ML)
%estimOld75Q = quantile(angular_errors_recovery_ML,0.95)
estimnew75Q = quantile(angular_errors_reproduction_ML,0.95)
%maxold = max(angular_errors_recovery_ML)
maxnew = max(angular_errors_reproduction_ML)
%meanold = mean(angular_errors_recovery_ML)
meannew = mean(angular_errors_reproduction_ML)
%%
%--------------- Bayesian -------------------------------------%
clear,clc,
%load 'illum-SFU_processed';
%load 'bayesian-Shi_processed';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-bayesian-reverse';
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
%medianOld = nanmedian(angular_errors_recovery)
%estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
%maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
%meanold = mean(angular_errors_recovery)
meannew = mean(angular_errors_reproduction)
%%
%------------------ bottom-up -------------------------------------------%
clear,
%load 'illum-SFU_processed';
%load 'hvi_bu-Shi_processed';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-hvi_bu-reverse';
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
%medianOld = nanmedian(angular_errors_recovery)
%estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
%maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
%meanold = mean(angular_errors_recovery)
meannew = mean(angular_errors_reproduction)
%%
%------------------ top-down -------------------------------------------%
clear,
%load 'illum-SFU_processed';
%load 'hvi_td-Shi_processed';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-hvi_td-reverse';
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
%medianOld = nanmedian(angular_errors_recovery)
%estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
%maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
%meanold = mean(angular_errors_recovery)
meannew = mean(angular_errors_reproduction)
%%
%------------------ Bottom-up+top-down ----------------------------------%
clear,
%load 'illum-SFU_processed';
%load 'hvi_combination-Shi_processed';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-hvi_combination-reverse';
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
%medianOld = nanmedian(angular_errors_recovery)
%estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
%maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
%meanold = mean(angular_errors_recovery)
meannew = mean(angular_errors_reproduction)
%%
%------------------ Using natural images --------------------------------%
clear,clc,
%load 'illum-SFU_processed';
%load '_ccnis-Shi_processed';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-ccnis-reverse';
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
%medianOld = nanmedian(angular_errors_recovery)
%estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
%maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
%meanold = mean(angular_errors_recovery)
meannew = mean(angular_errors_reproduction)
%%
%------------------ cart-based selection AAC; combination ----------------%
clear,
%load 'illum-SFU_processed';
%load 'AAC-Shi_processed';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-AAS_AAC-reverse';
%estimOldQ = quantile(angular_errors_recovery_AAS,[0.25, 0.5, 0.75]);
estimNewQ_AAS = quantile(angular_errors_reproduction_AAS,[0.25, 0.5, 0.75]);
estimNewQ_AAC = quantile(angular_errors_reproduction_AAC,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean_AAS = 0.25*estimNewQ_AAS(1)+0.5*estimNewQ_AAS(2)+0.25*estimNewQ_AAS(3)
estimNewtrimean_AAC = 0.25*estimNewQ_AAC(1)+0.5*estimNewQ_AAC(2)+0.25*estimNewQ_AAC(3)
mediannew_AAS = nanmedian(angular_errors_reproduction_AAS)
mediannew_AAC = nanmedian(angular_errors_reproduction_AAC)
%medianOld = nanmedian(angular_errors_recovery)
%estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q_AAS = quantile(angular_errors_reproduction_AAS,0.95)
estimnew75Q_AAC = quantile(angular_errors_reproduction_AAC,0.95)
%maxold = max(angular_errors_recovery)
maxnew_AAS = max(angular_errors_reproduction_AAS)
maxnew_AAC = max(angular_errors_reproduction_AAC)
%meanold = mean(angular_errors_recovery)
meannew_AAS = mean(angular_errors_reproduction_AAS)
meannew_AAC = mean(angular_errors_reproduction_AAC)
%%
%------------------ cart-based selection AAS ----------------%
clear,
load 'illum-SFU_processed';
%load 'AAS-Shi_processed';
estimOldQ = quantile(estimErrorOld_AAS,[0.25, 0.5, 0.75]);
estimNewQ = quantile(estimErrorNew_AAS,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(estimErrorNew_AAS)
medianOld = nanmedian(estimErrorOld_AAS)
estimOld75Q = quantile(estimErrorOld_AAS,0.95)
estimnew75Q = quantile(estimErrorNew_AAS,0.95)
maxold = max(estimErrorOld_AAS)
maxnew = max(estimErrorNew_AAS)
meanold = mean(estimErrorOld_AAS)
meannew = mean(estimErrorNew_AAS)
%%
%------------------ Exemplar-based CC ----------------%
clear,clc,
%load 'illum-SFU_processed';
%load 'exemplarCC-Shi_processed';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-exemplarCC-reverse';
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
%medianOld = nanmedian(angular_errors_recovery)
%estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
%maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
%meanold = mean(angular_errors_recovery)
meannew = mean(angular_errors_reproduction)
%%
%---------------------- wge: weighted grey edge--------------------------%
clear,clc,
load 'U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-wge-reproduction';
%load 'wge-SFU_processed';
% [r1,c1]=find(reshape(mean(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(mean(angular_errors_recovery,1))))
[r2,c2]=find(reshape(mean(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
    min(min(mean(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(nanmedian(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(nanmedian(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(nanmedian(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(nanmedian(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(quantile(angular_errors_recovery,0.95),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%      min(min(quantile(angular_errors_recovery,0.95))))
% [r2,c2]=find(reshape(quantile(angular_errors_reproduction,0.95),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%      min(min(quantile(angular_errors_reproduction,0.95))))
% [r1,c1]=find(reshape(max(angular_errors_recovery,[],1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(max(angular_errors_recovery,[],1))))
% [r2,c2]=find(reshape(max(angular_errors_reproduction,[],1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(max(angular_errors_reproduction,[],1))))
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
% estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
% [r1,c1]=find(reshape(estimOldtrimean,size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(estimOldtrimean)))
% [r2,c2]=find(reshape(estimNewtrimean,size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(estimNewtrimean)))
% medianOld = nanmedian(angular_errors_recovery,1);
% medianOld(1,r1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
mednew = medianNew(1,r2,c2)
% estimOld95Q = quantile(angular_errors_recovery,0.95);
% estimOld95Q(1,r1,c1)
estimNew95Q = quantile(angular_errors_reproduction,0.95);
new95q = estimNew95Q(1,r2,c2)
% maxold = max(angular_errors_recovery,[],1);
% maxold(1,r1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maximanew = maxnew(1,r2,c2)
% meanOld = nanmean(angular_errors_recovery,1);
% meanOld(1,r1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meannewf = meanNew(1,r2,c2)
%%
%---------------------- GamutEdge ---------------------------------------%
clear,clc,
%load 'GamutEdge-Shi_processed';
%load 'GamutEdge-SFU_processed';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-GamutEdgebased-reverse';
%load 'U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GamutEdgebased-reverse';
% [r1,c1]=find(reshape(mean(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(mean(angular_errors_recovery,1))));
% [r2,c2]=find(reshape(mean(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(mean(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(nanmedian(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(nanmedian(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(nanmedian(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(nanmedian(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(quantile(angular_errors_recovery,0.95),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%      min(min(quantile(angular_errors_recovery,0.95))))
% [r2,c2]=find(reshape(quantile(angular_errors_reproduction,0.95),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%      min(min(quantile(angular_errors_reproduction,0.95))))
% [r1,c1]=find(reshape(max(angular_errors_recovery,[],1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(max(angular_errors_recovery,[],1))))
[r2,c2]=find(reshape(max(angular_errors_reproduction,[],1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
    min(min(max(angular_errors_reproduction,[],1))))
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
%estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
% estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
% [r1,c1]=find(reshape(estimOldtrimean,size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(estimOldtrimean)))
% [r2,c2]=find(reshape(estimNewtrimean,size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(estimNewtrimean)))
% medianOld = nanmedian(angular_errors_recovery,1);
% medianOld(1,r1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
mednew = medianNew(1,r2,c2)
% estimOld75Q = quantile(angular_errors_recovery,0.95);
% estimOld75Q(1,r1,c1)
estimNew75Q = quantile(angular_errors_reproduction,0.95);
new75q = estimNew75Q(1,r2,c2)
% maxold = max(angular_errors_recovery,[],1);
% maxold(1,r1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maximanew = maxnew(1,r2,c2)
% meanOld = nanmean(angular_errors_recovery,1);
% meanOld(1,r1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meannewf = meanNew(1,r2,c2)
%%
%-------------------- Gamutpixel -----------------------------------------%
clear,clc,
%load 'Gamutpixel-Shi_processed';
%load 'GamutPixel-SFU_processed';
%load 'U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GamutPixelbased-reverse';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-GamutPixelbased-reverse';
% r1=find((mean(angular_errors_recovery,1))== min(mean((angular_errors_recovery),1)))
% r2=find((mean(angular_errors_reproduction,1))== min(mean((angular_errors_reproduction),1)))
% r1=find((nanmedian(angular_errors_recovery,1))== min(nanmedian((angular_errors_recovery),1)))
% r2=find((nanmedian(angular_errors_reproduction,1))== min(nanmedian((angular_errors_reproduction),1)))
% r1=find((quantile(angular_errors_recovery,0.95))== min(quantile(angular_errors_recovery,0.95)))
% r2=find((quantile(angular_errors_reproduction,0.95))== min(quantile(angular_errors_reproduction,0.95)))
% r1=find(max((angular_errors_recovery),[],1)== min(max((angular_errors_recovery),[],1)))
% r2=find(max((angular_errors_reproduction),[],1)== min(max((angular_errors_reproduction),[],1)))
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,r1)+0.5*estimOldQ(2,r1)+0.25*estimOldQ(3,r1)
%estimNewtrimean = 0.25*estimNewQ(1,r2)+0.5*estimNewQ(2,r2)+0.25*estimNewQ(3,r2)
% estimOldtrimean = 0.25*estimOldQ(1,:)+0.5*estimOldQ(2,:)+0.25*estimOldQ(3,:);
 estimNewtrimean = 0.25*estimNewQ(1,:)+0.5*estimNewQ(2,:)+0.25*estimNewQ(3,:);
% r1=find((estimOldtrimean)== min(estimOldtrimean))
 r2=find((estimNewtrimean)== min(estimNewtrimean))
% medianOld = nanmedian(angular_errors_recovery,1);
% medianOld(1,r1)
medianNew = nanmedian(angular_errors_reproduction,1);
mednew = medianNew(1,r2)
% estimOld75Q = quantile(angular_errors_recovery,0.95);
% estimOld75Q(1,r1)
estimNew75Q = quantile(angular_errors_reproduction,0.95);
new95q = estimNew75Q(1,r2)
% maxold = max(angular_errors_recovery,[],1);
% maxold(1,r1)
maxnew = max(angular_errors_reproduction,[],1);
maximanew = maxnew(1,r2)
% meanOld = nanmean(angular_errors_recovery,1);
% meanOld(1,r1)
meanNew = nanmean(angular_errors_reproduction,1);
meannewf = meanNew(1,r2)
%%
%----------------- Gamutinter ------------------------------------------%
clear,clc,
%load 'Gamutinter-Shi_processed';
%load 'Gamutinter-SFU_processed';
%load 'U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GamutIntersection-reverse';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-GamutIntersection-reverse';
% [r1,c1]=find(reshape(mean(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(mean(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(mean(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(mean(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(nanmedian(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(nanmedian(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(nanmedian(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(nanmedian(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(quantile(angular_errors_recovery,0.95),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(quantile(angular_errors_recovery,0.95))))
% [r2,c2]=find(reshape(quantile(angular_errors_reproduction,0.95),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(quantile(angular_errors_reproduction,0.95))))
% [r1,c1]=find(reshape(max(angular_errors_recovery,[],1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(max(angular_errors_recovery,[],1))))
[r2,c2]=find(reshape(max(angular_errors_reproduction,[],1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
    min(min(max(angular_errors_reproduction,[],1))))
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
%estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
 estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
% [r1,c1]=find(reshape(estimOldtrimean,size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(estimOldtrimean)))
[r2,c2]=find(reshape(estimNewtrimean,size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
    min(min(estimNewtrimean)))
% medianOld = nanmedian(angular_errors_recovery,1);
% medianOld(1,r1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
mednew = medianNew(1,r2,c2)
% estimOld95Q = quantile(angular_errors_recovery,0.95);
% estimOld95Q(1,r1,c1)
estimNew95Q = quantile(angular_errors_reproduction,0.95);
new95q = estimNew95Q(1,r2,c2)
% maxold = max(angular_errors_recovery,[],1);
% maxold(1,r1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maximanew = maxnew(1,r2,c2)
% meanOld = nanmean(angular_errors_recovery,1);
% meanOld(1,r1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meannewf = meanNew(1,r2,c2)
%%
%----------- Union-based gamut ------------------------------------------%
clear,clc,
%load 'GamutUnion-SFU_processed';
load 'U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GamutUnion-reverse';
% [r1,c1]=find(reshape(mean(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(mean(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(mean(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(mean(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(nanmedian(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(nanmedian(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(nanmedian(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(nanmedian(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(quantile(angular_errors_recovery,0.95),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%      min(min(quantile(angular_errors_recovery,0.95))))
% [r2,c2]=find(reshape(quantile(angular_errors_reproduction,0.95),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%      min(min(quantile(angular_errors_reproduction,0.95))))
% [r1,c1]=find(reshape(max(angular_errors_recovery,[],1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(max(angular_errors_recovery,[],1))))
% [r2,c2]=find(reshape(max(angular_errors_reproduction,[],1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(max(angular_errors_reproduction,[],1))))
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
% estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
 estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
% [r1,c1]=find(reshape(estimOldtrimean,size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(estimOldtrimean)))
[r2,c2]=find(reshape(estimNewtrimean,size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
    min(min(estimNewtrimean)))
% medianOld = nanmedian(angular_errors_recovery,1);
% medianOld(1,r1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
mednew = medianNew(1,r2,c2)
% estimOld95Q = quantile(angular_errors_recovery,0.95);
% estimOld95Q(1,r1,c1)
estimNew95Q = quantile(angular_errors_reproduction,0.95);
new95q = estimNew95Q(1,r2,c2)
% maxold = max(angular_errors_recovery,[],1);
% maxold(1,r1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maximanew = maxnew(1,r2,c2)
% meanOld = nanmean(angular_errors_recovery,1);
% meanOld(1,r1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meannewf = meanNew(1,r2,c2)
%%
%------------- 1st GreyEdge ----------------------------------------------%
clear,clc,
%load '1GreyEdge-Shi_processed';
%load '1GreyEdge-SFU_processed';
%load 'U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-1stGrayEdge-reverse';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-1stGrayEdge-reverse';
% [r1,c1]=find(reshape(nanmean(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(nanmean(angular_errors_recovery,1))))
%[r2,c2]=find(reshape(nanmean(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(nanmean(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(nanmedian(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(nanmedian(angular_errors_recovery,1))))
%  [r2,c2]=find(reshape(nanmedian(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(nanmedian(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(quantile(angular_errors_recovery,0.95),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(quantile(angular_errors_recovery,0.95))))
% [r2,c2]=find(reshape(quantile(angular_errors_reproduction,0.95),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(quantile(angular_errors_reproduction,0.95))))
% [r1,c1]=find(reshape(max(angular_errors_recovery,[],1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(max(angular_errors_recovery,[],1))))
[r2,c2]=find(reshape(max(angular_errors_reproduction,[],1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
    min(min(max(angular_errors_reproduction,[],1))))
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
%estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
 estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
% [r1,c1]=find(reshape(estimOldtrimean,size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(estimOldtrimean)))
% [r2,c2]=find(reshape(estimNewtrimean,size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(estimNewtrimean)))
% medianOld = nanmedian(angular_errors_recovery,1);
% medianOld(1,r1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
mednew = medianNew(1,r2,c2)
% estimOld95Q = quantile(angular_errors_recovery,0.95);
% estimOld95Q(1,r1,c1)
    estimNew95Q = quantile(angular_errors_reproduction,0.95);
    new95q = estimNew95Q(1,r2,c2)
% maxold = max(angular_errors_recovery,[],1);
% maxold(1,r1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maximanew = maxnew(1,r2,c2)
% meanOld = nanmean(angular_errors_recovery,1);
% meanOld(1,r1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meannewf = meanNew(1,r2,c2)
%%
%-------------------- 2nd GreyEdge --------------------------------------%
clear,clc,
%load '2GreyEdge-Shi_processed';
%load 'U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-2ndGrayEdge-reverse';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-2ndGrayEdge-reverse';
%load '2GreyEdge-SFU_processed';
% [r1,c1]=find(reshape(nanmean(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(nanmean(angular_errors_recovery,1))))
%[r2,c2]=find(reshape(nanmean(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(nanmean(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(nanmedian(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(nanmedian(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(nanmedian(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(nanmedian(angular_errors_reproduction,1))))
%[r1,c1]=find(reshape(quantile(angular_errors_recovery,0.95),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(quantile(angular_errors_recovery,0.95))))
%[r2,c2]=find(reshape(quantile(angular_errors_reproduction,0.95),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(quantile(angular_errors_reproduction,0.95))))
% [r1,c1]=find(reshape(max(angular_errors_recovery,[],1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(max(angular_errors_recovery,[],1))))
% [r2,c2]=find(reshape(max(angular_errors_reproduction,[],1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(max(angular_errors_reproduction,[],1))))
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
%estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
 estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
% [r1,c1]=find(reshape(estimOldtrimean,size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(estimOldtrimean)))
 [r2,c2]=find(reshape(estimNewtrimean,size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(estimNewtrimean)))
% medianOld = nanmedian(angular_errors_recovery,1);
% medianOld(1,r1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
mednew= medianNew(1,r2,c2)
% estimOld95Q = quantile(angular_errors_recovery,0.95);
% estimOld95Q(1,r1,c1)
estimNew95Q = quantile(angular_errors_reproduction,0.95);
new95q = estimNew95Q(1,r2,c2)
% maxold = max(angular_errors_recovery,[],1);
% maxold(1,r1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maximnew = maxnew(1,r2,c2)
% meanOld = nanmean(angular_errors_recovery,1);
% meanOld(1,r1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meannewf = meanNew(1,r2,c2)
%%
%---------------------------- general Greyworld --------------------------%
clear,clc,
%load 'generalgreyworld-Shi_processed';
%load 'generalgreyworld-SFU_processed';
%load 'U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GeneralGrayWorld-reverse';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-GeneralGrayWorld-reverse';
%angular_errors_reproduction(angular_errors_reproduction == 0)=nan;
%angular_errors_recovery(angular_errors_recovery == 0)=nan;
% [r1,c1]=find(reshape(mean(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(mean(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(mean(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(mean(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(nanmedian(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(nanmedian(angular_errors_recovery,1))))
%[r2,c2]=find(reshape(nanmedian(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(nanmedian(angular_errors_reproduction,1))))
%[r1,c1]=find(reshape(quantile(angular_errors_recovery,0.95),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(quantile(angular_errors_recovery,0.95))))
%[r2,c2]=find(reshape(quantile(angular_errors_reproduction,0.95),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(quantile(angular_errors_reproduction,0.95))))
% [r1,c1]=find(reshape(max(angular_errors_recovery,[],1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(max(angular_errors_recovery,[],1))))
% [r2,c2]=find(reshape(max(angular_errors_reproduction,[],1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(max(angular_errors_reproduction,[],1))))
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
%estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
 estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
% [r1,c1]=find(reshape(estimOldtrimean,size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(estimOldtrimean)))
 [r2,c2]=find(reshape(estimNewtrimean,size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(estimNewtrimean)))
%medianOld = nanmedian(angular_errors_recovery,1);
%medianOld(1,r1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
mednew = medianNew(1,r2,c2)
%estimOld95Q = quantile(angular_errors_recovery,0.95);
%estimOld95Q(1,r1,c1)
estimNew95Q = quantile(angular_errors_reproduction,0.95);
new95q = estimNew95Q(1,r2,c2)
%maxold = max(angular_errors_recovery,[],1);
%maxold(1,r1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maximanew = maxnew(1,r2,c2)
%meanOld = nanmean(angular_errors_recovery,1);
%meanOld(1,r1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meannewf = meanNew(1,r2,c2)
%%
%---------------------- shades of Grey ----------------------------------%
clear,clc,
%load 'ShadesofGrey-SFU_processed';
%load 'ShadesofGrey-Shi_processed';
%load 'U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-ShadesofGray-reverse';
load 'U:\Roshanak\CODE\colour constancy\finalresults\Shi-ShadesofGray-reverse';
% c1=find(mean(angular_errors_recovery,1)== min(min(mean(angular_errors_recovery,1))))
% c2=find(mean(angular_errors_reproduction,1)== min(min(mean(angular_errors_reproduction,1))))
% c1=find(nanmedian(angular_errors_recovery,1)== min(min(nanmedian(angular_errors_recovery,1))))
% c2=find(nanmedian(angular_errors_reproduction,1)== min(min(nanmedian(angular_errors_reproduction,1))))
%c1=find(quantile(angular_errors_recovery,0.95)== min(min(quantile(angular_errors_recovery,0.95))))
%c2=find(quantile(angular_errors_reproduction,0.95)== min(min(quantile(angular_errors_reproduction,0.95))))
%c1=find(max(angular_errors_recovery,[],1)== min(min(max(angular_errors_recovery,[],1))))
%c2=find(max(angular_errors_reproduction,[],1)== min(min(max(angular_errors_reproduction,[],1))))
%estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,c1)+0.5*estimOldQ(2,c1)+0.25*estimOldQ(3,c1)
%estimNewtrimean = 0.25*estimNewQ(1,c2)+0.5*estimNewQ(2,c2)+0.25*estimNewQ(3,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:)+0.5*estimOldQ(2,:)+0.25*estimOldQ(3,:);
 estimNewtrimean = 0.25*estimNewQ(1,:)+0.5*estimNewQ(2,:)+0.25*estimNewQ(3,:);
% c1=find(estimOldtrimean == min(min(estimOldtrimean)))
 c2=find(estimNewtrimean == min(min(estimNewtrimean)))
% medianOld = nanmedian(angular_errors_recovery,1);
% medianOld(1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
mednew = medianNew(1,c2)
% estimOld95Q = quantile(angular_errors_recovery,0.95);
% estimOld95Q(1,c1)
estimNew95Q = quantile(angular_errors_reproduction,0.95);
new95q = estimNew95Q(1,c2)
% maxold = max(angular_errors_recovery,[],1);
% maxold(1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maximanew = maxnew(1,c2)
% meanOld = nanmean(angular_errors_recovery,1);
% meanOld(1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meannewf = meanNew(1,c2)