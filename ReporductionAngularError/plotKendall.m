% %% according to error 
% [num1,str1] = xlsread('U:\Roshanak\colour constancy and illuminance estimation\notes\sorteddec19.xls','shi','B1:C11');
% [num2,str2] = xlsread('U:\Roshanak\colour constancy and illuminance estimation\notes\sorteddec19.xls','shi','G1:H11');
% x = ones(1,11);
% figure(1),scatter(x,num1','r*'),xlim([0 3]),set(gca,'XTick',0:1:3), text(x,num1',str1(:),'FontSize',8), hold on,
% x2 = ones(1,11)+1;
% figure(1),scatter(x2,num2','b*'),xlim([0 3]),set(gca,'XTick',0:1:3), text(x2,num2',str2(:),'FontSize',8), hold on,
% for i=1:11
%     k=find(strcmp(str2,str1(i)));
%     figure(1),line([1 2],[num1(i) num2(k)]);
%     hold on,
% end

%% rank only
clear
% [rank1,str1] = xlsread('U:\Roshanak\colour constancy and illuminance estimation\notes\sorteddec19.xls','shi','C33:D44');
% [rank2,str2] = xlsread('U:\Roshanak\colour constancy and illuminance estimation\notes\sorteddec19.xls','shi','H33:I44');
[rank1,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','shi-sort-rankdiff','I2:J273');
[rank2,str2] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','shi-sort-rankdiff','L2:M273');

numalgo = size(rank1,1);
x = ones(1,numalgo);
x2 = ones(1,numalgo)+1;
selectedalgo =0;
s = 1;
for i=1:numalgo
    k=find(strcmp(str2,str1(i)));
    if ((abs(i-k))>= 20)
        selectedrank1(s) =  rank1(i);
        selectedrank2(s) =  rank2(k);
        figure(1),scatter(x(1,i),rank1(i),'r*'),xlim([0 3]),set(gca,'XTick',0:1:3),text(x(1,i) -.4,rank1(i),str1(i),'FontSize',10),  hold on,
        figure(1),scatter(x2(1,k),rank1(k),'b*'),text(x2(1,k)+.1,rank1(k),str2(k),'FontSize',10), hold on,
        figure(1),line([1 2],[rank1(i) rank1(k)]); 
        selectedalgo = selectedalgo + 1;
        s = s+1;
    end
    hold on,
end
[~,~,rnk1] = unique(selectedrank1);
[~,~,rnk2] = unique(selectedrank2);
corr(selectedrank1', selectedrank2', 'type', 'kendall');
taut = corr(rnk1, rnk2, 'type', 'kendall');
con2disconratio =(1+taut)/(1- taut)
%% edit distance
clear
[rank1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','sfu-sort','E3:E544');
[rank2] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','sfu-sort','A3:A544');
str1 = num2str(rank1);
str1 = str1';
str1 = str1(3,:);
% str1 = strrep(str1',' ','');

str2 = num2str(rank2);
str2 = str2';
str2 = str2(3,:);

[V,v] = EditDistance(str1,str2);
%% kendall's test within threshold
clear;
[~,~,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','sfu','A5:F616');
str1(:,1)=[];
str1(:,3)=[];
str1(:,3)=[];
sorted1 = sortrows(str1,2);
%sorted2 = sortrows(str1,3);
[~,~,rnk1] = unique(cell2mat(sorted1(:,2)));
[~,~,rnk2] = unique(cell2mat(sorted1(:,3)));

numalgo = size(rnk1,1);
x = ones(1,numalgo);
x2 = ones(1,numalgo)+1;

s = 1;

for i=1:numalgo-1
    %k=find(strcmp(str2,str1(i)));
    %if ((abs(rnk1(i)-rnk2(i)))>= 50)
    if((sign(rnk1(i)-rnk1(i+1))~= sign(rnk2(i)-rnk2(i+1))))
        selectedrank1(s) =  rnk1(i);
        selectedrank2(s) =  rnk2(i);
        %selectedalgo(s)= str1(i,1);
        figure(1),scatter(x(1,i),rnk1(i),'r*'),xlim([0 3]),set(gca,'XTick',0:1:3),text(x(1,i) -.4,rnk1(i),str1(i,1),'FontSize',10),  hold on,
        figure(1),scatter(x2(1,i),rnk2(i),'b*'),text(x2(1,i)+.1,rnk2(i),str1(i,1),'FontSize',10), hold on,
        figure(1),line([1 2],[rnk1(i) rnk2(i)]); 
        
        s = s+1;
    end
    hold on,
end
taut = corr(selectedrank1', selectedrank2', 'type', 'kendall');
con2disconratio =(1+taut)/(1- taut)

%% multispectra data
clear;
[~,~,str1] = xlsread('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\multispectral\31 bands estimates.xlsx','median_1_5.8','X3:AB6');
%str1(:,1)=[];
%str1(:,3)=[];
%str1(:,3)=[];
sorted1 = sortrows(str1,2);
%sorted2 = sortrows(str1,3);
[~,~,rnk1] = unique(cell2mat(sorted1(:,2)));
[~,~,rnk2] = unique(cell2mat(sorted1(:,4)));

numalgo = size(rnk1,1);
x = ones(1,numalgo);
x2 = ones(1,numalgo)+1;

s = 1;

for i=1:numalgo
    %k=find(strcmp(str2,str1(i)));
    %if ((abs(rnk1(i)-rnk2(i)))>= 50)
    %if((sign(rnk1(i)-rnk1(i+1))~= sign(rnk2(i)-rnk2(i+1))))
       selectedrank1(s) =  rnk1(i);
        selectedrank2(s) =  rnk2(i);
        %selectedalgo(s)= str1(i,1);
        figure(1),scatter(x(1,i),rnk1(i),'rs','filled'),xlim([0 3]),ylim([0 5]),set(gca,'YTick',0:1:5),set(gca,'YTickLabel',{' ';'1';'2';'3';'4';' '}),...
            set(gca,'XTick',0:1:3),set(gca,'XTickLabel',{' ';'Recovery error';'Reproduction error';' '},'fontsize',10,'fontweight','bold'),text(x(1,i) -.85,rnk1(i),str1(i,1),'FontSize',10,'FontWeight' ,'bold'),  hold on,
        figure(1),scatter(x2(1,i),rnk2(i),'rs','filled'),text(x2(1,i)+.1,rnk2(i),str1(i,1),'FontSize',10,'FontWeight' ,'bold'), hold on,
        figure(1),line([1 2],[rnk1(i) rnk2(i)],'LineWidth',2); 
        
        s = s+1;
    %end
    hold on,
end
taut = corr(selectedrank1', selectedrank2', 'type', 'kendall');
con2disconratio =(1+taut)/(1- taut)

