clear
load('2GreyEdge-Shi_processed.mat');
%load('2GreyEdge-SFUGreyball_processed.mat');
estimErrorNew2G = reshape(estimErrorNew2G,size(estimErrorNew2G,1),size(estimErrorNew2G,2)*size(estimErrorNew2G,3));
estimErrorOld2G = reshape(estimErrorOld2G,size(estimErrorOld2G,1),size(estimErrorOld2G,2)*size(estimErrorOld2G,3));
diff = zeros(size(estimErrorNew2G,2)*size(estimErrorNew2G,2),3);
n=1;
for i=1:size(estimErrorNew2G,2)
    for j=1:size(estimErrorNew2G,2)
    res1 = kstest2(estimErrorNew2G(:,i),estimErrorNew2G(:,j));
    res2 = kstest2(estimErrorOld2G(:,i),estimErrorOld2G(:,j));
    diff(n,1) = res1+res2;
    diff(n,2) = i;
    diff(n,3) = j;
    n = n+1;
    end
end

find(diff(:,1)==1);

%% new ranks
clear,clc;
%[error] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','shi_sort','H3:I274');
%[v,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','shi_sort','C3:C274');
% [error] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','greyball-sort','O2:P543');
% [v,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','greyball-sort','C2:C543');
[error(:,1)] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','shi','C5:C286');
[error(:,2)] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','shi','F5:F286');
[v,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','shi','B5:F286');
% rho = corr(error(:,1), error(:,2), 'type', 'Spearman');

[~,~,rnk1] = unique(error(:,1));
[~,~,rnk2] = unique(error(:,2));
x = (1:1:size(rnk1,1));
y = (1:1:size(str1,1));
plot(rnk1(:),'r*'),hold on, plot(rnk2(:),'b*')
% plot(rnk1(1:100),'r*'),hold on, plot(rnk2(1:100),'b*'),ylabel('ranks'),axis([1 101 1  max(max(rnk1(:),rnk2(:)))]),set(gca,'YTick',y),...
% set(gca,'XTick',x(1:100)),set(gca,'XTickLabel',str1(1:100)),rotateXLabels(gca(),90 );
% t = str1(1:50);
% set(t,'Rotation',45);
% % Remove the default labels
% set(gca,'XTickLabel','')

[r,t,p]=spear(rnk1,rnk2);
 rho = corr(rnk1, rnk2, 'type', 'Spearman');
  corr(rnk1, rnk2, 'type', 'kendall')

