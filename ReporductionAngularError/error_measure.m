% error_measure: measures the new and old angular error and the related statisitical moments. 
%
% 
% SYNOPSIS:
%    [ estimErrorOld, estimErrorNew, estimOldmean, estimNewmean,
%    estimOldmed, estimNewmed, estimOldmax, estimNewmax,
%    estimOldtrimean,estimNewtrimean] = error_measure( groundtruth_illum,
%    estimated_illum, mink_norm,sigma)
%    
% INPUT :
%   groundtruth_illum   : the ground truth illumiant (Nx3, N:number of images in the dataset).
%	estimated_illum     : estimated illuminant using some colour constancy method, (dimensions depend on the method). 
%	mink_norm           : minkowski norm used. 
%   sigma               : smoothing value.
%                  
% OUTPUT: 
%   estimErrorOld       : old angular error
%   estimErrorNew       : new angular error
%   estimOldmean        : mean of old angular error
%   estimNewmean        : mean of old angular error
%   estimOldmed         : median of old angular error
%   estimNewmed         : median of new angular error
%   estimOldmax         : max of old angular error
%   estimNewmax         : max of new angular error
%   estimOldtrimean     : trimean of old angular error
%   estimNewtrimean     : trimean of new angular error
%   
% writen by:  Roshanak Zakizadeh     28/11/2013 
% LITERATURE :
%
% Graham D. Finlayson, Roshanak Zakizadeh
% "Reproduction Angular Error: An Improved Performance Metric for Illuminant Estimation, BMVC 2014"
%
function [ estimErrorOld, estimErrorNew, estimOldmean, estimNewmean, estimOldmed, estimNewmed,...
    estimOldmax, estimNewmax, estimOldtrimean,estimNewtrimean] = error_measure( groundtruth_illum, estimated_illum, mink_norm,sigma)
%
%   Detailed explanation goes here
%estimated_illum(~estimated_illum(:))=1/255;
if(nargin<3), mink_norm=0;sigma=0; end
if(nargin<4), sigma=0; end

nimages= size(estimated_illum,1);
estimErrorOld = zeros(nimages,1);
D65=[1;1;1];
estimErrorNew = zeros(nimages,1);

for imageno = 1:nimages
    normTrueIlluminant =groundtruth_illum(imageno,:)/norm(groundtruth_illum(imageno,:));
    if (sigma==0)
        if (mink_norm==0)% mink_norm,sigma=0 Grey-world & MaxRGB
            normest =squeeze(estimated_illum(imageno,:))/norm(squeeze(estimated_illum(imageno,:)));
            D = diag(groundtruth_illum(imageno,:))/diag(reshape(estimated_illum(imageno,:),1,3));
            % reverse version
            %D = diag(reshape(estimated_illum(imageno,:),1,3))/diag(groundtruth_illum(imageno,:));
                            
        else % sigma=0, Shades_of_Grey
            normest =squeeze(estimated_illum(imageno,mink_norm,:))/norm(squeeze(estimated_illum(imageno,mink_norm,:)));
            D = diag(groundtruth_illum(imageno,:))/diag(reshape(estimated_illum(imageno,mink_norm,:),1,3));
            % reverse version
            %D = diag(reshape(estimated_illum(imageno,mink_norm,:),1,3))/diag(groundtruth_illum(imageno,:));
           
        end
    else
        if(mink_norm==0)% mink_norm=0, Gamut_mapping
        else % mink_norm & sigma are non zero, general_grey_world, 1st-order grey-edge 7 2nd-order grey-edge
            normest =squeeze(estimated_illum(imageno,mink_norm,sigma,:))/norm(squeeze(estimated_illum(imageno,mink_norm,sigma,:)));
            D = diag(groundtruth_illum(imageno,:))/diag(reshape(estimated_illum(imageno,mink_norm,sigma,:),1,3));
            % reverse version
            %D = diag(reshape(estimated_illum(imageno,mink_norm,sigma,:),1,3))/diag(groundtruth_illum(imageno,:));
           
        end
    end
    
    % old angular error
    estimErrorOld(imageno) = acosd(dot(normTrueIlluminant,normest));
    
    % new angular error             
    estimErrorNew(imageno)= acosd(dot((D65/norm(D65)),( [D(1,1);D(2,2);D(3,3)]/norm([D(1,1);D(2,2);D(3,3)]))));
    
   
    
end
   
% statistical moments
estimOldmean= nanmean(estimErrorOld(:));
estimNewmean= nanmean(estimErrorNew(:));
estimOldmed= nanmedian(estimErrorOld(:));
estimNewmed= nanmedian(estimErrorNew(:));
estimOldmax= max(estimErrorOld(:));
estimNewmax= max(estimErrorNew(:));
estimOldQ = quantile(estimErrorOld,[0.25, 0.5, 0.75]);
estimNewQ = quantile(estimErrorNew,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3);
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3);

end

