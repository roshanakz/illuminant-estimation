%% kendall's test within threshold
clear;
%[~,~,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','SFUFrameworks','A3:E184');
[~,~,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','sfu','B5:F665');
%[~,~,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','greyball','A367:E546');
str1(:,3)=[];
str1(:,3)=[];
%str1(:,2:3) = num2cell(round(cell2mat(str1(:,2:3))*100)/100);
% second column is the new error median based
sorted1 = sortrows(str1,2);
% remove the duplicates
[~, ind] = unique(cell2mat(sorted1(:,3)), 'first');
% sort based on the old error ranking
sorted1 = sorted1(ind,:);
%sorted2 = sortrows(str1,3);
% give a rank to each algo based on new and old error
[~,~,rnk1] = unique(cell2mat(sorted1(:,2)));
[~,~,rnk2] = unique(cell2mat(sorted1(:,3)));

% 'sorted1' is finally sorted based on old error
numalgo = size(rnk1,1);
%x = ones(1,numalgo);
%x2 = ones(1,numalgo)+1;

s = 1;

for i=1:numalgo
    %k=find(strcmp(str2,str1(i)));
   %if ((abs(rnk1(i)-rnk2(i)))>= 1)
    %if((sign(rnk1(i)-rnk1(i+1))~= sign(rnk2(i)-rnk2(i+1)))&& ((abs(rnk1(i)-rnk2(i)))>= 50))
        selectedrank1(s) =  rnk1(i);
        selectedrank2(s) =  rnk2(i);
        % the name of the algorithms sorted based on old error
        selectedalgo(s,1)= cellstr(sorted1(i,1));
        selectedalgorankdiff(s,1)= abs(rnk1(i)-rnk2(i));
%         figure(1),scatter(x(1,i),rnk1(i),'r*'),xlim([0 3]),set(gca,'XTick',0:1:3),text(x(1,i) -.5,rnk1(i),sorted1(i,1),'FontSize',10),  hold on,
%         figure(1),scatter(x2(1,i),rnk2(i),'b*'),text(x2(1,i)+.1,rnk2(i),sorted1(i,1),'FontSize',10), hold on,
%         figure(1),line([1 2],[rnk1(i) rnk2(i)]); 
%         
        s = s+1;
   %end
    
end
% returns a matrix containing all possible combinations of the elements 
% of vector v taken k at a time. Matrix C has k columns and n!/((n�k)! k!) rows, where n is length(v).
% k = the numbe of algorithms in the website
%b = nchoosek(n,k)
% performing kendall's test manually to compute the number of discordant and
% concordant pairs, the upper triangle of the mapped matrix can hold the
% sign of the rank difference and finally compared with the sorted set in
% the ascending order give us how discordant the ranks are
selrnk(:,1)=[11 10 9 7 4 2 3 1 5 8 6]';
selrnk(:,2)=[11 10 8 6 4 3 2 1 5 9 7]';

selrnk = sortrows(selrnk,2);
% look for ic in this (for ranking with the ties)
[C, ia, ic] = unique(unnamed(:,1),'sorted');
clear,
selrnk(:,1)=[13 12 3 5 6 4 7]';
selrnk(:,2)=[12 13 2 4 5 3 6]';
selrnk = sortrows(selrnk,1);
selectedrank1 = selrnk(:,2);
n = length(selectedrank1);
k = repmat((1:n)',1,n);
signmat = zeros(n,n);
q = k(:);
k = k';
j = k(:);
k = find(q>j);
q = q(k);
j = j(k);
% for fillmat = 1:n
% [r,c]=find(j==fillmat);
% %signmat(fillmat,1)= 0;
% signmat(fillmat,q(r))= sign(x(q(r))-repmat(x(fillmat),1,length(x(q(r)))));
% end
diffs = sum(selectedrank1(q) < selectedrank1(j))
nodiffs = sum(selectedrank1(q) > selectedrank1(j))
ties = sum(selectedrank1(q) == selectedrank1(j));
Kendall_tau_c = (nodiffs-diffs)*( (2*1)/n^2*(2-1) )
Kendall_tau_b = (nodiffs-diffs) / ( (nodiffs + diffs + ties)*(nodiffs + diffs + ties) )^0.5 
Kendall_tau_a = (nodiffs-diffs) / (n*(n-1)/2)
kendall_tau_tie = (nodiffs-diffs) / (nodiffs+diffs)
T= nodiffs-diffs


% matlab is giving us the Kendall_tau_b or a values 
[taut pval]= corr(selectedrank1', selectedrank2', 'type', 'kendall','tail','left');
con2disconratio =(1+taut)/(1- taut)
zp=norminv([1-pval pval],0,1);
quantTaut = zp(:,2) *sqrt((2*(2*size(selectedrank1,2)+5)))/(3*sqrt(size(selectedrank1,2)*(size(selectedrank1,2)-1)))
quantT = zp(:,2) *sqrt(size(selectedrank1,2)*(size(selectedrank1,2)-1)*(2*size(selectedrank1,2)+5)/18)
T = taut*(size(selectedrank1,2)*(size(selectedrank1,2)-1)/2)

% figure(1),xlim([0 n]),set(gca, 'XTick',1:10:n,'XTickLabel',selectedalgo(1:n)),...
%             ylim([0 n]),set(gca, 'YTick',1:10:n,'YTickLabel',selectedalgo(1:n)),set(gca,'YAxisLocation','right'),rotateXLabels(gca(),90),...
%             title('The points below show the discordant in ranking of the color constancy algorithms according to the new metric compared with the conventional one','FontWeight','bold'),hold on
% for rown = 1:n
%     for coln= 1:n
%         if(signmat(rown,coln)<0)
%           figure(1),scatter(coln,rown,'b*'),  
%         end
%     end
% end

% [taut pval]= corr(selectedrank1(50:100)', selectedrank2(50:100)', 'type', 'kendall','tail','left');
% con2disconratio =(1+taut)/(1- taut)
% zp=norminv([1-pval pval],0,1);
% quantTaut = zp(:,2) *sqrt((2*(2*size(selectedrank1(50:100),2)+5)))/(3*sqrt(size(selectedrank1(50:100),2)*(size(selectedrank1(50:100),2)-1)))
% quantT = zp(:,2) *sqrt(size(selectedrank1(50:100),2)*(size(selectedrank1(50:100),2)-1)*(2*size(selectedrank1(50:100),2)+5)/18)
% T = taut*(size(selectedrank1(50:100),2)*(size(selectedrank1(50:100),2)-1)/2)

find(strcmp(selectedalgo(:),'Grey-world')==1)
find(strcmp(selectedalgo(:),'White-patch')==1)
str1(find(strcmp(str1(:),'White-patch')==1),:)
% str1(find(strcmp(str1(:),selectedalgo(329))==1),:)
[~,~,selrnk1]= unique([selectedrank1(1,640) selectedrank1(1,624) selectedrank1(1,623) selectedrank1(1,13:27)]');
[~,~,selrnk2]= unique([selectedrank2(1,640) selectedrank2(1,624) selectedrank2(1,623) selectedrank2(1,13:27)]');
selalgo = [selectedalgo(640,:);selectedalgo(624,:);selectedalgo(623,:);selectedalgo(13:27,:)];
%load 'selalgo120_140';
load 'selalgo13_27';
load 'selrnk13_27';
%load 'selrnk120_140';
selrnk(:,1)=[9 7 2 3 8 6]';
selrnk(:,2)=[8 6 3 2 9 7]';
selrnk = sortrows(selrnk,1);
[~,~,selrnk1]= unique(selrnk(:,1));
[~,~,selrnk2]= unique(selrnk(:,2));
selalgo = {'Pixel-based Gamut';'Edge-based Gamut';'Weighted Gray-Edge';'1^{st} Gray-Edge';'Heavy-tailed';'shades of Gray'};
x = ones(1,size(selrnk1,1));
x2 = ones(1,size(selrnk1,1))+1;
figure(1),scatter(x',selrnk1,'rs', 'LineWidth',4),xlim([0 3]),set(gca,'XTick',0:1:3),ylim([0 7]),set(gca,'XTick',0:1:7),...
  set(gca, 'XTickLabel',{'','Recovery angular error','Reproduction angular error',''},'FontSize',10,'fontWeight','bold'),ylabel('ranks'),...
    text(x'-.9,selrnk1,selalgo),  hold on,
    figure(1),scatter(x2',selrnk2,'rs', 'LineWidth',4),text(x2'+.1,selrnk2,selalgo), hold on,
    figure(1),line([1 2],[selrnk1 selrnk2],'Color',[0 0 1], 'LineWidth',4);
    figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',10,'fontWeight','bold'),

%     figure(2),scatter(x',selrnk1,'r*'),xlim([0 3]),set(gca,'XTick',0:1:3),...
%     set(gca, 'XTickLabel',{'','New angular error','Old angular error',''}),...
%     ylabel('ranks'),...
%     text(x'-.7,selrnk1,selalgo,'FontSize',10),  hold on,
%     figure(2),scatter(x2',selrnk2,'b*'),text(x2'+.1,selrnk2,selalgo,'FontSize',10), hold on,
%     figure(2),line([1 2],[selrnk1 selrnk2],'Color',[0 0 1]); 
    
    save('selalgo13_27','selalgo');
    save('selrnk13_27','selrnk1','selrnk2');
    
    clc, clear,
    selrnk(:,1)=[5 5 2 3 3 1 4 1 5 2 1 1 1 1 2 2 1 1]';
    selrnk(:,2)=[5 5 3 3 3 1 4 1 6 2 2 2 2 1 2 2 2 1]';
    %selrnk = sortrows(selrnk,1);
    [~,~,selrnk1]= unique(selrnk(:,1));
    [~,~,selrnk2]= unique(selrnk(:,2));
    [taut pval]= corr(selrnk1, selrnk2, 'type', 'Kendall','tail','left');
    % look zp up in the normal distribution table
    zp=norminv([1-pval pval],0,1);
    quantTaut = zp(:,2) *sqrt((2*(2*size(selrnk1,1)+5)))/(3*sqrt(size(selrnk1,1)*(size(selrnk1,1)-1)))
    quantT = zp(:,2) *sqrt(size(selrnk1,1)*(size(selrnk1,1)-1)*(2*size(selrnk1,1)+5)/18)
    %T = taut*(size(selrnk1,1)*(size(selrnk1,1)-1)/2)

%%
%[~,~,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','SFUFrameworks','A3:E184');
[~,~,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','sfu','B5:F646');
%[~,~,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','greyball','A367:E546');
%str1(:,1)=[];
str1(:,3)=[];
str1(:,3)=[];
%str1(:,2:3) = num2cell(round(cell2mat(str1(:,2:3))*100)/100);
sorted1 = sortrows(str1,2);
[~, ind] = unique(cell2mat(sorted1(:,3)), 'first');
sorted1 = sorted1(ind,:);
%sorted2 = sortrows(str1,3);
[~,~,rnk1] = unique(cell2mat(sorted1(:,2)));
[~,~,rnk2] = unique(cell2mat(sorted1(:,3)));

numalgo = size(rnk1,1);
x = ones(1,numalgo);
x2 = ones(1,numalgo)+1;

s = 1;

for i=1:numalgo
    %k=find(strcmp(str2,str1(i)));
   %if ((abs(rnk1(i)-rnk2(i)))>= 1)
    %if((sign(rnk1(i)-rnk1(i+1))~= sign(rnk2(i)-rnk2(i+1)))&& ((abs(rnk1(i)-rnk2(i)))>= 50))
        selectedrank1(s) =  rnk1(i);
        selectedrank2(s) =  rnk2(i);
        selectedalgo(s,1)= cellstr(sorted1(i,1));
        selectedalgorankdiff(s,1)= abs(rnk1(i)-rnk2(i));
%         figure(1),scatter(x(1,i),rnk1(i),'r*'),xlim([0 3]),set(gca,'XTick',0:1:3),text(x(1,i) -.4,rnk1(i),sorted1(i,1),'FontSize',10),  hold on,
%         figure(1),scatter(x2(1,i),rnk2(i),'b*'),text(x2(1,i)+.1,rnk2(i),sorted1(i,1),'FontSize',10), hold on,
%         figure(1),line([1 2],[rnk1(i) rnk2(i)]); 
%         
        s = s+1;
   %end
    
end
% returns a matrix containing all possible combinations of the elements 
% of vector v taken k at a time. Matrix C has k columns and n!/((n�k)! k!) rows, where n is length(v).
% k = the numbe of algorithms in the website
%b = nchoosek(n,k)
% performing kendall's test manually to compute the number of discordant and
% concordant pairs, the upper triangle of the mapped matrix can hold the
% sign of the rank difference and finally compared with the sorted set in
% the ascending order give us how discordant the ranks are
x = selectedrank1;
n = length(x);
k = repmat((1:n)',1,n);
signmat = zeros(n,n);
q = k(:);
k = k';
j = k(:);
k = find(q>j);
q = q(k);
j = j(k);
for fillmat = 1:n
[r,c]=find(j==fillmat);
%signmat(fillmat,1)= 0;
signmat(fillmat,q(r))= sign(x(q(r))-repmat(x(fillmat),1,length(x(q(r)))));
end
diffs = sum(x(q) < x(j));
nodiffs = sum(x(q) > x(j));
ties = sum(x(q) == x(j));
Kendall_tau_c = (nodiffs-diffs)*( (2*1)/n^2*(2-1) )
Kendall_tau_b = (nodiffs-diffs) / ( (nodiffs + diffs + ties)*(nodiffs + diffs + ties) )^0.5 
Kendall_tau_a = (nodiffs-diffs) / (n*(n-1)/2)

% matlab is giving us the Kendall_tau_b or a values 
[taut pval]= corr(selectedrank1', selectedrank2', 'type', 'kendall','tail','left');
con2disconratio =(1+taut)/(1- taut)
zp=norminv([1-pval pval],0,1);
quantTaut = zp(:,2) *sqrt((2*(2*size(selectedrank1,2)+5)))/(3*sqrt(size(selectedrank1,2)*(size(selectedrank1,2)-1)))
quantT = zp(:,2) *sqrt(size(selectedrank1,2)*(size(selectedrank1,2)-1)*(2*size(selectedrank1,2)+5)/18)
T = taut*(size(selectedrank1,2)*(size(selectedrank1,2)-1)/2)

% figure(1),xlim([0 n]),set(gca, 'XTick',1:10:n,'XTickLabel',selectedalgo(1:n)),...
%             ylim([0 n]),set(gca, 'YTick',1:10:n,'YTickLabel',selectedalgo(1:n)),set(gca,'YAxisLocation','right'),rotateXLabels(gca(),90),...
%             title('The points below show the discordant in ranking of the color constancy algorithms according to the new metric compared with the conventional one','FontWeight','bold'),hold on
% for rown = 1:n
%     for coln= 1:n
%         if(signmat(rown,coln)<0)
%           figure(1),scatter(coln,rown,'b*'),  
%         end
%     end
% end    
    
%% kendall's part by part
clear;
%load 'U:\Roshanak\CODE\colour constancy\Data\Gamut mapping\sfulab_gamut\sfulab_gamut_edgebased';
%meanangular_error = zeros(size(angular_errors,2),size(angular_errors,3));
%meanangular_error = reshape(round(nanmean(angular_errors,1)*10)/10,size(angular_errors,2),size(angular_errors,3));
%[r,c]=find(meanangular_error==3.6)
load('GamutEdge-SFU_processed.mat');
estimOldmean = round(estimOldmean*10)/10;
estimNewmean = round(estimNewmean*10)/10;
% yedge (2nd parameter) is chosen in colorconstancy website
estimOldmean_Gamutyedge = num2cell(estimOldmean(:,2));
estimNewmean_Gamutyedge = num2cell(estimNewmean(:,2));
estimOldmean_Gamutyedge(:,2)= strcat({'Edge-based Gamut Mapping'},int2str((1:10).')).';
Gamutyedge = [estimNewmean_Gamutyedge,estimOldmean_Gamutyedge];
Gamutyedge = sortrows(Gamutyedge,1);

clearvars estimOldmean estimNewmean meanangular_error
load('Gamutinter-SFU_processed.mat');
estimOldmean = round(estimOldmean*10)/10;
estimNewmean = round(estimNewmean*10)/10;
% the second to the intersection of the 2-jet (2nd parameter) is chosen in colorconstancy website
estimOldmean_Gamut2jetinter = num2cell(estimOldmean(:,2));
estimNewmean_Gamut2jetinter = num2cell(estimNewmean(:,2));
estimOldmean_Gamut2jetinter(:,2)= strcat({'Intersection Gamut 2-jet'},int2str((1:10).')).';
Gamut2jetinter = [estimNewmean_Gamut2jetinter,estimOldmean_Gamut2jetinter];
Gamut2jetinter = sortrows(Gamut2jetinter,1);

Gamutmap = [Gamutyedge;Gamut2jetinter];

clearvars estimOldmean estimNewmean meanangular_error
load('GamutPixel-SFU_processed.mat')
estimOldmean = round(estimOldmean*10)/10;
estimNewmean = round(estimNewmean*10)/10;
estimOldmean_Gamutpixel = num2cell(estimOldmean(:,1));
estimNewmean_Gamutpixel = num2cell(estimNewmean(:,1));
estimOldmean_Gamutpixel(:,2)= strcat({'Pixel-based Gamut Mapping'},int2str((1:10).')).';
Gamutpixel = [estimNewmean_Gamutpixel,estimOldmean_Gamutpixel];
Gamutpixel = sortrows(Gamutpixel,1);
Gamutmap = [Gamutmap;Gamutpixel];

sorted1 = sortrows(Gamutmap,1);
%[~, ind] = unique(cell2mat(sorted1(:,2)), 'first');
%sorted1 = sorted1(ind,:);
%sorted2 = sortrows(str1,3);
[~,~,rnk1] = unique(cell2mat(sorted1(:,1)));
[~,~,rnk2] = unique(cell2mat(sorted1(:,2)));

numalgo = size(rnk1,1);
x = ones(1,numalgo);
x2 = ones(1,numalgo)+1;

s = 1;

for i=1:numalgo
    %k=find(strcmp(str2,str1(i)));
   %if ((abs(rnk1(i)-rnk2(i)))>= 1)
   %if((sign(rnk1(i)-rnk2(i))~= sign(rnk1(i+1)-rnk2(i+1)))||(sign(rnk1(i)-rnk2(i))~= sign(rnk1(i-1)-rnk2(i-1))))
        selectedrank1(s) =  rnk1(i);
        selectedrank2(s) =  rnk2(i);
        selectedalgo(s,1)= cellstr(sorted1(i,3));
        selectedalgorankdiff(s,1)= abs(rnk1(i)-rnk2(i));
        figure(1),scatter(x(1,i),rnk1(i),'r*'),xlim([0 3]),set(gca,'XTick',0:1:3),text(x(1,i) -.4,rnk1(i),sorted1(i,3),'FontSize',10),  hold on,
        figure(1),scatter(x2(1,i),rnk2(i),'b*'),text(x2(1,i)+.1,rnk2(i),sorted1(i,3),'FontSize',10), hold on,
        figure(1),line([1 2],[rnk1(i) rnk2(i)]); 
%         
        s = s+1;
   %end
    hold on,
end

% if((sign(selectedrank1(i)-selectedrank1(i+1))~= sign(selectedrank1(i)-selectedrank2(i+1))))
% returns a matrix containing all possible combinations of the elements 
% of vector v taken k at a time. Matrix C has k columns and n!/((n�k)! k!) rows, where n is length(v).
% k = the number of algorithms in the website
%b = nchoosek(n,k)
[taut pval]= corr(selectedrank1', selectedrank2', 'type', 'kendall','tail','left');
con2disconratio =(1+taut)/(1- taut)
zp=norminv([1-pval pval],0,1);
quantTaut = zp(:,2) *sqrt((2*(2*size(selectedrank1,2)+5)))/(3*sqrt(size(selectedrank1,2)*(size(selectedrank1,2)-1)))
quantT = zp(:,2) *sqrt(size(selectedrank1,2)*(size(selectedrank1,2)-1)*(2*size(selectedrank1,2)+5)/18)
T = taut*(size(selectedrank1,2)*(size(selectedrank1,2)-1)/2)

% [taut pval]= corr(selectedrank1(50:100)', selectedrank2(50:100)', 'type', 'kendall','tail','left');
% con2disconratio =(1+taut)/(1- taut)
% zp=norminv([1-pval pval],0,1);
% quantTaut = zp(:,2) *sqrt((2*(2*size(selectedrank1(50:100),2)+5)))/(3*sqrt(size(selectedrank1(50:100),2)*(size(selectedrank1(50:100),2)-1)))
% quantT = zp(:,2) *sqrt(size(selectedrank1(50:100),2)*(size(selectedrank1(50:100),2)-1)*(2*size(selectedrank1(50:100),2)+5)/18)
% T = taut*(size(selectedrank1(50:100),2)*(size(selectedrank1(50:100),2)-1)/2)

%  find(strcmp(selectedalgo(:),'MaxRGB')==1)
% str1(find(strcmp(str1(:),'MaxRGB')==1),:)
%  str1(find(strcmp(str1(:),selectedalgo(329))==1),:)


clearvars estimOldmean estimNewmean meanangular_error
load('MaxRGB-SFU_processed.mat')
 [~,~,selrnk1]= unique([selectedrank1(1,27:28) selectedrank1(1,17:18)]');
 [~,~,selrnk2]= unique([selectedrank2(1,27:28) selectedrank2(1,17:18)]');
% figure(2),scatter([x(1,26) x(1,171) x(1,12:22)]',selrnk1,'r*'),xlim([0 3]),set(gca,'XTick',0:1:3),text([x(1,26) x(1,171) x(1,12:22)]' -.6,selrnk1,[selectedalgo(26,:);selectedalgo(171,:);selectedalgo(12:22,:)],'FontSize',10),  hold on,
%         figure(2),scatter([x2(1,26) x2(1,171) x2(1,12:22)]',selrnk2,'b*'),text([x2(1,26) x2(1,171) x2(1,12:22)]'+.1,selrnk2,[selectedalgo(26,:);selectedalgo(171,:);selectedalgo(12:22,:)],'FontSize',10), hold on,
%         figure(2),line([1 2],[selrnk1 selrnk2],'Color',[0 0 1]); 
% 
    [taut pval]= corr(selrnk1, selrnk2, 'type', 'Kendall','tail','left');
    % look zp up in the normal distribution table
    zp=norminv([1-pval pval],0,1);
    quantTaut = zp(:,2) *sqrt((2*(2*size(selrnk1,1)+5)))/(3*sqrt(size(selrnk1,1)*(size(selrnk1,1)-1)))
    quantT = zp(:,2) *sqrt(size(selrnk1,1)*(size(selrnk1,1)-1)*(2*size(selrnk1,1)+5)/18)
    T = taut*(size(selrnk1,1)*(size(selrnk1,1)-1)/2)


