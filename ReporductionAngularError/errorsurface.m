%% est is the estimate made by an algorithm
%% for the light when the true light is [1 1 1]
% 16/02/2016 
%% try [.8 .9 1.4]'
%est=[.8 .9 1.4]';
function r=errorsurface(est)
 
est=est./sqrt(sum(est.^2));
 
%% reprduced white under the reference
esti=1./est;
esti=esti./sqrt(sum(esti.^2))
%% reproduction error (always constant)
ang=acosd(sum(esti./sqrt(3)));
 
%% make new lights
ills=rand(10000,3);
new=ills;
new=new./(sqrt(sum(new'.^2)'*[1 1 1]));
 
%% estimates scale by diagonal model
newest=new*diag(est);
newest=newest./(sqrt(sum(newest'.^2)'*[1 1 1]));
 
%% calc recovery error
err=acosd((new.*(newest))*[1;1;1]);
 
%% make chromaticity
rg=new(:,1:3)./(sum(new')'*[1 1 1]);
 
%% plot using orthogonal projection (to [1,1,1]
%tr=[1/sqrt(2)  -1/sqrt(2)  0; 1/sqrt(6) 1/sqrt(2) -2/sqrt(6)]';
tr = eye(3);
rg=rg*tr;
 
% %% Plot Recovery error
% plot3(rg(:,1),rg(:,2),err,'.');
% hold on

% %% Plot Reproduction error
% plot3(rg(:,1),rg(:,2),err-err+ang,'.r');
% hold off

% figure 
% %% Zoom in
%  
% ind=(new*ones(3,1)./sqrt(3));
% ind=acosd(ind)<30;
% plot3(rg(ind==1,1),rg(ind==1,2),err(ind==1),'.');
% hold on
% plot3(rg(ind==1,1),rg(ind==1,2),ones(sum(ind==1),1)+ang,'.r');
% hold off

% %% mesh grid
%  xlin = linspace(min(rg(:,1)),max(rg(:,1)),33);
% ylin = linspace(min(rg(:,2)),max(rg(:,2)),33);
% [X,Y] = meshgrid(xlin,ylin);
%  f = scatteredInterpolant(rg(:,1),rg(:,2),err);
%  Z = f(X,Y);
% surf(X,Y,Z,'FaceColor',[0.21 0.31 0.49]) %interpolated %use either surf or mesh
%  axis tight; hold on
%  plot3(rg(:,1),rg(:,2),err,'.','MarkerSize',15,'Color',[0.2 0.2 0.6],'MarkerSize',15); %nonuniform
%  f2 = scatteredInterpolant(rg(:,1),rg(:,2),err-err+ang);
%  Z2 = f2(X,Y);
% surf(X,Y,Z2,'FaceColor',[0.5 0.24 0.24]) %interpolated
% axis tight; hold on
% plot3(rg(:,1),rg(:,2),err-err+ang,'.r','MarkerSize',15,'Color',[0.7 0.24 0.24]);

%% zoom in mesh grid
 figure
ind=(new*ones(3,1)./sqrt(3));
ind=acosd(ind)<30;
xlin = linspace(min(rg(ind==1,1)),max(rg(ind==1,1)),33);
ylin = linspace(min(rg(ind==1,2)),max(rg(ind==1,2)),33);
[X,Y] = meshgrid(xlin,ylin);
 f = scatteredInterpolant(rg(ind==1,1),rg(ind==1,2),err(ind==1));
 Z = f(X,Y);
surf(X,Y,Z,'FaceColor',[0.23 0.35 0.42]) %interpolated %use either surf or mesh
axis tight; hold on,set(0,'DefaultTextFontname', 'Times New Roman'),set(0,'DefaultAxesFontName', 'Times New Roman')
%plot3(rg(ind==1,1),rg(ind==1,2),err(ind==1),'.','Color',[0.2 0.2 0.6],'MarkerSize',15); %nonuniform
 f2 = scatteredInterpolant(rg(ind==1,1),rg(ind==1,2),ones(sum(ind==1),1)+ang);
 Z2 = f2(X,Y);
surf(X,Y,Z2,'FaceColor',[0.5 0.24 0.24]),xlabel('r','FontSize',15),ylabel('g','FontSize',15),zlabel('errors','FontSize',15),xlim([0 .7]),ylim([0 .7]) %interpolated
%axis tight; hold on
%zlim([0 15])
%plot3(rg(ind==1,1),rg(ind==1,2),ones(sum(ind==1),1)+ang,'.r','MarkerSize',15,'Color',[0.7 0.24 0.24]); %nonuniform
h_legend = legend('recovery error','reproduction error');set(h_legend,'FontSize',15);set(gca,'FontSize',12);
r=0;



