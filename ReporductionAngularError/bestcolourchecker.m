%-- 07/05/2014 ---%
%% best algos according to new and old errors (mean, median, trimean) for shi
%%
%--------------------------------------------- whitepatch-----------------%
clear,
load 'U:\Roshanak\CODE\colour constancy\Data\White-Patch\greyball_whitepatch';
load 'U:\Roshanak\CODE\colour constancy\finalresults\by dataset\Grayball\SFUGrayball-MaxRGB-reproduction';
estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
medianOld = nanmedian(angular_errors_recovery)
estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
meanold = mean(angular_errors_recovery)
meannew = mean(angular_errors_reproduction)
%%
%---------------------- Grey world --------------------------------------%
clear,
load 'U:\Roshanak\CODE\colour constancy\Data\GreyWorld\greyball_greyworld';
load 'U:\Roshanak\CODE\colour constancy\finalresults\by dataset\Grayball\SFUGrayball-GrayWorld-reproduction';
estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
medianOld = nanmedian(angular_errors_recovery)
estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
meanold = mean(angular_errors_recovery)
meannew = mean(angular_errors_reproduction)
%%
%------------------- iic ------------------------------------------------%
clear,
load 'U:\Roshanak\CODE\colour constancy\Data\other\Grayball\greyball_iic';
load 'U:\Roshanak\CODE\colour constancy\finalresults\by dataset\Grayball\SFUGrayball-iic-reproduction';
estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
medianOld = nanmedian(angular_errors_recovery)
estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
meanold = nanmean(angular_errors_recovery)
meannew = nanmean(angular_errors_reproduction)
%%
%------------------ Using natural images --------------------------------%
clear,
load 'U:\Roshanak\CODE\colour constancy\Data\other\Grayball\greyball_ccnis';
load 'U:\Roshanak\CODE\colour constancy\finalresults\by dataset\Grayball\SFUGrayball-ccnis-reproduction';
estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
medianOld = nanmedian(angular_errors_recovery)
estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
meanold = mean(angular_errors_recovery)
meannew = mean(angular_errors_reproduction)
%%
%------------------ Exemplar-based CC ----------------%
clear,
load 'U:\Roshanak\CODE\colour constancy\Data\other\Grayball\greyball_exemplarCC';
load 'U:\Roshanak\CODE\colour constancy\finalresults\by dataset\Grayball\SFUGrayball-exemplarCC-reproduction';
estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(angular_errors_reproduction)
medianOld = nanmedian(angular_errors_recovery)
estimOld75Q = quantile(angular_errors_recovery,0.95)
estimnew75Q = quantile(angular_errors_reproduction,0.95)
maxold = max(angular_errors_recovery)
maxnew = max(angular_errors_reproduction)
meanold = mean(angular_errors_recovery)
meannew = mean(angular_errors_reproduction)
%%
%---------------------- GamutEdge ---------------------------------------%
clear,
load 'U:\Roshanak\CODE\colour constancy\Data\Gamut mapping\greyball_gamut\greyball_gamut_edgebased';
load 'U:\Roshanak\CODE\colour constancy\finalresults\by dataset\Grayball\SFUGrayball-GamutEdgebased-reproduction';
% [r1,c1]=find(reshape(mean(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(mean(angular_errors_recovery,1))));
% [r2,c2]=find(reshape(mean(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(mean(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(nanmedian(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(nanmedian(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(nanmedian(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(nanmedian(angular_errors_reproduction,1))))
[r1,c1]=find(reshape(quantile(angular_errors_recovery,0.95),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
     min(min(quantile(angular_errors_recovery,0.95))))
[r2,c2]=find(reshape(quantile(angular_errors_reproduction,0.95),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
     min(min(quantile(angular_errors_reproduction,0.95))))
% [r1,c1]=find(reshape(max(angular_errors_recovery,[],1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(max(angular_errors_recovery,[],1))))
% [r2,c2]=find(reshape(max(angular_errors_reproduction,[],1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(max(angular_errors_reproduction,[],1))))
estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
% estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
% [r1,c1]=find(reshape(estimOldtrimean,size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(estimOldtrimean)))
% [r2,c2]=find(reshape(estimNewtrimean,size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(estimNewtrimean)))
medianOld = nanmedian(angular_errors_recovery,1);
medianOld(1,r1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
medianNew(1,r2,c2)
estimOld75Q = quantile(angular_errors_recovery,0.95);
estimOld75Q(1,r1,c1)
estimNew75Q = quantile(angular_errors_reproduction,0.95);
estimNew75Q(1,r2,c2)
maxold = max(angular_errors_recovery,[],1);
maxold(1,r1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maxnew(1,r2,c2)
meanOld = nanmean(angular_errors_recovery,1);
meanOld(1,r1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meanNew(1,r2,c2)
%%
%-------------------- Gamutpixel -----------------------------------------%
clear,
load 'U:\Roshanak\CODE\colour constancy\Data\Gamut mapping\greyball_gamut\greyball_gamut_pixelbased';
load 'U:\Roshanak\CODE\colour constancy\finalresults\by dataset\Grayball\SFUGrayball-GamutPixelbased-reproduction';
% r1=find((mean(angular_errors_recovery,1))== min(mean((angular_errors_recovery),1)))
% r2=find((mean(angular_errors_reproduction,1))== min(mean((angular_errors_reproduction),1)))
% r1=find((nanmedian(angular_errors_recovery,1))== min(nanmedian((angular_errors_recovery),1)))
% r2=find((nanmedian(angular_errors_reproduction,1))== min(nanmedian((angular_errors_reproduction),1)))
r1=find((quantile(angular_errors_recovery,0.95))== min(quantile(angular_errors_recovery,0.95)))
r2=find((quantile(angular_errors_reproduction,0.95))== min(quantile(angular_errors_reproduction,0.95)))
% r1=find(max((angular_errors_recovery),[],1)== min(max((angular_errors_recovery),[],1)))
% r2=find(max((angular_errors_reproduction),[],1)== min(max((angular_errors_reproduction),[],1)))
estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1,r1)+0.5*estimOldQ(2,r1)+0.25*estimOldQ(3,r1)
estimNewtrimean = 0.25*estimNewQ(1,r2)+0.5*estimNewQ(2,r2)+0.25*estimNewQ(3,r2)
% estimOldtrimean = 0.25*estimOldQ(1,:)+0.5*estimOldQ(2,:)+0.25*estimOldQ(3,:);
% estimNewtrimean = 0.25*estimNewQ(1,:)+0.5*estimNewQ(2,:)+0.25*estimNewQ(3,:);
% r1=find((estimOldtrimean)== min(estimOldtrimean))
% r2=find((estimNewtrimean)== min(estimNewtrimean))
medianOld = nanmedian(angular_errors_recovery,1);
medianOld(1,r1)
medianNew = nanmedian(angular_errors_reproduction,1);
medianNew(1,r2)
estimOld75Q = quantile(angular_errors_recovery,0.95);
estimOld75Q(1,r1)
estimNew75Q = quantile(angular_errors_reproduction,0.95);
estimNew75Q(1,r2)
maxold = max(angular_errors_recovery,[],1);
maxold(1,r1)
maxnew = max(angular_errors_reproduction,[],1);
maxnew(1,r2)
meanOld = nanmean(angular_errors_recovery,1);
meanOld(1,r1)
meanNew = nanmean(angular_errors_reproduction,1);
meanNew(1,r2)
%%
%----------------- Gamutinter ------------------------------------------%
clear,
load 'U:\Roshanak\CODE\colour constancy\Data\Gamut mapping\greyball_gamut\greyball_gamut_intersection';
load 'U:\Roshanak\CODE\colour constancy\finalresults\by dataset\Grayball\SFUGrayball-GamutIntersection-reproduction';
% [r1,c1]=find(reshape(mean(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(mean(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(mean(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(mean(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(nanmedian(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(nanmedian(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(nanmedian(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(nanmedian(angular_errors_reproduction,1))))
[r1,c1]=find(reshape(quantile(angular_errors_recovery,0.95),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
    min(min(quantile(angular_errors_recovery,0.95))))
[r2,c2]=find(reshape(quantile(angular_errors_reproduction,0.95),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
    min(min(quantile(angular_errors_reproduction,0.95))))
% [r1,c1]=find(reshape(max(angular_errors_recovery,[],1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(max(angular_errors_recovery,[],1))))
% [r2,c2]=find(reshape(max(angular_errors_reproduction,[],1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(max(angular_errors_reproduction,[],1))))
estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
% estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
% [r1,c1]=find(reshape(estimOldtrimean,size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(estimOldtrimean)))
% [r2,c2]=find(reshape(estimNewtrimean,size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(estimNewtrimean)))
medianOld = nanmedian(angular_errors_recovery,1);
medianOld(1,r1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
medianNew(1,r2,c2)
estimOld75Q = quantile(angular_errors_recovery,0.95);
estimOld75Q(1,r1,c1)
estimNew75Q = quantile(angular_errors_reproduction,0.95);
estimNew75Q(1,r2,c2)
maxold = max(angular_errors_recovery,[],1);
maxold(1,r1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maxnew(1,r2,c2)
meanOld = nanmean(angular_errors_recovery,1);
meanOld(1,r1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meanNew(1,r2,c2)
%%
%------------- 1st GreyEdge ----------------------------------------------%
clear,
load 'U:\Roshanak\CODE\colour constancy\Data\1st order Grey-Edge\greyball_firstorder_greyedge';
load 'U:\Roshanak\CODE\colour constancy\finalresults\by dataset\Grayball\SFUGrayball-1stGrayEdge-reproduction';
% [r1,c1]=find(reshape(nanmean(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(nanmean(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(nanmean(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(nanmean(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(nanmedian(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(nanmedian(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(nanmedian(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(nanmedian(angular_errors_reproduction,1))))
[r1,c1]=find(reshape(quantile(angular_errors_recovery,0.95),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
    min(min(quantile(angular_errors_recovery,0.95))))
[r2,c2]=find(reshape(quantile(angular_errors_reproduction,0.95),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
    min(min(quantile(angular_errors_reproduction,0.95))))
% [r1,c1]=find(reshape(max(angular_errors_recovery,[],1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(max(angular_errors_recovery,[],1))))
% [r2,c2]=find(reshape(max(angular_errors_reproduction,[],1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(max(angular_errors_reproduction,[],1))))
estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
% estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
% [r1,c1]=find(reshape(estimOldtrimean,size(angular_errors_recovery,2),size(angular_errors_recovery,3))== ...
%     min(min(estimOldtrimean)))
% [r2,c2]=find(reshape(estimNewtrimean,size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== ...
%     min(min(estimNewtrimean)))
medianOld = nanmedian(angular_errors_recovery,1);
medianOld(1,r1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
medianNew(1,r2,c2)
estimOld75Q = quantile(angular_errors_recovery,0.95);
estimOld75Q(1,r1,c1)
estimNew75Q = quantile(angular_errors_reproduction,0.95);
estimNew75Q(1,r2,c2)
maxold = max(angular_errors_recovery,[],1);
maxold(1,r1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maxnew(1,r2,c2)
meanOld = nanmean(angular_errors_recovery,1);
meanOld(1,r1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meanNew(1,r2,c2)
%%
%-------------------- 2nd GreyEdge --------------------------------------%
clear,
load 'U:\Roshanak\CODE\colour constancy\Data\2nd order Grey-Edge\greyball_secondorder_greyedge';
load 'U:\Roshanak\CODE\colour constancy\finalresults\by dataset\Grayball\SFUGrayball-2ndGrayEdge-reproduction';
% [r1,c1]=find(reshape(nanmean(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(nanmean(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(nanmean(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(nanmean(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(nanmedian(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(nanmedian(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(nanmedian(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(nanmedian(angular_errors_reproduction,1))))
[r1,c1]=find(reshape(quantile(angular_errors_recovery,0.95),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(quantile(angular_errors_recovery,0.95))))
[r2,c2]=find(reshape(quantile(angular_errors_reproduction,0.95),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(quantile(angular_errors_reproduction,0.95))))
% [r1,c1]=find(reshape(max(angular_errors_recovery,[],1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(max(angular_errors_recovery,[],1))))
% [r2,c2]=find(reshape(max(angular_errors_reproduction,[],1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(max(angular_errors_reproduction,[],1))))
estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
% estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
% [r1,c1]=find(reshape(estimOldtrimean,size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(estimOldtrimean)))
% [r2,c2]=find(reshape(estimNewtrimean,size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(estimNewtrimean)))
medianOld = nanmedian(angular_errors_recovery,1);
medianOld(1,r1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
medianNew(1,r2,c2)
estimOld75Q = quantile(angular_errors_recovery,0.95);
estimOld75Q(1,r1,c1)
estimNew75Q = quantile(angular_errors_reproduction,0.95);
estimNew75Q(1,r2,c2)
maxold = max(angular_errors_recovery,[],1);
maxold(1,r1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maxnew(1,r2,c2)
meanOld = nanmean(angular_errors_recovery,1);
meanOld(1,r1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meanNew(1,r2,c2)
%%
%---------------------------- general Greyworld --------------------------%
clear,
load 'U:\Roshanak\CODE\colour constancy\Data\General Grey-World\greyball_generalgreyworld';
load 'U:\Roshanak\CODE\colour constancy\finalresults\by dataset\Grayball\SFUGrayball-GeneralGrayWorld-reproduction';
angular_errors_reproduction(angular_errors_reproduction == 0)=nan;
angular_errors_recovery(angular_errors_recovery == 0)=nan;
% [r1,c1]=find(reshape(mean(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(mean(angular_errors_recovery,1))))
% [r2,c2]=find(reshape(mean(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(mean(angular_errors_reproduction,1))))
% [r1,c1]=find(reshape(nanmedian(angular_errors_recovery,1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(nanmedian(angular_errors_recovery,1))))
%[r2,c2]=find(reshape(nanmedian(angular_errors_reproduction,1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(nanmedian(angular_errors_reproduction,1))))
[r1,c1]=find(reshape(quantile(angular_errors_recovery,0.95),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(quantile(angular_errors_recovery,0.95))))
[r2,c2]=find(reshape(quantile(angular_errors_reproduction,0.95),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(quantile(angular_errors_reproduction,0.95))))
% [r1,c1]=find(reshape(max(angular_errors_recovery,[],1),size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(max(angular_errors_recovery,[],1))))
% [r2,c2]=find(reshape(max(angular_errors_reproduction,[],1),size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(max(angular_errors_reproduction,[],1))))
estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
% estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
% [r1,c1]=find(reshape(estimOldtrimean,size(angular_errors_recovery,2),size(angular_errors_recovery,3))== min(min(estimOldtrimean)))
% [r2,c2]=find(reshape(estimNewtrimean,size(angular_errors_reproduction,2),size(angular_errors_reproduction,3))== min(min(estimNewtrimean)))
medianOld = nanmedian(angular_errors_recovery,1);
medianOld(1,r1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
medianNew(1,r2,c2)
estimOld75Q = quantile(angular_errors_recovery,0.95);
estimOld75Q(1,r1,c1)
estimNew75Q = quantile(angular_errors_reproduction,0.95);
estimNew75Q(1,r2,c2)
maxold = max(angular_errors_recovery,[],1);
maxold(1,r1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maxnew(1,r2,c2)
meanOld = nanmean(angular_errors_recovery,1);
meanOld(1,r1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meanNew(1,r2,c2)
%%
%---------------------- shades of Grey ----------------------------------%
clear,
load 'U:\Roshanak\CODE\colour constancy\Data\Shades-of-Grey\greyball_shadesofgrey';
load 'U:\Roshanak\CODE\colour constancy\finalresults\by dataset\Grayball\SFUGrayball-ShadesofGray-reproduction';
% c1=find(mean(angular_errors_recovery,1)== min(min(mean(angular_errors_recovery,1))))
% c2=find(mean(angular_errors_reproduction,1)== min(min(mean(angular_errors_reproduction,1))))
% c1=find(nanmedian(angular_errors_recovery,1)== min(min(nanmedian(angular_errors_recovery,1))))
% c2=find(nanmedian(angular_errors_reproduction,1)== min(min(nanmedian(angular_errors_reproduction,1))))
c1=find(quantile(angular_errors_recovery,0.95)== min(min(quantile(angular_errors_recovery,0.95))))
c2=find(quantile(angular_errors_reproduction,0.95)== min(min(quantile(angular_errors_reproduction,0.95))))
%c1=find(max(angular_errors_recovery,[],1)== min(min(max(angular_errors_recovery,[],1))))
%c2=find(max(angular_errors_reproduction,[],1)== min(min(max(angular_errors_reproduction,[],1))))
estimOldQ = quantile(angular_errors_recovery,[0.25, 0.5, 0.75]);
estimNewQ = quantile(angular_errors_reproduction,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1,c1)+0.5*estimOldQ(2,c1)+0.25*estimOldQ(3,c1)
estimNewtrimean = 0.25*estimNewQ(1,c2)+0.5*estimNewQ(2,c2)+0.25*estimNewQ(3,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:)+0.5*estimOldQ(2,:)+0.25*estimOldQ(3,:);
% estimNewtrimean = 0.25*estimNewQ(1,:)+0.5*estimNewQ(2,:)+0.25*estimNewQ(3,:);
% c1=find(estimOldtrimean == min(min(estimOldtrimean)))
% c2=find(estimNewtrimean == min(min(estimNewtrimean)))
medianOld = nanmedian(angular_errors_recovery,1);
medianOld(1,c1)
medianNew = nanmedian(angular_errors_reproduction,1);
medianNew(1,c2)
estimOld75Q = quantile(angular_errors_recovery,0.95);
estimOld75Q(1,c1)
estimNew75Q = quantile(angular_errors_reproduction,0.95);
estimNew75Q(1,c2)
maxold = max(angular_errors_recovery,[],1);
maxold(1,c1)
maxnew = max(angular_errors_reproduction,[],1);
maxnew(1,c2)
meanOld = nanmean(angular_errors_recovery,1);
meanOld(1,c1)
meanNew = nanmean(angular_errors_reproduction,1);
meanNew(1,c2)