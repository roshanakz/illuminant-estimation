%-- 28/11/2013 13:23 --%
%% 2nd order Grey-Edge
clear;
clc;
%load 'C:\ROSHANAK\DATA\2nd order Grey-Edge\sfulab_secondorder_greyedge';
load 'C:\ROSHANAK\DATA\2nd order Grey-Edge\greyball_secondorder_greyedge';
%load 'C:\ROSHANAK\DATA\2nd order Grey-Edge\colorchecker_shi_secondorder_greyedge';
[nim,mnorm,s,ch]= size(estimated_illuminants);
if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
angular_errors_recovery = zeros(nim,mnorm,s);
angular_errors_reproduction = zeros(nim,mnorm,s);
for mink_norm =1:mnorm 
for sig = 1:s  
[angular_errors_recovery(:,mink_norm, sig), angular_errors_reproduction(:,mink_norm, sig)] = error_measure( groundtruth_illuminants, estimated_illuminants, mink_norm, sig);
end
end
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-2ndGrayEdge-reproduction','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-2ndGrayEdge-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-2ndGrayEdge-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-2ndGrayEdge-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-2ndGrayEdge-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-2ndGrayEdge-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');


%% 1st order Grey-Edge
clear;
%load 'C:\ROSHANAK\DATA\1st order Grey-Edge\sfulab_firstorder_greyedge';
load 'C:\ROSHANAK\DATA\1st order Grey-Edge\greyball_firstorder_greyedge';
%load 'C:\ROSHANAK\DATA\1st order Grey-Edge\colorchecker_shi_firstorder_greyedge';
[nim,mnorm,s,ch]= size(estimated_illuminants);
if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
angular_errors_recovery = zeros(nim,mnorm,s);
angular_errors_reproduction = zeros(nim,mnorm,s);
for mink_norm =1:mnorm 
for sig = 1:s   
[angular_errors_recovery(:,mink_norm, sig), angular_errors_reproduction(:,mink_norm, sig)] = error_measure( groundtruth_illuminants, estimated_illuminants, mink_norm, sig);
end
end
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-1stGrayEdge-reproduction','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-1stGrayEdge-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-1stGrayEdge-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-1stGrayEdge-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
   'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-1stGrayEdge-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-1stGrayEdge-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');

%% general grey world
clear;
% mink_norm=10;    % any number between 1 and infinity
% sigma=4;        % sigma
%load 'C:\ROSHANAK\DATA\General Grey-World\sfulab_generalgreyworld';
%load 'C:\ROSHANAK\DATA\General Grey-World\greyball_generalgreyworld';
load 'C:\ROSHANAK\DATA\General Grey-World\colorchecker_shi_generalgreyworld';

[nim,mnorm,s,ch]= size(estimated_illuminants);
if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
angular_errors_recovery = zeros(nim,mnorm,s);
estimErrorNewGG = zeros(nim,mnorm,s);


for mink_norm =1:mnorm
for sig = 1:s
 [angular_errors_recovery(:, mink_norm,sig), angular_errors_reproduction(:, mink_norm,sig)] = ...
     error_measure(groundtruth_illuminants, estimated_illuminants, mink_norm,sig);

end
end

% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GeneralGrayWorld-reproduction','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GeneralGrayWorld-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-GeneralGrayWorld-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-GeneralGrayWorld-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-GeneralGrayWorld-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
%plotdata = sortrows([physicalangle(:,1:11)',estimErrorNewGG(:,2,1),estimErrorOldGG(:,2,1)]);
% figure, plot(plotdata(1:10,1),plotdata(1:10,3),'--rs','LineWidth',2,...
%                        'MarkerEdgeColor','k',...
%                        'MarkerFaceColor','k',...
%                        'MarkerSize',10), hold on, plot(plotdata(1:10,1),plotdata(1:10,2),'--^','LineWidth',2,...
%                        'MarkerEdgeColor','k',...
%                        'MarkerFaceColor','k',...
%                        'MarkerSize',10)

%figure,plot(physicalangle(1,1:11),estimErrorNewGG(:,1,1),'+');
%estimNewmed = estimNewmed';
%estimOldmed = estimOldmed';

%% shades of grey %it's a superset of General grey world
clear;
%mink_norm=12;    % any number between 1 and infinity
%load 'C:\ROSHANAK\DATA\Shades-of-Grey\sfulab_shadesofgrey';
%load 'C:\ROSHANAK\DATA\Shades-of-Grey\greyball_shadesofgrey';
load 'C:\ROSHANAK\DATA\Shades-of-Grey\colorchecker_shi_shadesofgrey';

[nim,mnorm,ch]= size(estimated_illuminants);
angular_errors_recovery = zeros(nim,mnorm-1);
angular_errors_reproduction = zeros(nim,mnorm-1);

for mink_norm =1:mnorm     

[angular_errors_recovery(:,mink_norm), angular_errors_reproduction(:,mink_norm)] = ...
    error_measure( groundtruth_illuminants, estimated_illuminants, mink_norm);

end
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-ShadesofGray-reproduction','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-ShadesofGray-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-ShadesofGray-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-ShadesofGray-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-ShadesofGray-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-ShadesofGray-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');



%% MaxRGB (White-Patch)
clear;
%load 'C:\ROSHANAK\DATA\White-Patch\sfulab_whitepatch';
load 'C:\ROSHANAK\DATA\White-Patch\greyball_whitepatch';
%load 'C:\ROSHANAK\DATA\White-Patch\colorchecker_shi_whitepatch';
[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants,0,0);
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-MaxRGB-reproduction','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%     'estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-MaxRGB-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%     'estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-MaxRGB-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-MaxRGB-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
    'all_image_names','estimated_illuminants','groundtruth_illuminants');

% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-MaxRGB-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-MaxRGB-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%     'estimated_illuminants','groundtruth_illuminants');


%% Grey-World
clear;
%load 'C:\ROSHANAK\DATA\GreyWorld\sfulab_greyworld';
load 'C:\ROSHANAK\DATA\GreyWorld\greyball_greyworld';
%load 'C:\ROSHANAK\DATA\GreyWorld\colorchecker_shi_greyworld';
%groundtruth_illuminants=groundtruth_illuminants./groundtruth_illuminants;
[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GrayWorld-reproduction','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%     'estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GrayWorld-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%     'estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-GrayWorld-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-GrayWorld-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');

% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-GrayWorld-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-GrayWorld-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%     'estimated_illuminants','groundtruth_illuminants');

% gammach = abs(sqrt(estimated_illuminants(:,1)./estimated_illuminants(:,3)));
% light1 = [ones(size(gammach)),zeros(size(gammach)),gammach];
% betach = abs(sqrt(estimated_illuminants(:,3)./estimated_illuminants(:,2)));
% light2 = [zeros(size(betach)),betach,ones(size(betach))];
% alphach = abs(sqrt(estimated_illuminants(:,2)./estimated_illuminants(:,1)));
% light3 = [alphach,ones(size(alphach)),zeros(size(alphach))];
% 
% estimlightgamma = [estimated_illuminants(:,1),zeros(size(betach)),estimated_illuminants(:,3).*gammach];
% [maxoldgamma]=error_measure( estimlightgamma, light1);
% estimlightbeta = [zeros(size(betach)),estimated_illuminants(:,2).*betach,estimated_illuminants(:,3)];
% [maxoldbeta]=error_measure( estimlightbeta, light2);
% estimlightalpha = [estimated_illuminants(:,1).*alphach,estimated_illuminants(:,2),zeros(size(betach))];
% [maxoldalpha]=error_measure( estimlightalpha, light3);
% 
% x = sort(angular_errors_recovery,1);
% n = length(angular_errors_recovery);
% p = (((1:n)-0.5)' ./ n);
% figure(2),stairs(p,x,'k-'),hold on,stairs(p,sort(maxoldgamma,1),'r-')
% % ,hold on,stairs(sort(maxoldbeta,1),p,'b-'),...
% %     hold on,stairs(sort(maxoldalpha,1),p,'g-');
% xlabel('Recovery angular errors');
% ylabel('Cumulative probability (p)'), hold on

% ,...
%     'illuminant with \beta=\surd(\mu_b/\mu_g)','illuminant with \alpha=\surd(\mu_g/\mu_r)');
%set(h,'Interpreter','latex','fontsize',24) ;
%figure,hist(x),hold on, hist(maxold);
%figure,cdfplot(estimErrorOldGW),hold on, cdfplot(maxold);
%% Gamut mapping edgebased
clear;
clc;
% sfu
%load 'C:\ROSHANAK\DATA\Gamut mapping\sfulab_gamut\sfulab_gamut_edgebased';
% shi
%load 'C:\ROSHANAK\DATA\Gamut mapping\colorchecker_shi_gamut\colorchecker_shi_gamut_edgebased';
% greyball
load 'C:\ROSHANAK\DATA\Gamut mapping\greyball_gamut\greyball_gamut_edgebased';

[nim,mnorm,s,ch]= size(estimated_illuminants);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
estimErrorOldGamutEdge = zeros(nim,mnorm,s);
estimErrorNewGamutEdge = zeros(nim,mnorm,s);

for mink_norm =1:mnorm 
for sig = 1:s  
[angular_errors_recovery(:,mink_norm, sig), angular_errors_reproduction(:,mink_norm, sig)] = ...
    error_measure( groundtruth_illuminants, estimated_illuminants, mink_norm, sig);
end
end

% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GamutEdgebased-reproduction','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GamutEdgebased-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-GamutEdgebased-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-GamutEdgebased-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
    'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-GamutEdgebased-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-GamutEdgebased-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');

%% Gamut mapping intersection
clear;
clc;
% SFU
%load 'C:\ROSHANAK\DATA\Gamut mapping\sfulab_gamut\sfulab_gamut_intersection';
% shi
%load 'C:\ROSHANAK\DATA\Gamut mapping\colorchecker_shi_gamut\colorchecker_shi_gamut_intersection';
% greyball
load 'C:\ROSHANAK\DATA\Gamut mapping\greyball_gamut\greyball_gamut_intersection';
[nim,mnorm,s,ch]= size(estimated_illuminants);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
estimErrorOldGamutinter = zeros(nim,mnorm,s);
estimErrorNewGamutinter = zeros(nim,mnorm,s);

for mink_norm =1:mnorm 
for sig = 1:s  
[angular_errors_recovery(:,mink_norm, sig), angular_errors_reproduction(:,mink_norm, sig)] = ...
    error_measure( groundtruth_illuminants, estimated_illuminants, mink_norm, sig);
end
end

% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GamutIntersection-reproduction','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GamutIntersection-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGrayball-GamutIntersection-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGrayball-GamutIntersection-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
    'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');

% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-GamutIntersection-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-GamutIntersection-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');


%% gamut mapping pixel-based
clear;
%clc;
% sfu
%load 'C:\ROSHANAK\DATA\Gamut mapping\sfulab_gamut\sfulab_gamut_pixelbased';
% shi
%load 'C:\ROSHANAK\DATA\Gamut mapping\colorchecker_shi_gamut\colorchecker_shi_gamut_pixelbased';
% greyball
load 'C:\ROSHANAK\DATA\Gamut mapping\greyball_gamut\greyball_gamut_pixelbased';

% D65=[1;1;1];
% normTrueIlluminant =groundtruth_illuminants./repmat((arrayfun(@(idx) norm(groundtruth_illuminants(idx,:)),...
%     1:size(groundtruth_illuminants,1))).',1,3);
% physicalangle= acosd(dot(repmat((D65/norm(D65)).',size(normTrueIlluminant,1),1)',normTrueIlluminant'));

[nim,mnorm,ch]= size(estimated_illuminants);
angular_errors_recovery = zeros(nim,mnorm);
angular_errors_reproduction = zeros(nim,mnorm);

for mink_norm =1:mnorm 

[angular_errors_recovery(:,mink_norm), angular_errors_reproduction(:,mink_norm)] =...
    error_measure( groundtruth_illuminants, estimated_illuminants, mink_norm);

end
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GamutPixelbased-reproduction','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GamutPixelbased-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-GamutPixelbased-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%      'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-GamutPixelbased-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
%      'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-GamutPixelbased-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-GamutPixelbased-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');

% gammach = abs(sqrt(estimated_illuminants(:,1,1)./estimated_illuminants(:,1,3)));
% light1 = [ones(size(gammach)),zeros(size(gammach)),gammach];
% betach = abs(sqrt(estimated_illuminants(:,1,3)./estimated_illuminants(:,1,2)));
% light2 = [zeros(size(betach)),betach,ones(size(betach))];
% alphach = abs(sqrt(estimated_illuminants(:,1,2)./estimated_illuminants(:,1,1)));
% light3 = [alphach,ones(size(alphach)),zeros(size(alphach))];
% 
% estimlightgamma = [estimated_illuminants(:,1,1),zeros(size(betach)),estimated_illuminants(:,1,3).*gammach];
% [maxoldgamma]=error_measure( estimlightgamma, light1);
% estimlightbeta = [zeros(size(betach)),estimated_illuminants(:,1,2).*betach,estimated_illuminants(:,1,3)];
% [maxoldbeta]=error_measure( estimlightbeta, light2);
% estimlightalpha = [estimated_illuminants(:,1,1).*alphach,estimated_illuminants(:,1,2),zeros(size(betach))];
% [maxoldalpha]=error_measure( estimlightalpha, light3);
% 
% x = sort(estimErrorOldGamutpixel(:,1),1);
% n = length(estimErrorOldGamutpixel(:,1));
% p = (((1:n)-0.5)' ./ n);
% figure(2),stairs(p,x,'g-'),hold on,stairs(p,sort(maxoldgamma,1),'b-')
% % ,hold on,stairs(sort(maxoldbeta,1),p,'b-'),...
% %     hold on,stairs(sort(maxoldalpha,1),p,'g-');
% xlabel('Recovery angular errors');
% ylabel('Cumulative probability (p)'), h=legend('actual angular errorsGW ','maximum angular errorsGW','actual angular errors ','maximum angular errors'),
%    figureHandle = gcf;
% set(findall(figureHandle,'type','text'),'fontSize',10,'fontWeight','bold'),
% % ,...
%     'illuminant with \beta=\surd(\mu_b/\mu_g)','illuminant with \alpha=\surd(\mu_g/\mu_r)');

%% gamut Union-based
clear,
load 'C:\ROSHANAK\DATA\Gamut mapping\sfulab_gamut\sfulab_gamut_union';

[nim,mnorm,s,ch]= size(estimated_illuminants);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
estimErrorOldGamutinter = zeros(nim,mnorm,s);
estimErrorNewGamutinter = zeros(nim,mnorm,s);

for mink_norm =1:mnorm 
for sig = 1:s  
[angular_errors_recovery(:,mink_norm, sig), angular_errors_reproduction(:,mink_norm, sig)] = ...
    error_measure( groundtruth_illuminants, estimated_illuminants, mink_norm, sig);
end
end

% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GamutUnion-reproduction','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%    'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-GamutUnion-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
   'dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-GamutGamutUnion-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%      'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-GamutGamutUnion-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','dimensions','estimated_illuminants','groundtruth_illuminants','readme');

%% Inv. Intensity Chrom. Space
clear,
clc,
%load 'C:\ROSHANAK\DATA\other\SFUlab\sfulab_iic';
load 'C:\ROSHANAK\DATA\other\Grayball\greyball_iic';
%load 'C:\ROSHANAK\DATA\other\shi\colorchecker_shi_iic';
%load 'C:\ROSHANAK\DATA\original dataset\SFU_lab\mondrian_8_bit\mondrian_8_bit\apples2\apples2_syl-cwf.rgb';
%White_light = apples2_syl_cwf./255;
%White_light = White_light./sqrt(sum((White_light).^2));
%estimated_illuminants = estimated_illuminants./repmat(sqrt(sum(estimated_illuminants.^2,2)),1,3);
%estimated_illuminants = estimated_illuminants./repmat(White_light,size(estimated_illuminants,1),1);
%groundtruth_illuminants = groundtruth_illuminants./repmat(White_light,size(estimated_illuminants,1),1);

[nim,ch]= size(estimated_illuminants);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
estimErrorOldiic = zeros(nim,1);
estimErrorNewiic = zeros(nim,1);

[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-iic-reproduction','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%     'estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-iic-reverse','angular_errors_recovery', 'angular_errors_reproduction','all_image_names',...
%     'estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGrayball-iic-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGrayball-iic-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
    'all_image_names','estimated_illuminants','groundtruth_illuminants');

% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-iic-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
%% regression (SVR)
clear,
clc,

load 'C:\ROSHANAK\DATA\other\shi\colorchecker_shi_regression';

%White_light = apples2_syl_cwf./255;
%White_light = White_light./sqrt(sum((White_light).^2));
%estimated_illuminants = estimated_illuminants./repmat(sqrt(sum(estimated_illuminants.^2,2)),1,3);
%estimated_illuminants = estimated_illuminants./repmat(White_light,size(estimated_illuminants,1),1);
%groundtruth_illuminants = groundtruth_illuminants./repmat(White_light,size(estimated_illuminants,1),1);

[nim,ch]= size(estimated_illuminants);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
estimErrorOldSVR = zeros(nim,1);
estimErrorNewSVR = zeros(nim,1);

[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);

% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-regression-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-regression-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
    'all_image_names','estimated_illuminants','groundtruth_illuminants');
%% Spatial correlation: Gaussian based
clear,
clc,

load 'C:\ROSHANAK\DATA\other\shi\colorchecker_shi_spatcorr_reg.mat';

%load 'C:\ROSHANAK\DATA\original dataset\SFU_lab\mondrian_8_bit\mondrian_8_bit\apples2\apples2_syl-cwf.rgb';
%White_light = apples2_syl_cwf./255;
%White_light = White_light./sqrt(sum((White_light).^2));
%estimated_illuminants = estimated_illuminants./repmat(sqrt(sum(estimated_illuminants.^2,2)),1,3);
%estimated_illuminants = estimated_illuminants./repmat(White_light,size(estimated_illuminants,1),1);
%groundtruth_illuminants = groundtruth_illuminants./repmat(White_light,size(estimated_illuminants,1),1);

%[nim,ch]= size(estimated_illuminants);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
% estimErrorOldspatcorr = zeros(nim,1);
% estimErrorNewspatcorr = zeros(nim,1);

[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-spatcorr_reg-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-spatcorr_reg-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
%% Bayesian
clear,
clc,

load 'C:\ROSHANAK\DATA\other\shi\colorchecker_shi_bayesian';

%load 'C:\ROSHANAK\DATA\original dataset\SFU_lab\mondrian_8_bit\mondrian_8_bit\apples2\apples2_syl-cwf.rgb';
%White_light = apples2_syl_cwf./255;
%White_light = White_light./sqrt(sum((White_light).^2));
%estimated_illuminants = estimated_illuminants./repmat(sqrt(sum(estimated_illuminants.^2,2)),1,3);
%estimated_illuminants = estimated_illuminants./repmat(White_light,size(estimated_illuminants,1),1);
%groundtruth_illuminants = groundtruth_illuminants./repmat(White_light,size(estimated_illuminants,1),1);

[nim,ch]= size(estimated_illuminants);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
estimErrorOldbayesian = zeros(nim,1);
estimErrorNewbayesian = zeros(nim,1);

[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-bayesian-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-bayesian-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
    'all_image_names','estimated_illuminants','groundtruth_illuminants');
%% Bottom-up
clear,
clc,

load 'C:\ROSHANAK\DATA\other\shi\colorchecker_shi_hvi_bu';

%load 'C:\ROSHANAK\DATA\original dataset\SFU_lab\mondrian_8_bit\mondrian_8_bit\apples2\apples2_syl-cwf.rgb';
%White_light = apples2_syl_cwf./255;
%White_light = White_light./sqrt(sum((White_light).^2));
%estimated_illuminants = estimated_illuminants./repmat(sqrt(sum(estimated_illuminants.^2,2)),1,3);
%estimated_illuminants = estimated_illuminants./repmat(White_light,size(estimated_illuminants,1),1);
%groundtruth_illuminants = groundtruth_illuminants./repmat(White_light,size(estimated_illuminants,1),1);

[nim,ch]= size(estimated_illuminants);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
estimErrorOldhvi_bu = zeros(nim,1);
estimErrorNewhvi_bu = zeros(nim,1);

[angular_errors_recovery,angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-hvi_bu-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-hvi_bu-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
    'all_image_names','estimated_illuminants','groundtruth_illuminants');
%% Top-down
clear,
clc,

load 'C:\ROSHANAK\DATA\other\shi\colorchecker_shi_hvi_td';

%load 'C:\ROSHANAK\DATA\original dataset\SFU_lab\mondrian_8_bit\mondrian_8_bit\apples2\apples2_syl-cwf.rgb';
%White_light = apples2_syl_cwf./255;
%White_light = White_light./sqrt(sum((White_light).^2));
%estimated_illuminants = estimated_illuminants./repmat(sqrt(sum(estimated_illuminants.^2,2)),1,3);
%estimated_illuminants = estimated_illuminants./repmat(White_light,size(estimated_illuminants,1),1);
%groundtruth_illuminants = groundtruth_illuminants./repmat(White_light,size(estimated_illuminants,1),1);

[nim,ch]= size(estimated_illuminants);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
estimErrorOldhvi_td = zeros(nim,1);
estimErrorNewhvi_td = zeros(nim,1);

[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-hvi_td-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-hvi_td-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
    'all_image_names','estimated_illuminants','groundtruth_illuminants');
%% bottom-up+Top-down
clear,
clc,
%load 'C:\ROSHANAK\DATA\other\SFUlab\sfulab_iic';
load 'C:\ROSHANAK\DATA\other\shi\colorchecker_shi_hvi_combination';
%load 'C:\ROSHANAK\DATA\other\shi\CCillum';
%load 'C:\ROSHANAK\DATA\original dataset\SFU_lab\mondrian_8_bit\mondrian_8_bit\apples2\apples2_syl-cwf.rgb';
%White_light = apples2_syl_cwf./255;
%White_light = White_light./sqrt(sum((White_light).^2));
%estimated_illuminants = estimated_illuminants./repmat(sqrt(sum(estimated_illuminants.^2,2)),1,3);
%estimated_illuminants = estimated_illuminants./repmat(White_light,size(estimated_illuminants,1),1);
%groundtruth_illuminants = groundtruth_illuminants./repmat(White_light,size(estimated_illuminants,1),1);

[nim,ch]= size(estimated_illuminants);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
angular_errors_recovery = zeros(nim,1);
angular_errors_reproduction = zeros(nim,1);

[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-hvi_combination-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-hvi_combination-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
    'all_image_names','estimated_illuminants','groundtruth_illuminants');
%% natural images
clear,
clc,
%load 'C:\ROSHANAK\DATA\other\shi\colorchecker_shi_ccnis';
load 'C:\ROSHANAK\DATA\other\Grayball\greyball_ccnis';

%load 'C:\ROSHANAK\DATA\original dataset\SFU_lab\mondrian_8_bit\mondrian_8_bit\apples2\apples2_syl-cwf.rgb';
%White_light = apples2_syl_cwf./255;
%White_light = White_light./sqrt(sum((White_light).^2));
%estimated_illuminants = estimated_illuminants./repmat(sqrt(sum(estimated_illuminants.^2,2)),1,3);
%estimated_illuminants = estimated_illuminants./repmat(White_light,size(estimated_illuminants,1),1);
%groundtruth_illuminants = groundtruth_illuminants./repmat(White_light,size(estimated_illuminants,1),1);

[nim,ch]= size(estimated_illuminants);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
estimErrorOld_ccnis = zeros(nim,1);
estimErrorNew_ccnis = zeros(nim,1);

[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-ccnis-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-ccnis-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-ccnis-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-ccnis-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
    'all_image_names','estimated_illuminants','groundtruth_illuminants');
%% Cart-based selection
clear,
clc,

load 'C:\ROSHANAK\DATA\other\shi\colorchecker_shi_AAS_AAC';
%load 'C:\ROSHANAK\DATA\other\shi\CCillum';
%load 'C:\ROSHANAK\DATA\original dataset\SFU_lab\mondrian_8_bit\mondrian_8_bit\apples2\apples2_syl-cwf.rgb';
%White_light = apples2_syl_cwf./255;
%White_light = White_light./sqrt(sum((White_light).^2));
%estimated_illuminants = estimated_illuminants./repmat(sqrt(sum(estimated_illuminants.^2,2)),1,3);
%estimated_illuminants = estimated_illuminants./repmat(White_light,size(estimated_illuminants,1),1);
%groundtruth_illuminants = groundtruth_illuminants./repmat(White_light,size(estimated_illuminants,1),1);

[nim,ch]= size(estimated_illuminants_AAC);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
estimErrorOld_AAS = zeros(nim,1);
estimErrorNew_AAS = zeros(nim,1);
estimErrorOld_AAC = zeros(nim,1);
estimErrorNew_AAC = zeros(nim,1);

[angular_errors_recovery_AAS, angular_errors_reproduction_AAS] = error_measure( groundtruth_illuminants, estimated_illuminants_AAS);
[angular_errors_recovery_AAC, angular_errors_reproduction_AAC] = error_measure( groundtruth_illuminants, estimated_illuminants_AAC);
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-AAS_AAC-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants_AAC','estimated_illuminants_AAS','groundtruth_illuminants','readme');
save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-AAS_AAC-reverse','angular_errors_recovery_AAS', 'angular_errors_reproduction_AAS',...
    'angular_errors_recovery_AAC', 'angular_errors_reproduction_AAC',...
    'all_image_names','estimated_illuminants_AAC','estimated_illuminants_AAS','groundtruth_illuminants','readme');
%% Exemplar based color constancy
clear,
clc,

%load 'C:\ROSHANAK\DATA\other\shi\colorchecker_shi_exemplarCC';
load 'C:\ROSHANAK\DATA\other\Grayball\greyball_exemplarCC';

%load 'C:\ROSHANAK\DATA\original dataset\SFU_lab\mondrian_8_bit\mondrian_8_bit\apples2\apples2_syl-cwf.rgb';
%White_light = apples2_syl_cwf./255;
%White_light = White_light./sqrt(sum((White_light).^2));
%estimated_illuminants = estimated_illuminants./repmat(sqrt(sum(estimated_illuminants.^2,2)),1,3);
%estimated_illuminants = estimated_illuminants./repmat(White_light,size(estimated_illuminants,1),1);
%groundtruth_illuminants = groundtruth_illuminants./repmat(White_light,size(estimated_illuminants,1),1);

[nim,ch]= size(estimated_illuminants);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
angular_errors_recovery = zeros(nim,1);
angular_errors_reproduction = zeros(nim,1);

[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-exemplarCC-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-exemplarCC-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-exemplarCC-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'all_image_names','estimated_illuminants','groundtruth_illuminants');
save('U:\Roshanak\CODE\colour constancy\finalresults\SFUGreyball-exemplarCC-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
    'all_image_names','estimated_illuminants','groundtruth_illuminants');
%% heavy tailed-based
clear,
clc,
%load 'C:\ROSHANAK\DATA\other\SFUlab\SFUillum';
load 'C:\ROSHANAK\DATA\other\shi\CCillum';
%load 'C:\ROSHANAK\DATA\original dataset\SFU_lab\mondrian_8_bit\mondrian_8_bit\apples2\apples2_syl-wwf.rgb';
%White_light = apples2_syl_wwf./255;
%load 'groundtruth_illuminant_SFU';
%estimated_illuminants_ML = estimated_illuminants_ML./repmat(White_light,size(estimated_illuminants_ML,1),1);
%groundtruth_illuminants = groundtruth_illuminants./repmat(White_light,size(estimated_illuminants_ML,1),1);
[nim,ch]= size(estimated_illuminants_ML);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
angular_errors_recovery = zeros(nim,1);
angular_errors_reproduction = zeros(nim,1);
 
[angular_errors_recovery_ML, angular_errors_reproduction_ML] = error_measure( groundtruth_illuminants, estimated_illuminants_ML);
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-illum-reproduction','angular_errors_recovery_ML', 'angular_errors_reproduction_ML',...
%     'all_image_names','estimated_illuminants_ML','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\Shi-illum-reverse','angular_errors_recovery_ML', 'angular_errors_reproduction_ML',...
%     'all_image_names','estimated_illuminants_ML','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-illum-reproduction','angular_errors_recovery_ML', 'angular_errors_reproduction_ML',...
%     'all_image_names','estimated_illuminants_ML','groundtruth_illuminants');
% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-illum-reverse','angular_errors_recovery_ML', 'angular_errors_reproduction_ML',...
%     'all_image_names','estimated_illuminants_ML','groundtruth_illuminants');
%% Weighted Grey-edge
clear,
clc,
load 'C:\ROSHANAK\DATA\other\SFUlab\wge_sfulab';
load 'groundtruth_illuminant_SFU';
clear norm;
clear sigma;
%load 'C:\ROSHANAK\DATA\original dataset\SFU_lab\mondrian_8_bit\mondrian_8_bit\apples2\apples2_syl-wwf.rgb';
%White_light = apples2_syl_wwf./255;
%estimated_illuminants = estimated_illuminants./repmat(White_light,size(estimated_illuminants,1),10,3,1);
%groundtruth_illuminants = groundtruth_illuminants./repmat(White_light,size(estimated_illuminants,1),1);
[nim,mnorm,s,ch]= size(estimated_illuminants);

%if(mnorm<s), estimated_illuminants = permute(estimated_illuminants,[1 3 2 4]);[nim,mnorm,s,ch]= size(estimated_illuminants); end
estimErrorOldwge = zeros(nim,s,mnorm);
estimErrorNewwge = zeros(nim,s,mnorm);
for sig = 1:s 
for mink_norm =1:mnorm 
 
[angular_errors_recovery(:,sig,mink_norm), angular_errors_reproduction(:,sig,mink_norm)] = ...
    error_measure( groundtruth_illuminants, estimated_illuminants, mink_norm, sig);
end
end

% save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-wge-reproduction','angular_errors_recovery', 'angular_errors_reproduction',...
%     'estimated_illuminants','groundtruth_illuminants','kappa','norm','sigma');
%save('U:\Roshanak\CODE\colour constancy\finalresults\SFUlab-wge-reverse','angular_errors_recovery', 'angular_errors_reproduction',...
%    'estimated_illuminants','groundtruth_illuminants','kappa','norm','sigma');
%% AlexNetSVR
clear,
clc,
load 'C:\ROSHANAK\DATA\other\shi\colorchecker_shi_AlexNetSVR';
[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
%% CNN
clear,
clc,
load 'C:\ROSHANAK\DATA\other\shi\colorchecker_shi_deepCNN';
[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
%% colorcat
clear,
clc,
load 'C:\ROSHANAK\DATA\other\Grayball\greyball_ColorCat';
[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
%% colordog
clear,
clc,
load 'C:\ROSHANAK\DATA\other\Grayball\greyball_ColorDog_WP_GW.mat';
[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
%% SHI data
 clear;
 method = {'G1,1,1' 'G1,3,7' 'G1,7,4' 'G2,8,1' 'G2,9,1' 'GreyW' 'GGW1,2' 'GGW1,3' 'GGW1,6' 'GGW1,9' 'MRGB'};
 load '1GreyEdge-Shi_processed';
 sampnum = 11;
 shidatanew = zeros(size(estimErrorNew1G,1),sampnum);
 shidatanew(:,1) =  estimErrorNew1G(:,1,1);
 shidatanew(:,2) =  estimErrorNew1G(:,3,7);
 shidatanew(:,3) =  estimErrorNew1G(:,7,4);
 load '2GreyEdge-Shi_processed';
 shidatanew(:,4) =  angular_errors_reproduction(:,8,1);
 shidatanew(:,5) =  angular_errors_reproduction(:,9,1);
 load 'Grey-World-Shi_processed';
 shidatanew(:,6) =   estimErrorNewGW;
 load 'generalgreyworld-Shi_processed';
 shidatanew(:,7) =  estimErrorNewGG(:,1,2);
 shidatanew(:,8) =  estimErrorNewGG(:,1,3);
 shidatanew(:,9) =  estimErrorNewGG(:,1,6);
 shidatanew(:,10) =  estimErrorNewGG(:,1,9);
 load 'MaxRGB-Shi_processed';
 shidatanew(:,11) =  estimErrorNewMRGB;
% old
 shidataold = zeros(size(estimErrorNew1G,1),sampnum);
 shidataold(:,1) =  angular_errors_recovery(:,1,1);
 shidataold(:,2) =  angular_errors_recovery(:,3,7);
 shidataold(:,3) =  angular_errors_recovery(:,7,4);
 
 shidataold(:,4) =  angular_errors_recovery(:,8,1);
 shidataold(:,5) =  angular_errors_recovery(:,9,1);

 shidataold(:,6) =  estimErrorOldGW;

 shidataold(:,7) =  estimErrorOldGG(:,1,2);
 shidataold(:,8) =  estimErrorOldGG(:,1,3);
 shidataold(:,9) =  estimErrorOldGG(:,1,6);
 shidataold(:,10) = estimErrorOldGG(:,1,9);

 shidataold(:,11) =  estimErrorOldMRGB;
fi = figure(1); boxplot(shidatanew,method,'colors','r');hold on, boxplot(shidataold,method,'colors','b'); iB=findobj(fi,'Type','hggroup'); iL=findobj(iB,'Type','text'); set(iL,'Rotation',20); title('new angular errors on Shi');ylabel('new angular error');ylim([0 20]);set(gca,'YTick',0:1:20);
%fh = figure(2); boxplot(shidataold,method); hB=findobj(fh,'Type','hggroup'); hL=findobj(hB,'Type','text'); set(hL,'Rotation',20); title('old angular errors on Shi');ylabel('old angular error');ylim([0 20]);set(gca,'YTick',0:1:20);
%% Greyball data
 clear;
 method = {'G1,12,2' 'G2,4,1' 'GW' 'GGW1,2' 'GGW10,1' 'GGW2,8' 'GGW2,9' 'GGW5,3' 'GGW6,1' 'MRGB'};
 load '1GreyEdge-SFUGreyball_processed';
 sampnum = 10;
 shidatanew = zeros(size(estimErrorNew1G,1),sampnum);
 shidatanew(:,1) =  estimErrorNew1G(:,12,2);
 load '2GreyEdge-SFUGreyball_processed';
 shidatanew(:,2) =  angular_errors_reproduction(:,4,1);
 load 'Grey-World-SFUGreyball_processed';
 shidatanew(:,3) =   estimErrorNewGW;
 load 'generalgreyworld-SFUGreyball_processed';
 shidatanew(:,4) =  estimErrorNewGG(:,1,2);
 shidatanew(:,5) =  estimErrorNewGG(:,10,1);
 shidatanew(:,6) =  estimErrorNewGG(:,2,8);
 shidatanew(:,7) =  estimErrorNewGG(:,2,9);
 shidatanew(:,8) =  estimErrorNewGG(:,5,3);
 shidatanew(:,9) =  estimErrorNewGG(:,6,1);
 load 'MaxRGB-SFUGreyball_processed';
 shidatanew(:,10) =  estimErrorNewMRGB;
% old
 shidataold = zeros(size(estimErrorNew1G,1),sampnum);
 shidataold(:,1) =  angular_errors_recovery(:,12,2);
 shidataold(:,2) =  angular_errors_recovery(:,4,1);
 shidataold(:,3) =  estimErrorOldGW;
 shidataold(:,4) =  estimErrorOldGG(:,1,2);
 shidataold(:,5) =  estimErrorOldGG(:,10,1);
 shidataold(:,6) =  estimErrorOldGG(:,2,8);
 shidataold(:,7) = estimErrorOldGG(:,2,9);
 shidataold(:,8) = estimErrorOldGG(:,5,3);
 shidataold(:,9) = estimErrorOldGG(:,6,1);
 shidataold(:,10) =  estimErrorOldMRGB;
 
fi = figure(1); boxplot(shidatanew,method); iB=findobj(fi,'Type','hggroup'); iL=findobj(iB,'Type','text'); set(iL,'Rotation',20); title('new angular error on Greyball');ylabel('new angular error');ylim([0 20]);set(gca,'YTick',0:1:20);
fh = figure(2); boxplot(shidataold,method); hB=findobj(fh,'Type','hggroup'); hL=findobj(hB,'Type','text'); set(hL,'Rotation',20); title('old angular error on Greyball');ylabel('old angular error');ylim([0 20]);set(gca,'YTick',0:1:20);

%% SFU data
 clear;
 method = {'G1,10,9' 'G1,12,10' 'G1,4,3' 'G2,3,5' 'G2,7,9' 'G2,10,8' 'GW' 'GGW7,5' 'GGW9,1' 'GGW10,1' 'GGW14,7' 'MRGB'};
 load '1GreyEdge-SFU_processed';
 sampnum = 12;
 shidatanew = zeros(size(estimErrorNew1G,1),sampnum);
 shidatanew(:,1) =  estimErrorNew1G(:,10,9);
 shidatanew(:,2) =  estimErrorNew1G(:,12,10);
 shidatanew(:,3) =  estimErrorNew1G(:,4,3);
 load '2GreyEdge-SFU_processed';
 shidatanew(:,4) =  angular_errors_reproduction(:,3,5);
 shidatanew(:,5) =  angular_errors_reproduction(:,7,9);
 shidatanew(:,6) =  angular_errors_reproduction(:,10,8);
 load 'Grey-World-SFU_processed';
 shidatanew(:,7) =   estimErrorNewGW;
 load 'generalgreyworld-SFU_processed';
 shidatanew(:,8) =  estimErrorNewGG(:,7,5);
 shidatanew(:,9) =  estimErrorNewGG(:,9,1);
 shidatanew(:,10) =  estimErrorNewGG(:,10,1);
 shidatanew(:,11) =  estimErrorNewGG(:,14,7);
 load 'MaxRGB-SFU_processed';
 shidatanew(:,12) =  estimErrorNewMRGB;
% old
 shidataold = zeros(size(estimErrorNew1G,1),sampnum);
 shidataold(:,1) =  angular_errors_recovery(:,10,9);
 shidataold(:,2) =  angular_errors_recovery(:,12,10);
 shidataold(:,3) =  angular_errors_recovery(:,4,3);
 shidataold(:,4) =  angular_errors_recovery(:,3,5);
 shidataold(:,5) =  angular_errors_recovery(:,7,9);
 shidataold(:,6) =  angular_errors_recovery(:,10,8);
 shidataold(:,7) =  estimErrorOldGW;
 shidataold(:,8) =  estimErrorOldGG(:,7,5);
 shidataold(:,9) =  estimErrorOldGG(:,9,1);
 shidataold(:,10) =  estimErrorOldGG(:,10,1);
 shidataold(:,11) = estimErrorOldGG(:,14,7);
 shidataold(:,12) =  estimErrorOldMRGB;
 
fi = figure(1); boxplot(shidatanew,method); iB=findobj(fi,'Type','hggroup'); iL=findobj(iB,'Type','text'); set(iL,'Rotation',45); title('new angular error on SFU');ylabel('new angular error');ylim([0 20]);set(gca,'YTick',0:1:20);
fh = figure(2); boxplot(shidataold,method); hB=findobj(fh,'Type','hggroup'); hL=findobj(hB,'Type','text'); set(hL,'Rotation',45); title('old angular error on SFU');ylabel('old angular error');ylim([0 20]);set(gca,'YTick',0:1:20);

