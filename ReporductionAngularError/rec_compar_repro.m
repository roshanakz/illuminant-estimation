% greyworld SFU
clear;
load 'U:\Roshanak\CODE\colour constancy\Data\GreyWorld\sfulab_greyworld';
D65=[1;1;1];
normTrueIlluminant =groundtruth_illuminants./repmat((arrayfun(@(idx) norm(groundtruth_illuminants(idx,:)),...
    1:size(groundtruth_illuminants,1))).',1,3);
% groundtruth to  physical angles 
physicalangle= acosd(dot(repmat((D65/norm(D65)).',size(normTrueIlluminant,1),1)',normTrueIlluminant'));
%estimillum = [white_R.',white_G.',white_B.'];

[angular_errors_recovery, angular_errors_reproduction] = error_measure( groundtruth_illuminants, estimated_illuminants);
%angular_errors_recovery = angular_errors_recovery-repmat(mean(angular_errors_recovery),size(angular_errors_recovery,1),1);
%angular_errors_reproduction = angular_errors_reproduction-repmat(mean(angular_errors_reproduction),size(angular_errors_recovery,1),1);
plotdata = sortrows([physicalangle',angular_errors_reproduction,angular_errors_recovery]);

plotdata(:,1) = round(plotdata(:,1));
[u,id1,id2] = unique(plotdata(:,1),'rows');
B = [u,accumarray(id2,plotdata(:,3))./accumarray(id2,1)];
C = [u,accumarray(id2,plotdata(:,2))./accumarray(id2,1)];

C(:,2)=C(:,2)-repmat(mean(C(:,2)),size(C(:,2),1),1);
B(:,2)=B(:,2)-repmat(mean(B(:,2)),size(B(:,2),1),1);
numimg = size(B,1);
figure(1), plot(B(1:numimg,1),B(1:numimg,2),'k-o','LineWidth',2,...
                       'MarkerEdgeColor','k',...
                       'MarkerFaceColor','w',...
                       'MarkerSize',10),hold on ,
                    %xlabel('physical angle'),ylabel('angular errors'),legend('Recovery angualr error'),     
                     %   figureHandle = gcf;
                     %   set(findall(figureHandle,'type','text'),'fontSize',10,'fontWeight','bold'),
                   %numim = numim+1;
                   %figure(numim), 
                        plot(C(1:numimg,1),C(1:numimg,2),'k-o','LineWidth',2,...
                       'MarkerEdgeColor','k',...
                       'MarkerFaceColor','k',...
                       'MarkerSize',10), xlabel('physical angle'),ylabel('angular errors'),
                   %legend('Reproduction angular error'),
                        %figureHandle = gcf;
                        %set(findall(figureHandle,'type','text'),'fontSize',10,'fontWeight','bold'),
             
hold on,
legend('Recovery angular error','Reproduction angular error');
figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',10,'fontWeight','bold'),