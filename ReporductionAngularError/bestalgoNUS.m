%-- 25/08/2015 ---%
%% best algos according to new and old errors (mean, median, trimean) for NUS
%%
%--------------------------------------------- whitepatch-----------------%
clear,clc,
load 'C:\ROSHANAK\DATA\NUS processed\Canon600D\Canon600D_gt_whitepatch';
estimOldQ = quantile(recovery_error,[0.25, 0.5, 0.75]);
estimNewQ = quantile(reproduction_error,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(reproduction_error)
medianOld = nanmedian(recovery_error)
estimOld95Q = quantile(recovery_error,0.95)
estimnew95Q = quantile(reproduction_error,0.95)
maxold = max(recovery_error)
maxnew = max(reproduction_error)
meanold = mean(recovery_error)
meannew = mean(reproduction_error)
%---------------------- Grey world --------------------------------------%
clear,clc,
load 'C:\ROSHANAK\DATA\NUS processed\Canon600D\Canon600D_gt_greyworld';
estimOldQ = quantile(recovery_error,[0.25, 0.5, 0.75]);
estimNewQ = quantile(reproduction_error,[0.25, 0.5, 0.75]);
estimOldtrimean = 0.25*estimOldQ(1)+0.5*estimOldQ(2)+0.25*estimOldQ(3)
estimNewtrimean = 0.25*estimNewQ(1)+0.5*estimNewQ(2)+0.25*estimNewQ(3)
mediannew = nanmedian(reproduction_error)
medianOld = nanmedian(recovery_error)
estimOld95Q = quantile(recovery_error,0.95)
estimnew95Q = quantile(reproduction_error,0.95)
maxold = max(recovery_error)
maxnew = max(reproduction_error)
meanold = mean(recovery_error)
meannew = mean(reproduction_error)
%------------- 1st GreyEdge ----------------------------------------------%
clear,clc,
load 'C:\ROSHANAK\DATA\NUS processed\Canon600D\Canon600D_gt_firstgreyedge_all2';
%[r1,c1]=find(reshape(nanmean(recoveryError,1),size(recoveryError,2),size(recoveryError,3))== min(min(nanmean(recoveryError,1))))
%[r2,c2]=find(reshape(nanmean(reproductionError,1),size(reproductionError,2),size(reproductionError,3))== min(min(nanmean(reproductionError,1))))
% [r1,c1]=find(reshape(nanmedian(recoveryError,1),size(recoveryError,2),size(recoveryError,3))== ...
%      min(min(nanmedian(recoveryError,1))))
%  [r2,c2]=find(reshape(nanmedian(reproductionError,1),size(reproductionError,2),size(reproductionError,3))== ...
%     min(min(nanmedian(reproductionError,1))))
% [r1,c1]=find(reshape(quantile(recoveryError,0.95),size(recoveryError,2),size(recoveryError,3))== ...
%     min(min(quantile(recoveryError,0.95))))
% [r2,c2]=find(reshape(quantile(reproductionError,0.95),size(reproductionError,2),size(reproductionError,3))== ...
%     min(min(quantile(reproductionError,0.95))))
% [r1,c1]=find(reshape(max(recoveryError,[],1),size(recoveryError,2),size(recoveryError,3))== ...
%     min(min(max(recoveryError,[],1))))
% [r2,c2]=find(reshape(max(reproductionError,[],1),size(reproductionError,2),size(reproductionError,3))== ...
%     min(min(max(reproductionError,[],1))))
estimOldQ = quantile(recoveryError,[0.25, 0.5, 0.75]);
estimNewQ = quantile(reproductionError,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
%estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
[r1,c1]=find(reshape(estimOldtrimean,size(recoveryError,2),size(recoveryError,3))== ...
    min(min(estimOldtrimean)))
[r2,c2]=find(reshape(estimNewtrimean,size(reproductionError,2),size(reproductionError,3))== ...
    min(min(estimNewtrimean)))
medianOld = nanmedian(recoveryError,1);
medold = medianOld(1,r1,c1)
medianNew = nanmedian(reproductionError,1);
mednew = medianNew(1,r2,c2)
estimOld95Q = quantile(recoveryError,0.95);
old95q = estimOld95Q(1,r1,c1)
estimNew95Q = quantile(reproductionError,0.95);
new95q = estimNew95Q(1,r2,c2)
maxold = max(recoveryError,[],1);
maxold(1,r1,c1)
maxnew = max(reproductionError,[],1);
maximanew = maxnew(1,r2,c2)
meanOld = nanmean(recoveryError,1);
meanoldf = meanOld(1,r1,c1)
meanNew = nanmean(reproductionError,1);
meannewf = meanNew(1,r2,c2)
%%
%-------------------- 2nd GreyEdge --------------------------------------%
clear,clc,
load 'C:\ROSHANAK\DATA\NUS processed\Canon600D\Canon600D_gt_secondgreyedge_all';
%[r1,c1]=find(reshape(nanmean(recoveryError(:,1:9,1:10),1),size(recoveryError(:,1:9,1:10),2),size(recoveryError(:,1:9,1:10),3))== min(min(nanmean(recoveryError(:,1:9,1:10),1))))
%[r2,c2]=find(reshape(nanmean(reproductionError(:,1:9,1:10),1),size(reproductionError(:,1:9,1:10),2),size(reproductionError(:,1:9,1:10),3))== min(min(nanmean(reproductionError(:,1:9,1:10),1))))
%[r1,c1]=find(reshape(nanmedian(recoveryError(:,1:9,1:10),1),size(recoveryError(:,1:9,1:10),2),size(recoveryError(:,1:9,1:10),3))== min(min(nanmedian(recoveryError(:,1:9,1:10),1))))
%[r2,c2]=find(reshape(nanmedian(reproductionError(:,1:9,1:10),1),size(reproductionError(:,1:9,1:10),2),size(reproductionError(:,1:9,1:10),3))== min(min(nanmedian(reproductionError(:,1:9,1:10),1))))
%[r1,c1]=find(reshape(quantile(recoveryError(:,1:9,1:10),0.95),size(recoveryError(:,1:9,1:10),2),size(recoveryError(:,1:9,1:10),3))== min(min(quantile(recoveryError(:,1:9,1:10),0.95))))
%[r2,c2]=find(reshape(quantile(reproductionError(:,1:9,1:10),0.95),size(reproductionError(:,1:9,1:10),2),size(reproductionError(:,1:9,1:10),3))== min(min(quantile(reproductionError(:,1:9,1:10),0.95))))
% [r1,c1]=find(reshape(max(recoveryError(:,1:9,1:10),[],1),size(recoveryError(:,1:9,1:10),2),size(recoveryError(:,1:9,1:10),3))== min(min(max(recoveryError(:,1:9,1:10),[],1))))
% [r2,c2]=find(reshape(max(reproductionError(:,1:9,1:10),[],1),size(reproductionError(:,1:9,1:10),2),size(reproductionError(:,1:9,1:10),3))== min(min(max(reproductionError(:,1:9,1:10),[],1))))
estimOldQ = quantile(recoveryError,[0.25, 0.5, 0.75]);
estimNewQ = quantile(reproductionError,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
%estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
[r1,c1]=find(reshape(estimOldtrimean,size(recoveryError,2),size(recoveryError,3))== min(min(estimOldtrimean)))
[r2,c2]=find(reshape(estimNewtrimean,size(reproductionError,2),size(reproductionError,3))== min(min(estimNewtrimean)))
medianOld = nanmedian(recoveryError,1);
medold = medianOld(1,r1,c1)
medianNew = nanmedian(reproductionError,1);
mednew= medianNew(1,r2,c2)
estimOld95Q = quantile(recoveryError,0.95);
old95q = estimOld95Q(1,r1,c1)
estimNew95Q = quantile(reproductionError,0.95);
new95q = estimNew95Q(1,r2,c2)
maxold = max(recoveryError,[],1);
maximold = maxold(1,r1,c1)
maxnew = max(reproductionError,[],1);
maximnew = maxnew(1,r2,c2)
meanOld = nanmean(recoveryError,1);
meanoldf = meanOld(1,r1,c1)
meanNew = nanmean(reproductionError,1);
meannewf = meanNew(1,r2,c2)
%%
%---------------------------- general Greyworld --------------------------%
clear,clc,
load 'C:\ROSHANAK\DATA\NUS processed\Canon600D\Canon600D_gt_generalgrayworld';
angular_errors_reproduction(reproductionError == 0)=nan;
angular_errors_recovery(recoveryError == 0)=nan;
%[r1,c1]=find(reshape(mean(recoveryError,1),size(recoveryError,2),size(recoveryError,3))== min(min(mean(recoveryError,1))))
%[r2,c2]=find(reshape(mean(reproductionError,1),size(reproductionError,2),size(reproductionError,3))== min(min(mean(reproductionError,1))))
 %[r1,c1]=find(reshape(nanmedian(recoveryError,1),size(recoveryError,2),size(recoveryError,3))== min(min(nanmedian(recoveryError,1))))
%[r2,c2]=find(reshape(nanmedian(reproductionError,1),size(reproductionError,2),size(reproductionError,3))== min(min(nanmedian(reproductionError,1))))
%[r1,c1]=find(reshape(quantile(recoveryError,0.95),size(recoveryError,2),size(recoveryError,3))== min(min(quantile(recoveryError,0.95))))
%[r2,c2]=find(reshape(quantile(reproductionError,0.95),size(reproductionError,2),size(reproductionError,3))== min(min(quantile(reproductionError,0.95))))
%[r1,c1]=find(reshape(max(recoveryError,[],1),size(recoveryError,2),size(recoveryError,3))== min(min(max(recoveryError,[],1))))
%[r2,c2]=find(reshape(max(reproductionError,[],1),size(reproductionError,2),size(reproductionError,3))== min(min(max(reproductionError,[],1))))
%estimOldQ = quantile(recoveryError,[0.25, 0.5, 0.75]);
%estimNewQ = quantile(reproductionError,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,r1,c1)+0.5*estimOldQ(2,r1,c1)+0.25*estimOldQ(3,r1,c1)
%estimNewtrimean = 0.25*estimNewQ(1,r2,c2)+0.5*estimNewQ(2,r2,c2)+0.25*estimNewQ(3,r2,c2)
% estimOldtrimean = 0.25*estimOldQ(1,:,:)+0.5*estimOldQ(2,:,:)+0.25*estimOldQ(3,:,:);
% estimNewtrimean = 0.25*estimNewQ(1,:,:)+0.5*estimNewQ(2,:,:)+0.25*estimNewQ(3,:,:);
%[r1,c1]=find(reshape(estimOldtrimean,size(recoveryError,2),size(recoveryError,3))== min(min(estimOldtrimean)))
%[r2,c2]=find(reshape(estimNewtrimean,size(reproductionError,2),size(reproductionError,3))== min(min(estimNewtrimean)))
%medianOld = nanmedian(recoveryError,1);
%medianOld(1,r1,c1)
medianNew = nanmedian(reproductionError,1);
mednew = medianNew(1,r2,c2)
%estimOld95Q = quantile(recoveryError,0.95);
%estimOld95Q(1,r1,c1)
estimNew95Q = quantile(reproductionError,0.95);
new95q = estimNew95Q(1,r2,c2)
%maxold = max(recoveryError,[],1);
maximold = maxold(1,r1,c1)
maxnew = max(reproductionError,[],1);
maximanew = maxnew(1,r2,c2)
%meanOld = nanmean(recoveryError,1);
meanoldf = meanOld(1,r1,c1)
meanNew = nanmean(reproductionError,1);
meannewf = meanNew(1,r2,c2)
%%
%---------------------- shades of Grey ----------------------------------%
clear,clc,
load 'C:\ROSHANAK\DATA\NUS processed\Canon600D\Canon600D_gt_shadesofgray_all';
%c1=find(mean(recoveryError,1)== min(min(mean(recoveryError,1))))
%c2=find(mean(reproductionError,1)== min(min(mean(reproductionError,1))))
% c1=find(nanmedian(recoveryError,1)== min(min(nanmedian(recoveryError,1))))
% c2=find(nanmedian(reproductionError,1)== min(min(nanmedian(reproductionError,1))))
%c1=find(quantile(recoveryError,0.95)== min(min(quantile(recoveryError,0.95))))
%c2=find(quantile(reproductionError,0.95)== min(min(quantile(reproductionError,0.95))))
%c1=find(max(recoveryError,[],1)== min(min(max(recoveryError,[],1))))
%c2=find(max(reproductionError,[],1)== min(min(max(reproductionError,[],1))))
 estimOldQ = quantile(recoveryError,[0.25, 0.5, 0.75]);
 estimNewQ = quantile(reproductionError,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,c1)+0.5*estimOldQ(2,c1)+0.25*estimOldQ(3,c1)
%estimNewtrimean = 0.25*estimNewQ(1,c2)+0.5*estimNewQ(2,c2)+0.25*estimNewQ(3,c2)
estimOldtrimean = 0.25*estimOldQ(1,:)+0.5*estimOldQ(2,:)+0.25*estimOldQ(3,:);
estimNewtrimean = 0.25*estimNewQ(1,:)+0.5*estimNewQ(2,:)+0.25*estimNewQ(3,:);
 c1=find(estimOldtrimean == min(min(estimOldtrimean)))
 c2=find(estimNewtrimean == min(min(estimNewtrimean)))
medianOld = nanmedian(recoveryError,1);
medold = medianOld(1,c1)
medianNew = nanmedian(reproductionError,1);
mednew = medianNew(1,c2)
estimOld95Q = quantile(recoveryError,0.95);
old95q =  estimOld95Q(1,c1)
estimNew95Q = quantile(reproductionError,0.95);
new95q = estimNew95Q(1,c2)
maxold = max(recoveryError,[],1);
maximold = maxold(1,c1)
maxnew = max(reproductionError,[],1);
maximanew = maxnew(1,c2)
meanOld = nanmean(recoveryError,1);
meanOld(1,c1)
meanNew = nanmean(reproductionError,1);
meannewf = meanNew(1,c2)

%%
%---------------------- Dongliang JOSAA2014 ----------------------------------%
%%
%---------------------- gamut mapping pixel ----------------------------------%
%---- Canon1D
clear, clc
camera = 'Canon1DsMkIII';

Canon1D.estimatedillum(:,:,1) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'0','B3:D261');
Canon1D.estimatedillum(:,:,2) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'0','F3:H261');
Canon1D.estimatedillum(:,:,3) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'0','J3:L261');
Canon1D.estimatedillum(:,:,4) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'0','N3:P261');

DB = load(['C:\ROSHANAK\DATA\original dataset\singapore\original singapore\' camera '\' camera '_gt.mat']);
N = length(DB.all_image_names);
truel = DB.groundtruth_illuminants;
reproe = zeros([N 4]);
% note sigmapar = 1 implies 0, sigmapar = 2 is 1, sigmapar=3 is 3 and
% sigmapar = 4 is 5
for sigmapar = 1:4
    for i =1:N
        reproe(i,sigmapar) = acosd(dot([1;1;1]/sqrt(3),(truel(i,:)./squeeze(Canon1D.estimatedillum(i,:,sigmapar)))./norm(truel(i,:)./squeeze(Canon1D.estimatedillum(i,:,sigmapar)))));
    end
end
Canon1D.ReproError = reproe;
Canon1D.RecovError(:,1) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'0','E3:E261');
Canon1D.RecovError(:,2) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'0','I3:I261');
Canon1D.RecovError(:,3) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'0','M3:M261');
Canon1D.RecovError(:,4) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'0','Q3:Q261');
reproductionError = Canon1D.ReproError;
recoveryError = Canon1D.RecovError;
% c1=find(mean(recoveryError,1)== min(min(mean(recoveryError,1))))
% c2=find(mean(reproductionError,1)== min(min(mean(reproductionError,1))))
% c1=find(nanmedian(recoveryError,1)== min(min(nanmedian(recoveryError,1))))
% c2=find(nanmedian(reproductionError,1)== min(min(nanmedian(reproductionError,1))))
%c1=find(quantile(recoveryError,0.95)== min(min(quantile(recoveryError,0.95))))
%c2=find(quantile(reproductionError,0.95)== min(min(quantile(reproductionError,0.95))))
% c1=find(max(recoveryError,[],1)== min(min(max(recoveryError,[],1))))
% c2=find(max(reproductionError,[],1)== min(min(max(reproductionError,[],1))))
 estimOldQ = quantile(recoveryError,[0.25, 0.5, 0.75]);
 estimNewQ = quantile(reproductionError,[0.25, 0.5, 0.75]);
% estimOldtrimean = 0.25*estimOldQ(1,c1)+0.5*estimOldQ(2,c1)+0.25*estimOldQ(3,c1)
% estimNewtrimean = 0.25*estimNewQ(1,c2)+0.5*estimNewQ(2,c2)+0.25*estimNewQ(3,c2)
estimOldtrimean = 0.25*estimOldQ(1,:)+0.5*estimOldQ(2,:)+0.25*estimOldQ(3,:);
estimNewtrimean = 0.25*estimNewQ(1,:)+0.5*estimNewQ(2,:)+0.25*estimNewQ(3,:);
 c1=find(estimOldtrimean == min(min(estimOldtrimean)))
 c2=find(estimNewtrimean == min(min(estimNewtrimean)))
medianOld = nanmedian(recoveryError,1);
medold = medianOld(1,c1)
medianNew = nanmedian(reproductionError,1);
mednew = medianNew(1,c2)
estimOld95Q = quantile(recoveryError,0.95);
old95q =  estimOld95Q(1,c1)
estimNew95Q = quantile(reproductionError,0.95);
new95q = estimNew95Q(1,c2)
maxold = max(recoveryError,[],1);
maximold = maxold(1,c1)
maxnew = max(reproductionError,[],1);
maximanew = maxnew(1,c2)
meanOld = nanmean(recoveryError,1);
meanOld(1,c1)
meanNew = nanmean(reproductionError,1);
meannewf = meanNew(1,c2)

gp.estimated_illuminants = Canon1D.estimatedillum;
gp.groundtruth_illuminants = truel ;
gp.angular_errors_recovery = recoveryError;
gp.angular_errors_reproduction = reproductionError ;
save('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Canon1DsMkIII\Canon1DsMkIII_gamutpixel.mat','gp');
%%
%---------------------- gamut mapping edge ----------------------------------%
%---- Canon1D
clear, clc
camera = 'Canon1DsMkIII';
%Canon1D.estimatedillum(:,:,1) = xlsread('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\Results_Excel\Excelsheets\Canon600D.xlsx','1grad','B3:D261');
Canon1D.estimatedillum(:,:,1) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'1grad','F3:H261');
Canon1D.estimatedillum(:,:,2) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'1grad','J3:L261');
Canon1D.estimatedillum(:,:,3) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'1grad','N3:P261');

DB = load(['C:\ROSHANAK\DATA\original dataset\singapore\original singapore\' camera '\' camera '_gt.mat']);
N = length(DB.all_image_names);
truel = DB.groundtruth_illuminants;
reproe = zeros([N 3]);
% note sigmapar = 1 implies 1, sigmapar = 2 is 3, sigmapar=3 is 5 
for sigmapar = 1:3
    for i =1:N
        reproe(i,sigmapar) = acosd(dot([1;1;1]/sqrt(3),(truel(i,:)./squeeze(Canon1D.estimatedillum(i,:,sigmapar)))./norm(truel(i,:)./squeeze(Canon1D.estimatedillum(i,:,sigmapar)))));
    end
end
Canon1D.ReproError = reproe;
%Canon1D.RecovError(:,1) = xlsread('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\Results_Excel\Excelsheets\Canon600D.xlsx','1grad','E3:E261');
Canon1D.RecovError(:,1) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'1grad','I3:I261');
Canon1D.RecovError(:,2) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'1grad','M3:M261');
Canon1D.RecovError(:,3) = xlsread(['C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Results_Excel\Excelsheets\' camera '.xlsx'],'1grad','Q3:Q261');
reproductionError = Canon1D.ReproError;
recoveryError = Canon1D.RecovError;
% c1=find(mean(recoveryError,1)== min(min(mean(recoveryError,1))))
% c2=find(mean(reproductionError,1)== min(min(mean(reproductionError,1))))
% c1=find(nanmedian(recoveryError,1)== min(min(nanmedian(recoveryError,1))))
% c2=find(nanmedian(reproductionError,1)== min(min(nanmedian(reproductionError,1))))
% c1=find(quantile(recoveryError,0.95)== min(min(quantile(recoveryError,0.95))))
% c2=find(quantile(reproductionError,0.95)== min(min(quantile(reproductionError,0.95))))
%c1=find(max(recoveryError,[],1)== min(min(max(recoveryError,[],1))))
%c2=find(max(reproductionError,[],1)== min(min(max(reproductionError,[],1))))
 estimOldQ = quantile(recoveryError,[0.25, 0.5, 0.75]);
 estimNewQ = quantile(reproductionError,[0.25, 0.5, 0.75]);
%estimOldtrimean = 0.25*estimOldQ(1,c1)+0.5*estimOldQ(2,c1)+0.25*estimOldQ(3,c1)
%estimNewtrimean = 0.25*estimNewQ(1,c2)+0.5*estimNewQ(2,c2)+0.25*estimNewQ(3,c2)
estimOldtrimean = 0.25*estimOldQ(1,:)+0.5*estimOldQ(2,:)+0.25*estimOldQ(3,:);
estimNewtrimean = 0.25*estimNewQ(1,:)+0.5*estimNewQ(2,:)+0.25*estimNewQ(3,:);
 c1=find(estimOldtrimean == min(min(estimOldtrimean)))
 c2=find(estimNewtrimean == min(min(estimNewtrimean)))
medianOld = nanmedian(recoveryError,1);
medold = medianOld(1,c1)
medianNew = nanmedian(reproductionError,1);
mednew = medianNew(1,c2)
estimOld95Q = quantile(recoveryError,0.95);
old95q =  estimOld95Q(1,c1)
estimNew95Q = quantile(reproductionError,0.95);
new95q = estimNew95Q(1,c2)
maxold = max(recoveryError,[],1);
maximold = maxold(1,c1)
maxnew = max(reproductionError,[],1);
maximanew = maxnew(1,c2)
meanOld = nanmean(recoveryError,1);
meanOld(1,c1)
meanNew = nanmean(reproductionError,1);
meannewf = meanNew(1,c2)

ge.estimated_illuminants = Canon1D.estimatedillum;
ge.groundtruth_illuminants = truel ;
ge.angular_errors_recovery = recoveryError;
ge.angular_errors_reproduction = reproductionError ;
save('C:\ROSHANAK\Dropbox\UEA\publications\2015\PAMI_reproduction angular error\results\NUS_Canon1DsMkIII\Canon1DsMkIII_gamutedge.mat','ge');