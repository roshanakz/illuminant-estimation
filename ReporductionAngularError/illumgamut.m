%-- 01/04/2014 --%
% gamut of true illumints for the Greyball dataset(convex points represent the very chromatic lights)
%------------- 2st Grey-Edge algorithm -----------------------------------
%%
clear;
% they all contain the same groundtruth illuminant
%load 'C:\ROSHANAK\DATA\Gamut mapping\colorchecker_shi_gamut\colorchecker_shi_gamut_edgebased';
% use the ground truth illuminant to find the very chromatic lights
load 'C:\ROSHANAK\DATA\1st order Grey-Edge\greyball_firstorder_greyedge';
[nim,mnorm,s,ch]= size(estimated_illuminants);
groundtruth_illuminants=((groundtruth_illuminants./255));
groundtruth_illuminants_r = groundtruth_illuminants(:,1)./sum(groundtruth_illuminants,2);
groundtruth_illuminants_g = groundtruth_illuminants(:,2)./sum(groundtruth_illuminants,2);
% correlation between the physical angle and the reconstructed angle based
% on new and old angular error
D65=[1;1;1];
normTrueIlluminant =groundtruth_illuminants./repmat((arrayfun(@(idx) norm(groundtruth_illuminants(idx,:)),...
    1:size(groundtruth_illuminants,1))).',1,3);
physicalangle= acosd(dot(repmat((D65/norm(D65)).',size(normTrueIlluminant,1),1)',normTrueIlluminant'));
% find the most colored illuminants
K = convhull(groundtruth_illuminants_r,groundtruth_illuminants_g);
plot(groundtruth_illuminants_r(K),groundtruth_illuminants_g(K),'r-',groundtruth_illuminants_r,groundtruth_illuminants_g,'b+'),xlabel('r'),ylabel('g')
%figure,  trimesh(K,groundtruth_illuminants(:,1),groundtruth_illuminants(:,2),groundtruth_illuminants(:,3))

groundtruth_illuminants_chromatic = groundtruth_illuminants(K,:);
estimated_illuminants_chromatic = estimated_illuminants(K,:,:,:);
nim = size(estimated_illuminants_chromatic,1)-1;
estimErrorOld1G = zeros(nim,mnorm,s);
estimErrorNew1G = zeros(nim,mnorm,s);
% estimOldmean1G = zeros(mnorm,s);
% estimNewmean1G = zeros(mnorm,s);
% estimOldmed1G = zeros(mnorm,s);
% estimNewmed1G = zeros(mnorm,s);
% estimOldmax1G = zeros(mnorm,s);
% estimNewmax1G = zeros(mnorm,s);
% estimOldtrimean1G = zeros(mnorm,s);
% estimNewtrimean1G = zeros(mnorm,s);
for mink_norm =1:mnorm 
for sig = 1:s  
[estimErrorOld1G(:,mink_norm, sig), estimErrorNew1G(:,mink_norm, sig)] = ...
    error_measure( groundtruth_illuminants_chromatic(1:11,:), estimated_illuminants_chromatic(1:11,:,:,:), mink_norm, sig);
end
end
estimErrorOld1G = reshape(estimErrorOld1G,size(estimErrorOld1G,1),size(estimErrorOld1G,2)*size(estimErrorOld1G,3));
estimErrorNew1G = reshape(estimErrorNew1G,size(estimErrorNew1G,1),size(estimErrorNew1G,2)*size(estimErrorNew1G,3));
angular_errors_selected1G = reshape(angular_errors(K(1:11),:,:),11,size(estimErrorOld1G,2)*size(estimErrorOld1G,3));
str1G = strcat(strcat({'e_1,'},int2str(repmat((1:12),1,16).')),strcat({','},int2str( reshape(repmat(1:16,12,1),16*12,1)))).';
estimErrorOld1Gstr = [str1G;num2cell(estimErrorOld1G)];
estimErrorNew1Gstr = [str1G;num2cell(estimErrorNew1G)];
[~,ind1]=ismember(estimErrorOld1G,min(estimErrorOld1G,[],2));
[r1,c1]=find(ind1>0);
eachpic_bestalgo_1Gold(r1,1) = estimErrorOld1Gstr(1,c1);
eachpic_bestalgo_1Gold(r1,2) = num2cell(min(estimErrorOld1G,[],2));
[~,ind2]=ismember(estimErrorNew1G,min(estimErrorNew1G,[],2));
[r2,c2]=find(ind2>0);
eachpic_bestalgo_1Gnew(r2,1) = estimErrorNew1Gstr(1,c2);
eachpic_bestalgo_1Gnew(r2,2) = num2cell(min(estimErrorNew1G,[],2));
%%
%------------------ 2nd Grey-Edge algorithm -----------------------------
clear estimated_illuminants estimated_illuminants_chromatic
load 'C:\ROSHANAK\DATA\2nd order Grey-Edge\greyball_secondorder_greyedge';
[nim,mnorm,s,ch]= size(estimated_illuminants);
estimated_illuminants_chromatic = estimated_illuminants(K,:,:,:);
nim = size(estimated_illuminants_chromatic,1)-1;
estimErrorOld2G = zeros(nim,mnorm,s);
estimErrorNew2G = zeros(nim,mnorm,s);
for mink_norm =1:mnorm 
for sig = 1:s  
[estimErrorOld2G(:,mink_norm, sig), estimErrorNew2G(:,mink_norm, sig)] = ...
    error_measure( groundtruth_illuminants_chromatic(1:11,:), estimated_illuminants_chromatic(1:11,:,:,:), mink_norm, sig);
end
end
estimErrorOld2G = reshape(estimErrorOld2G,size(estimErrorOld2G,1),size(estimErrorOld2G,2)*size(estimErrorOld2G,3));
estimErrorNew2G = reshape(estimErrorNew2G,size(estimErrorNew2G,1),size(estimErrorNew2G,2)*size(estimErrorNew2G,3));
str2G = strcat(strcat({'e_2,'},int2str(repmat((1:12),1,16).')),strcat({','},int2str( reshape(repmat(1:16,12,1),16*12,1)))).';
estimErrorOld2Gstr = [str2G;num2cell(estimErrorOld2G)];
estimErrorNew2Gstr = [str2G;num2cell(estimErrorNew2G)];
[~,ind1]=ismember(estimErrorOld2G,min(estimErrorOld2G,[],2));
[r1,c1]=find(ind1>0);
eachpic_bestalgo_2Gold(r1,1) = estimErrorOld2Gstr(1,c1);
eachpic_bestalgo_2Gold(r1,2) = num2cell(min(estimErrorOld2G,[],2));
[~,ind2]=ismember(estimErrorNew2G,min(estimErrorNew2G,[],2));
[r2,c2]=find(ind2>0);
eachpic_bestalgo_2Gnew(r2,1) = estimErrorNew2Gstr(1,c2);
eachpic_bestalgo_2Gnew(r2,2) = num2cell(min(estimErrorNew2G,[],2));
%% -------------------- General Grey World algorithm ----------------------
clear estimated_illuminants estimated_illuminants_chromatic
load 'C:\ROSHANAK\DATA\General Grey-World\greyball_generalgreyworld';
[nim,mnorm,s,ch]= size(estimated_illuminants);
% groundtruth_illuminants=((groundtruth_illuminants./255));
% groundtruth_illuminants_r = groundtruth_illuminants(:,1)./sum(groundtruth_illuminants,2);
% groundtruth_illuminants_g = groundtruth_illuminants(:,2)./sum(groundtruth_illuminants,2);
% correlation between the physical angle and the reconstructed angle based
% on new and old angular error
% D65=[1;1;1];
% normTrueIlluminant =groundtruth_illuminants./repmat((arrayfun(@(idx) norm(groundtruth_illuminants(idx,:)),...
%     1:size(groundtruth_illuminants,1))).',1,3);
% physicalangle= acosd(dot(repmat((D65/norm(D65)).',size(normTrueIlluminant,1),1)',normTrueIlluminant'));
% find the most colored illuminants
%K = convhull(groundtruth_illuminants_r,groundtruth_illuminants_g);
%groundtruth_illuminants_chromatic = groundtruth_illuminants(K,:);
estimated_illuminants_chromatic = estimated_illuminants(K,:,:,:);
nim = size(estimated_illuminants_chromatic,1)-1;
estimErrorOldGG = zeros(nim,mnorm,s);
estimErrorNewGG = zeros(nim,mnorm,s);
% D65=[1;1;1];
% normTrueIlluminant =groundtruth_illuminants./repmat((arrayfun(@(idx) norm(groundtruth_illuminants(idx,:)),...
%     1:size(groundtruth_illuminants,1))).',1,3);
% physicalangle= acosd(dot(repmat((D65/norm(D65)).',size(normTrueIlluminant(K,:),1),1)',normTrueIlluminant(K,:)'));
for mink_norm =1:mnorm 
for sig = 1:s  
[estimErrorOldGG(:,mink_norm, sig), estimErrorNewGG(:,mink_norm, sig)] = ...
    error_measure( groundtruth_illuminants_chromatic(1:length(K)-1,:), estimated_illuminants_chromatic(1:length(K)-1,:,:,:), mink_norm, sig);
end
end
estimErrorOldGG = reshape(estimErrorOldGG,size(estimErrorOldGG,1),size(estimErrorOldGG,2)*size(estimErrorOldGG,3));
estimErrorNewGG = reshape(estimErrorNewGG,size(estimErrorNewGG,1),size(estimErrorNewGG,2)*size(estimErrorNewGG,3));
strGG = strcat(strcat({'e_0,'},int2str(repmat((1:size(estimated_illuminants,2)),1,size(estimated_illuminants,3)).'))...
    ,strcat({','},int2str( reshape(repmat(1:size(estimated_illuminants,3),...
    size(estimated_illuminants,2),1),size(estimated_illuminants,3)*size(estimated_illuminants,2),1)))).';
estimErrorOldGGstr = [strGG;num2cell(estimErrorOldGG)];
estimErrorNewGGstr = [strGG;num2cell(estimErrorNewGG)];
[~,ind1]=ismember(estimErrorOldGG,min(estimErrorOldGG,[],2));
[r1,c1]=find(ind1>0);
eachpic_bestalgo_GGold(r1,1) = estimErrorOldGGstr(1,c1);
eachpic_bestalgo_GGold(r1,2) = num2cell(min(estimErrorOldGG,[],2));
[~,ind2]=ismember(estimErrorNewGG,min(estimErrorNewGG,[],2));
[r2,c2]=find(ind2>0);
eachpic_bestalgo_GGnew(r2,1) = estimErrorNewGGstr(1,c2);
eachpic_bestalgo_GGnew(r2,2) = num2cell(min(estimErrorNewGG,[],2));
%figure,plot(physicalangle(1,1:10),estimErrorOldGG(1:10,:),'+')
%figure,plot(physicalangle(1,1:10),estimErrorNewGG(1:10,:),'+')

eachpic_bestalgo_old = [eachpic_bestalgo_GGold; eachpic_bestalgo_1Gold; eachpic_bestalgo_2Gold];
eachpic_bestalgo_new = [eachpic_bestalgo_GGnew; eachpic_bestalgo_1Gnew; eachpic_bestalgo_2Gnew];
%%
%-------------------- Whitepatch -----------------------------------------
%-------------------- Grey-World -----------------------------------------
%% -------------------- Gamut-Mapping Edge-based ---------------------------
clear estimated_illuminants estimated_illuminants_chromatic
load 'U:\Roshanak\CODE\colour constancy\Data\Gamut mapping\greyball_gamut\greyball_gamut_edgebased';
[nim,mnorm,s,ch]= size(estimated_illuminants);
estimated_illuminants_chromatic = estimated_illuminants(K,:,:,:);
nim = size(estimated_illuminants_chromatic,1)-1;
estimErrorOldGamutedge = zeros(nim,mnorm,s);
estimErrorNewGamutedge = zeros(nim,mnorm,s);
D65=[1;1;1];
normTrueIlluminant =groundtruth_illuminants./repmat((arrayfun(@(idx) norm(groundtruth_illuminants(idx,:)),...
    1:size(groundtruth_illuminants,1))).',1,3);
physicalangle= acosd(dot(repmat((D65/norm(D65)).',size(normTrueIlluminant(K,:),1),1)',normTrueIlluminant(K,:)'));
for mink_norm =1:mnorm 
for sig = 1:s  
[estimErrorOldGamutedge(:,mink_norm, sig), estimErrorNewGamutedge(:,mink_norm, sig)] = ...
    error_measure( groundtruth_illuminants_chromatic(1:length(K)-1,:), estimated_illuminants_chromatic(1:length(K)-1,:,:,:), mink_norm, sig);
end
end
estimErrorOldGamutedge = reshape(estimErrorOldGamutedge,size(estimErrorOldGamutedge,1),size(estimErrorOldGamutedge,2)*size(estimErrorOldGamutedge,3));
estimErrorNewGamutedge = reshape(estimErrorNewGamutedge,size(estimErrorNewGamutedge,1),size(estimErrorNewGamutedge,2)*size(estimErrorNewGamutedge,3));
strGG = strcat(strcat({'GEdge_'},int2str(repmat((1:size(estimated_illuminants,2)),1,size(estimated_illuminants,3)).'))...
    ,strcat({','},int2str( reshape(repmat(1:size(estimated_illuminants,3),...
    size(estimated_illuminants,2),1),size(estimated_illuminants,3)*size(estimated_illuminants,2),1)))).';
estimErrorOldGamutedgestr = [strGG;num2cell(estimErrorOldGamutedge)];
estimErrorNewGamutedgestr = [strGG;num2cell(estimErrorNewGamutedge)];
[~,ind1]=ismember(estimErrorOldGamutedge,min(estimErrorOldGamutedge,[],2));
[r1,c1]=find(ind1>0);
eachpic_bestalgo_Gamutedgeold(r1,1) = estimErrorOldGamutedgestr(1,c1);
eachpic_bestalgo_Gamutedgeold(r1,2) = num2cell(min(estimErrorOldGamutedge,[],2));
[~,ind2]=ismember(estimErrorNewGamutedge,min(estimErrorNewGamutedge,[],2));
[r2,c2]=find(ind2>0);
eachpic_bestalgo_Gamutedgenew(r2,1) = estimErrorNewGamutedgestr(1,c2);
eachpic_bestalgo_Gamutedgenew(r2,2) = num2cell(min(estimErrorNewGamutedge,[],2));
% figure,plot(physicalangle(1,1:10),estimErrorOldGamutpixel(1:10,:),'+')
% figure,plot(physicalangle(1,1:10),estimErrorNewGamutpixel(1:10,:),'+')
eachpic_bestalgo_old = [eachpic_bestalgo_old; eachpic_bestalgo_Gamutedgeold];
eachpic_bestalgo_new = [eachpic_bestalgo_new; eachpic_bestalgo_Gamutedgenew];
%% ------------------- Gamut-Mapping Pixle-based --------------------------------------
clear estimated_illuminants estimated_illuminants_chromatic
load 'U:\Roshanak\CODE\colour constancy\Data\Gamut mapping\greyball_gamut\greyball_gamut_pixelbased';
[nim,mnorm,ch]= size(estimated_illuminants);
estimated_illuminants_chromatic = estimated_illuminants(K,:,:);
nim = size(estimated_illuminants_chromatic,1)-1;
estimErrorOldGamutpixel = zeros(nim,mnorm);
estimErrorNewGamutpixel = zeros(nim,mnorm);
D65=[1;1;1];
normTrueIlluminant =groundtruth_illuminants./repmat((arrayfun(@(idx) norm(groundtruth_illuminants(idx,:)),...
    1:size(groundtruth_illuminants,1))).',1,3);
physicalangle= acosd(dot(repmat((D65/norm(D65)).',size(normTrueIlluminant(K,:),1),1)',normTrueIlluminant(K,:)'));

for mink_norm =1:mnorm 

[estimErrorOldGamutpixel(:,mink_norm), estimErrorNewGamutpixel(:,mink_norm)] = ...
    error_measure( groundtruth_illuminants_chromatic(1:length(K)-1,:), estimated_illuminants_chromatic(1:length(K)-1,:,:), mink_norm);

end
%estimErrorOldGamutpixel = reshape(estimErrorOldGamutpixel,size(estimErrorOldGamutpixel,1),size(estimErrorOldGamutpixel,2)*size(estimErrorOldGamutpixel,3));
%estimErrorNewGamutpixel = reshape(estimErrorNewGamutpixel,size(estimErrorNewGamutpixel,1),size(estimErrorNewGamutpixel,2)*size(estimErrorNewGamutpixel,3));
strGG = strcat({'Gpixel_'},int2str((1:mnorm).')).';
estimErrorOldGamutpixelstr = [strGG;num2cell(estimErrorOldGamutpixel)];
estimErrorNewGamutpixelstr = [strGG;num2cell(estimErrorNewGamutpixel)];
[~,ind1]=ismember(estimErrorOldGamutpixel,min(estimErrorOldGamutpixel,[],2));
[r1,c1]=find(ind1>0);
eachpic_bestalgo_Gamutpixelold(r1,1) = estimErrorOldGamutpixelstr(1,c1);
eachpic_bestalgo_Gamutpixelold(r1,2) = num2cell(min(estimErrorOldGamutpixel,[],2));
[~,ind2]=ismember(estimErrorNewGamutpixel,min(estimErrorNewGamutpixel,[],2));
[r2,c2]=find(ind2>0);
eachpic_bestalgo_Gamutpixelnew(r2,1) = estimErrorNewGamutpixelstr(1,c2);
eachpic_bestalgo_Gamutpixelnew(r2,2) = num2cell(min(estimErrorNewGamutpixel,[],2));
% figure,plot(physicalangle(1,1:10),estimErrorOldGamutpixel(1:10,:),'+')
% figure,plot(physicalangle(1,1:10),estimErrorNewGamutpixel(1:10,:),'+')
eachpic_bestalgo_old = [eachpic_bestalgo_old; eachpic_bestalgo_Gamutpixelold];
eachpic_bestalgo_new = [eachpic_bestalgo_new; eachpic_bestalgo_Gamutpixelnew];

%% ------------------- Gamut mapping intersection --------------------------------------
clear estimated_illuminants estimated_illuminants_chromatic
load 'U:\Roshanak\CODE\colour constancy\Data\Gamut mapping\greyball_gamut\greyball_gamut_intersection';
[nim,mnorm,s,ch]= size(estimated_illuminants);
estimated_illuminants_chromatic = estimated_illuminants(K,:,:,:);
nim = size(estimated_illuminants_chromatic,1)-1;
estimErrorOldGamutinter = zeros(nim,mnorm,s);
estimErrorNewGamutinter = zeros(nim,mnorm,s);
for mink_norm =1:mnorm 
for sig = 1:s  
[estimErrorOldGamutinter(:,mink_norm, sig), estimErrorNewGamutinter(:,mink_norm, sig)] = ...
    error_measure( groundtruth_illuminants_chromatic(1:length(K)-1,:), estimated_illuminants_chromatic(1:length(K)-1,:,:,:), mink_norm, sig);
end
end
estimErrorOldGamutinter = reshape(estimErrorOldGamutinter,size(estimErrorOldGamutinter,1),size(estimErrorOldGamutinter,2)*size(estimErrorOldGamutinter,3));
estimErrorNewGamutinter = reshape(estimErrorNewGamutinter,size(estimErrorNewGamutinter,1),size(estimErrorNewGamutinter,2)*size(estimErrorNewGamutinter,3));
strGG = strcat(strcat({'Gamutinter_'},int2str(repmat((1:size(estimated_illuminants,2)),1,size(estimated_illuminants,3)).'))...
    ,strcat({','},int2str( reshape(repmat(1:size(estimated_illuminants,3),...
    size(estimated_illuminants,2),1),size(estimated_illuminants,3)*size(estimated_illuminants,2),1)))).';
estimErrorOldGamutinterstr = [strGG;num2cell(estimErrorOldGamutinter)];
estimErrorNewGamutinterstr = [strGG;num2cell(estimErrorNewGamutinter)];
[~,ind1]=ismember(estimErrorOldGamutinter,min(estimErrorOldGamutinter,[],2));
%[r1,c1]=find(ind1>0);
[r1 c1]=max(ind1,[],2);
%[r1 c1] = accumarray(r1,c1,[],@max)';
%[r1,c1]=arrayfun(@(row) find(ind1 > row,1,'first'),0 );
%clear eachpic_bestalgo_Gamutinterold
eachpic_bestalgo_Gamutinterold(r1,1) = estimErrorOldGamutinterstr(1,c1);
eachpic_bestalgo_Gamutinterold(r1,2) = num2cell(min(estimErrorOldGamutinter,[],2));
[~,ind2]=ismember(estimErrorNewGamutinter,min(estimErrorNewGamutinter,[],2));
[r2,c2]=find(ind2>0);
eachpic_bestalgo_Gamutinternew(r2,1) = estimErrorNewGamutinterstr(1,c2);
eachpic_bestalgo_Gamutinternew(r2,2) = num2cell(min(estimErrorNewGamutinter,[],2));
eachpic_bestalgo_old = [eachpic_bestalgo_old; eachpic_bestalgo_Gamutinterold];
eachpic_bestalgo_new = [eachpic_bestalgo_new; eachpic_bestalgo_Gamutinternew];
%%
estimOldall = [estimErrorOldGGstr,estimErrorOld1Gstr,estimErrorOld2Gstr,estimErrorOldGamutedgestr,...
   estimErrorOldGamutinterstr,estimErrorOldGamutpixelstr];
%estimOldall_mean = mean(cell2mat(estimOldall(2:12,:)),1);
estimOldQ = quantile(cell2mat(estimOldall(2:12,:)),[0.25, 0.5, 0.75],1);
estimOldall_mean  = 0.25*estimOldQ(1,:)+0.5*estimOldQ(2,:)+0.25*estimOldQ(3,:);
estimOldall_mean = [estimOldall(1,:);num2cell(estimOldall_mean)];

estimNewall = [estimErrorNewGGstr,estimErrorNew1Gstr,estimErrorNew2Gstr,estimErrorNewGamutedgestr,...
   estimErrorNewGamutinterstr,estimErrorNewGamutpixelstr];
%estimNewall_mean = mean(cell2mat(estimNewall(2:12,:)),1);
estimNewQ = quantile(cell2mat(estimNewall(2:12,:)),[0.25, 0.5, 0.75],1);
estimNewall_mean  = 0.25*estimNewQ(1,:)+0.5*estimNewQ(2,:)+0.25*estimNewQ(3,:);
estimNewall_mean = [estimNewall(1,:);num2cell(estimNewall_mean)];

%[~,~,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','SFUFrameworks','A3:E184');
%[~,~,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','sfu','B5:F646');
%[~,~,str1] = xlsread('C:\Users\mcr13hfu\Dropbox\UEA\colour_constancy\notes\error values.xlsx','greyball','A367:E546');
%str1(:,1)=[];
%str1(:,3)=[];
%str1(:,3)=[];
%str1(:,2:3) = num2cell(round(cell2mat(str1(:,2:3))*100)/100);
%str1 = [estimOldall(1:2,:);estimNewall(1:2,:)];
str1 = [estimOldall_mean;estimNewall_mean];
sorted_im1 = sortrows(str1',2);
%[~, ind] = unique(cell2mat(str1(2,:)), 'first');
%sorted1 = sorted1(ind,:);
%sorted2 = sortrows(str1,3);
[~,~,rnk1] = unique(cell2mat(sorted_im1(:,2)));
[~,~,rnk2] = unique(cell2mat(sorted_im1(:,4)));

numalgo = size(rnk1,1);
x = ones(1,numalgo);
x2 = ones(1,numalgo)+1;

s = 1;

for i=1:numalgo
    %k=find(strcmp(str2,str1(i)));
   %if ((abs(rnk1(i)-rnk2(i)))>= 1)
    %if((sign(rnk1(i)-rnk1(i+1))~= sign(rnk2(i)-rnk2(i+1)))&& ((abs(rnk1(i)-rnk2(i)))>= 50))
        selectedrank1(s) =  rnk1(i);
        selectedrank2(s) =  rnk2(i);
        selectedalgo(s,1)= cellstr(sorted_im1 (i,1));
        selectedalgorankdiff(s,1)= abs(rnk1(i)-rnk2(i));
%         figure(1),scatter(x(1,i),rnk1(i),'r*'),xlim([0 3]),set(gca,'XTick',0:1:3),text(x(1,i) -.4,rnk1(i),sorted_im1(i,1),'FontSize',10),  hold on,
%         figure(1),scatter(x2(1,i),rnk2(i),'b*'),text(x2(1,i)+.1,rnk2(i),sorted_im1(i,1),'FontSize',10), hold on,
%         figure(1),line([1 2],[rnk1(i) rnk2(i)]); 
%         
%         s = s+1;
   %end
    
end

x = selectedrank2;
n = length(x);
k = repmat((1:n)',1,n);
signmat = zeros(n,n);
q = k(:);
k = k';
j = k(:);
k = find(q>j);
q = q(k);
j = j(k);
for fillmat = 1:n
[r,c]=find(j==fillmat);
%signmat(fillmat,1)= 0;
signmat(fillmat,q(r))= sign(x(q(r))-repmat(x(fillmat),1,length(x(q(r)))));
end
diffs = sum(x(q) < x(j));
nodiffs = sum(x(q) > x(j));
ties = sum(x(q) == x(j));
Kendall_tau_c = (nodiffs-diffs)*( (2*1)/n^2*(2-1) )
Kendall_tau_b = (nodiffs-diffs) / ( (nodiffs + diffs + ties)*(nodiffs + diffs + ties) )^0.5 
Kendall_tau_a = (nodiffs-diffs) / (n*(n-1)/2)

% matlab is giving us the Kendall_tau_b or a values 
[taut pval]= corr(selectedrank1', selectedrank2', 'type', 'kendall','tail','left');
con2disconratio =(1+taut)/(1- taut)
zp=norminv([1-pval pval],0,1);
quantTaut = zp(:,2) *sqrt((2*(2*size(selectedrank1,2)+5)))/(3*sqrt(size(selectedrank1,2)*(size(selectedrank1,2)-1)))
quantT = zp(:,2) *sqrt(size(selectedrank1,2)*(size(selectedrank1,2)-1)*(2*size(selectedrank1,2)+5)/18)
T = taut*(size(selectedrank1,2)*(size(selectedrank1,2)-1)/2)